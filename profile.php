<?php
session_start();
require_once("php/page_skeleton.php");

if (!isset($_SESSION["user"])){
    header("Location: "."sign_in_up.php", true, 303);
    die();
}

$page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
    //Styles==========
    echo $page::bootstrap_style;
    echo $page::modern_business_style;
    echo $page::font_awesome_style;
    echo $page::admin_main_style;

    //Scripts==========
    echo $page::for_ie_script;
    echo $page::angularjs_script;
    echo $page::angularjs_ng_messages_script;
    ?>
</head>

<body ng-app="rescue"
      ng-cloak
      ng-controller="profile_controller"
    <?php
    if (isset($_SESSION["user"])) {
        echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
    }
    ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<!--user navigation-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br>
            <ul class="nav nav-tabs">
                <li role="presentation" ng-class="{'active':active_tab_index==0}">
                    <a href="#" ng-click="ShowChangePass()">Change password</a>
                </li>
                <li role="presentation" ng-class="{'active':active_tab_index==1}">
                    <a href="#" ng-click="ShowOfferRecipe()">Offer recipe</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--user navigation end-->

<!--change user password-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"></div>

            <!--            Change password-->
            <div class="col-md-4" ng-show="active_tab_index==0">
                <br><br>
                <h1 class="center_text">Change password</h1>
                <br><br>

                <form name="changePasswordForm" novalidate ng-cloak>
                    <!--Old password-->
                    <fieldset class="form-group">
                        <label class="control-label" for="oldPassword">Old password (*):</label>
                        <input
                            required
                            ng-pattern="/^[A-Za-z0-9]+$/"
                            ng-minlength="4"
                            ng-maxlength="20"
                            ng-model="password.old_password"
                            ng-class="{'error_red_border' : !changePasswordForm.oldPassword.$valid && changePasswordForm.oldPassword.$dirty}"
                            name="oldPassword" type="password" class="form-control" placeholder="Between 4-20 characters">
                    </fieldset>
                    <!--Old password end-->

                    <!--New password-->
                    <fieldset class="form-group">
                        <label class="control-label" for="newPassword">New password (*):</label>
                        <input
                            required
                            ng-pattern="/^[A-Za-z0-9]+$/"
                            ng-minlength="4"
                            ng-maxlength="20"
                            ng-model="password.new_password"
                            ng-class="{'error_red_border' : !changePasswordForm.newPassword.$valid && changePasswordForm.newPassword.$dirty}"
                            name="newPassword" type="password" class="form-control" placeholder="Between 4-20 characters">
                    </fieldset>
                    <!--New password end-->

                    <!--Confirm password-->
                    <fieldset class="form-group">
                        <label class="control-label" for="confirmNewPassword">Confirm new password (*):</label>
                        <input
                            required
                            ng-pattern="/^[A-Za-z0-9]+$/"
                            ng-minlength="4"
                            ng-maxlength="20"
                            ng-model="password.confirm_new_password"
                            ng-class="{'error_red_border' : !changePasswordForm.confirmNewPassword.$valid && changePasswordForm.confirmNewPassword.$dirty}"
                            name="confirmNewPassword" type="password" class="form-control" placeholder="Between 4-20 characters">
                    </fieldset>
                    <!--Confirm password end-->

                    <fieldset class="form-group">
                        <div
                            class="btn btn-info btn-block"
                            ng-disabled="!changePasswordForm.$valid || (password.confirm_new_password!=password.new_password &&
                                                                       changePasswordForm.confirmNewPassword.$dirty &&
                                                                         changePasswordForm.newPassword.$dirty)"
                            ng-click="ChangePassword(changePasswordForm)"
                            >
                            Change password&nbsp;&nbsp;<i class="fa fa-check-circle"></i>
                        </div>
                    </fieldset>

                    <!--                    Errors handling-->
                    <fieldset class="form-group">
                        <div ng-messages="changePasswordForm.oldPassword.$error" ng-show="!changePasswordForm.oldPassword.$valid && changePasswordForm.oldPassword.$dirty">
                            <div ng-message="required" class="error_message_text">An old password is required field!</div>
                            <div ng-message="pattern" class="error_message_text">An old password field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="maxlength" class="error_message_text">An old password exceed 20 characters</div>
                            <div ng-message="minlength" class="error_message_text">A password less then 4 characters</div>
                        </div>

                        <div ng-messages="changePasswordForm.newPassword.$error" ng-show="!changePasswordForm.newPassword.$valid && changePasswordForm.newPassword.$dirty">
                            <div ng-message="required" class="error_message_text">A new password is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A new password field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="maxlength" class="error_message_text">A new password exceed 20 characters</div>
                            <div ng-message="minlength" class="error_message_text">A new password less then 4 characters</div>
                        </div>

                        <div ng-messages="changePasswordForm.confirmNewPassword.$error" ng-show="!changePasswordForm.confirmNewPassword.$valid && changePasswordForm.confirmNewPassword.$dirty">
                            <div ng-message="required" class="error_message_text">A new password confirmation is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A new password confirmation field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="maxlength" class="error_message_text">A new password confirmation is exceed 20 characters</div>
                            <div ng-message="minlength" class="error_message_text">A new password confirmation is less then 4 characters</div>
                        </div>

                        <div ng-show="password.new_password!=password.confirm_new_password &&
                                      changePasswordForm.confirmNewPassword.$dirty &&
                                      changePasswordForm.newPassword.$dirty"
                             class="error_message_text">
                            The new and confirm passwords are not equal
                        </div>
                    </fieldset>
                    <!--                    Errors handling end-->

                </form>
            </div>
            <!--Change password end-->
        </div>
    </div>
</div>
<!--end change user password-->


<!--user offer recipe-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3"></div>

            <div ng-show="active_tab_index==1" class="col-md-6">
                <br><br>
                <h1 class="center_text">Offer recipe</h1>
                <br><br>

                <form name="recipeForm" novalidate>
                    <!--Cuisine name-->
                    <fieldset class="form-group">
                        <label class="control-label" for="cuisineName">Cuisine name (*):</label>
                        <input required
                               ng-minlength="3"
                               ng-maxlength="50"
                               ng-model="recipe.cuisine_name"
                               ng-class="{'error_red_border' : !recipeForm.cuisineName.$valid && recipeForm.cuisineName.$dirty}"
                               name="cuisineName" type="text" class="form-control" placeholder="Cuisine name...">
                    </fieldset>
                    <!--Cuisine name end-->

                    <!--Recipe name-->
                    <fieldset class="form-group">
                        <label class="control-label" for="recipeName">Recipe name (*):</label>
                        <input required
                               ng-minlength="3"
                               ng-maxlength="40"
                               ng-model="recipe.name"
                               ng-class="{'error_red_border' : !recipeForm.recipeName.$valid && recipeForm.recipeName.$dirty}"
                               name="recipeName" type="text" class="form-control" placeholder="Recipe name...">
                    </fieldset>
                    <!--Recipe name end-->

                    <!--Ingredients-->
                    <fieldset class="form-group">
                        <label class="control-label" for="ingredientsNames">Ingredients, separate by comma (at least three) (*):</label>
                        <input required
                               ng-minlength="10"
                               ng-maxlength="400"
                               ng-model="recipe.ingredients"
                               ng-class="{'error_red_border' : !recipeForm.ingredientsNames.$valid && recipeForm.ingredientsNames.$dirty}"
                               name="ingredientsNames" type="text" class="form-control" placeholder="Ingredients...">
                    </fieldset>
                    <!--Ingredients end-->

                    <!--Recipe description-->
                    <fieldset class="form-group">
                        <label class="control-label" for="recipeDescription">Description (*):</label>
                        <textarea
                            rows="6"
                            required
                            ng-minlength="200"
                            ng-maxlength="2000"
                            ng-model="recipe.description"
                            ng-class="{'error_red_border' : !recipeForm.recipeDescription.$valid && recipeForm.recipeDescription.$dirty}"
                            name="recipeDescription" class="form-control textarea_no_resizing" placeholder="Recipe description...">
                        </textarea>
                    </fieldset>
                    <!--Recipe description end-->

                    <!--Image url-->
                    <fieldset class="form-group">
                        <label class="control-label" for="imageUrl">Image url (*):</label>
                        <input required
                               type="url"
                               ng-maxlength="300"
                               ng-model="recipe.image_url"
                               ng-class="{'error_red_border' : !recipeForm.imageUrl.$valid && recipeForm.imageUrl.$dirty}"
                               name="imageUrl" type="text" class="form-control" placeholder="Image url...">
                    </fieldset>
                    <!--Image url end-->

                    <fieldset class="form-group">
                        <div
                            class="btn btn-info btn-block"
                            ng-disabled="!recipeForm.$valid"
                            ng-click="OfferRecipe(recipeForm)"
                            >
                            Offer recipe&nbsp;&nbsp;<i class="fa fa-check-circle"></i>
                        </div>
                    </fieldset>

                    <!--                    Errors handling-->
                    <fieldset class="form-group">
                        <div ng-messages="recipeForm.cuisineName.$error" ng-show="!recipeForm.cuisineName.$valid && recipeForm.cuisineName.$dirty">
                            <div ng-message="required" class="error_message_text">A cuisine name is required field!</div>
                            <div ng-message="minlength" class="error_message_text">A cuisine name must be longer then 3 characters</div>
                            <div ng-message="maxlength" class="error_message_text">A cuisine name must not exceed 50 characters</div>
                        </div>

                        <div ng-messages="recipeForm.recipeName.$error" ng-show="!recipeForm.recipeName.$valid && recipeForm.recipeName.$dirty">
                            <div ng-message="required" class="error_message_text">A recipe name is required field!</div>
                            <div ng-message="minlength" class="error_message_text">A recipe name must be longer then 3 characters</div>
                            <div ng-message="maxlength" class="error_message_text">A recipe name must not exceed 40 characters</div>
                        </div>

                        <div ng-messages="recipeForm.ingredientsNames.$error" ng-show="!recipeForm.ingredientsNames.$valid && recipeForm.ingredientsNames.$dirty">
                            <div ng-message="required" class="error_message_text">Ingredients is a required field!</div>
                            <div ng-message="minlength" class="error_message_text">Ingredients field must be longer then 10 characters</div>
                            <div ng-message="maxlength" class="error_message_text">Ingredients field must not exceed 400s characters</div>
                        </div>

                        <div ng-messages="recipeForm.recipeDescription.$error" ng-show="!recipeForm.recipeDescription.$valid && recipeForm.recipeDescription.$dirty">
                            <div ng-message="required" class="error_message_text">Description is a required field!</div>
                            <div ng-message="minlength" class="error_message_text">Description must be longer then 200 characters</div>
                            <div ng-message="maxlength" class="error_message_text">Description field must not exceed 2000 characters</div>
                        </div>

                        <div ng-messages="recipeForm.imageUrl.$error" ng-show="!recipeForm.imageUrl.$valid && recipeForm.imageUrl.$dirty">
                            <div ng-message="required" class="error_message_text">Image url is a required field!</div>
                            <div ng-message="url" class="error_message_text">Not valid url address!</div>
                            <div ng-message="maxlength" class="error_message_text">Url address exceed 300 characters</div>
                        </div>
                    </fieldset>
                    <!--                    Errors handling end-->
                </form>
            </div>
            <!--Offer a recipe end-->

            <div class="col-md-3"></div>

        </div>
    </div>

    <?php
    $page->Footer();
    ?>
</div>

<?php
//Scripts==========
echo $page::jquery_script;
echo $page::bootstrap_script;

//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_profile_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_navigation_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_navigation_factory;
echo $page::angular_user_factory;
echo $page::angular_url_factory;

?>
</body>

