<?php
  session_start();
  require_once("php/page_skeleton.php");

  if (isset($_SESSION["user"])){
      header("Location: "."index.php", true, 303);
      die();
  }

  $page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
    //Styles==========
    echo $page::bootstrap_style;
    echo $page::modern_business_style;
    echo $page::font_awesome_style;
    echo $page::admin_main_style;

    //Scripts==========
    echo $page::for_ie_script;
    echo $page::angularjs_script;
    echo $page::angularjs_ng_messages_script;
    ?>
</head>

<body ng-app="rescue"
      ng-controller="signinup_controller"
      <?php
      if (isset($_SESSION["user"])) {
         echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
      }
    ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-1"></div>

<!--            Sign in block-->
            <div class="col-md-4">
                <br><br><br><br><br>
                <h1 class="center_text">Sign In</h1>
                <br><br>

                <form name="signinForm" novalidate ng-cloak>
                    <!--User email-->
                    <fieldset class="form-group">
                            <label class="control-label" for="userName">Email (*):</label>
                            <input
                            autocomplete="false"
                            required
                            ng-maxlength="100"
                            ng-model="signin.email"
                            ng-class="{'error_red_border' : !signinForm.userEmail.$valid && signinForm.userEmail.$dirty}"
                            ng-pattern="/^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/i"
                            name="userEmail"
                            type="text"
                            class="form-control" placeholder="Your email address">

<!--                            <input required-->
<!--                                   ng-pattern="/^[A-Za-z0-9]+$/"-->
<!--                                   ng-minlength="4"-->
<!--                                   ng-maxlength="30"-->
<!--                                   ng-model="signin.name"-->
<!--                                   ng-class="{'error_red_border' : !signinForm.userName.$valid && signinForm.userName.$dirty}"-->
<!--                                   name="userName" type="text" class="form-control" placeholder="Between 4-30 characters">-->
                    </fieldset>
                    <!--User email end-->
                    <!--User password-->
                    <fieldset class="form-group">
                            <label class="control-label" for="userPassword">Password (*):</label>
                            <input
                                autocomplete="false"
                                required
                                ng-pattern="/^[A-Za-z0-9]+$/"
                                ng-minlength="4"
                                ng-maxlength="20"
                                ng-model="signin.password"
                                ng-class="{'error_red_border' : !signinForm.userPassword.$valid && signinForm.userPassword.$dirty}"
                                name="userPassword" type="password" class="form-control" placeholder="Your password">
                    </fieldset>
                    <!--User password end-->
                    <fieldset class="form-group">
                        <div
                            class="btn btn-info btn-block"
                            ng-disabled="!signinForm.$valid"
                            ng-click="SignInUser(signinForm)"
                            >
                            Sign In&nbsp;&nbsp;<i class="fa fa-sign-in"></i>
                        </div>
                    </fieldset>
                    <a href="recover.php" id="forgot_password">Forgotten password?</a>

<!--                    Errors handling-->
                    <fieldset class="form-group">
                        <div ng-messages="signinForm.userEmail.$error" ng-show="!signinForm.userEmail.$valid && signinForm.userEmail.$dirty">
                            <div ng-message="required" class="error_message_text">A user email is required field!</div>
                            <div ng-message="pattern" class="error_message_text">Not valid email address</div>
                            <div ng-message="maxlength" class="error_message_text">A user email must not exceed 100 characters</div>
                        </div>

                        <!--                        <div ng-messages="signinForm.userName.$error" ng-show="!signinForm.userName.$valid && signinForm.userName.$dirty">-->
<!--                            <div ng-message="required" class="error_message_text">A user name is required field!</div>-->
<!--                            <div ng-message="pattern" class="error_message_text">A user name field acceptable characters are: a-z, A-Z, 0-9</div>-->
<!--                            <div ng-message="minlength" class="error_message_text">A user name must be over 3 characters</div>-->
<!--                            <div ng-message="maxlength" class="error_message_text">A user name must not exceed 30 characters</div>-->
<!--                        </div>-->

                        <div ng-messages="signinForm.userPassword.$error" ng-show="!signinForm.userPassword.$valid && signinForm.userPassword.$dirty">
                            <div ng-message="required" class="error_message_text">A password is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A password field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="minlength" class="error_message_text">A password can't be less then 4 characters</div>
                            <div ng-message="maxlength" class="error_message_text">A password can't exceed 20 characters</div>
                        </div>
                    </fieldset>
<!--                    Errors handling end-->
                </form>
            </div>
<!--            Sign in end-->

            <div class="col-md-2"></div>

<!--            Sign up block-->
            <div class="col-md-4">
                <br><br>
                <h1 class="center_text">Sign Up</h1>
                <br><br>

                <form name="signupForm" novalidate ng-cloak>
                    <!--User name-->
                    <fieldset class="form-group">
                        <label class="control-label" for="userNameS">Name (*):</label>
                        <input
                               required
                               ng-pattern="/^[A-Za-z0-9]+$/"
                               ng-minlength="4"
                               ng-maxlength="30"
                               ng-model="signup.name"
                               ng-class="{'error_red_border' : !signupForm.userName.$valid && signupForm.userName.$dirty}"
                               name="userNameS" type="text" class="form-control" placeholder="Between 4-30 characters">
                    </fieldset>
                    <!--User name end-->

                    <!--User email-->
                    <fieldset class="form-group">
                        <label class="control-label" for="userEmailS">Email (*):</label>
                        <input
                            required
                            ng-maxlength="100"
                            ng-pattern="/^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/i"
                            ng-model="signup.email"
                            ng-class="{'error_red_border' : !signupForm.userEmailS.$valid && signupForm.userEmailS.$dirty}"
                            name="userEmailS" type="text" class="form-control" placeholder="Your email address">
                    </fieldset>
                    <!--User email-->

                    <!--Password-->
                    <fieldset class="form-group">
                        <label class="control-label" for="userPassword">Password (*):</label>
                        <input
                            autocomplete="false"
                            required
                            ng-pattern="/^[A-Za-z0-9]+$/"
                            ng-minlength="4"
                            ng-maxlength="20"
                            ng-model="signup.password"
                            ng-class="{'error_red_border' : !signupForm.userPassword.$valid && signupForm.userPassword.$dirty}"
                            name="userPassword" type="password" class="form-control" placeholder="Between 4-20 characters">
                    </fieldset>
                    <!--Password end-->

                    <!--Confirm password-->
                    <fieldset class="form-group">
                        <label class="control-label" for="userConfirmPassword">Confirm password (*):</label>
                        <input
                            required
                            ng-pattern="/^[A-Za-z0-9]+$/"
                            ng-minlength="4"
                            ng-maxlength="20"
                            ng-model="signup.confirm_password"
                            ng-class="{'error_red_border' : !signupForm.userConfirmPassword.$valid && signupForm.userConfirmPassword.$dirty}"
                            name="userConfirmPassword" type="password" class="form-control" placeholder="Between 4-20 characters">
                    </fieldset>
                    <!--Confirm password end-->

                    <fieldset class="form-group">
                        <div
                            class="btn btn-info btn-block"
                            ng-disabled="!signupForm.$valid || (signup.confirm_password!=signup.password &&
                                                                signupForm.userConfirmPassword.$dirty &&
                                                                signupForm.userPassword.$dirty)"
                            ng-click="SignUpUser(signupForm)"
                            >
                            Sign Up&nbsp;&nbsp;<i class="fa fa-check-circle"></i>
                        </div>
                    </fieldset>

                    <!--                    Errors handling-->
                    <fieldset class="form-group">
                        <div ng-messages="signupForm.userName.$error" ng-show="!signupForm.userName.$valid && signupForm.userName.$dirty">
                            <div ng-message="required" class="error_message_text">A user name is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A user name field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="minlength" class="error_message_text">A user name must be over 3 characters</div>
                            <div ng-message="maxlength" class="error_message_text">A user name must not exceed 30 characters</div>
                        </div>

                        <div ng-messages="signupForm.userEmailS.$error" ng-show="!signupForm.userEmailS.$valid && signupForm.userEmailS.$dirty">
                            <div ng-message="required" class="error_message_text">A user email is required field!</div>
                            <div ng-message="pattern" class="error_message_text">Not valid email address</div>
                            <div ng-message="maxlength" class="error_message_text">A user email must not exceed 100 characters</div>
                        </div>

                        <div ng-messages="signupForm.userPassword.$error" ng-show="!signupForm.userPassword.$valid && signupForm.userPassword.$dirty">
                            <div ng-message="required" class="error_message_text">A password is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A password field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="maxlength" class="error_message_text">A password exceed 20 characters</div>
                            <div ng-message="minlength" class="error_message_text">A password less then 4 characters</div>
                        </div>

                        <div ng-messages="signupForm.userConfirmPassword.$error" ng-show="!signupForm.userConfirmPassword.$valid && signupForm.userConfirmPassword.$dirty">
                            <div ng-message="required" class="error_message_text">A confirm password is required field!</div>
                            <div ng-message="pattern" class="error_message_text">A confirm password field acceptable characters are: a-z, A-Z, 0-9</div>
                            <div ng-message="maxlength" class="error_message_text">A confirm password exceed 20 characters</div>
                            <div ng-message="minlength" class="error_message_text">A confirm password less then 4 characters</div>
                        </div>

                        <div ng-show="signup.confirm_password!=signup.password &&
                                      signupForm.userConfirmPassword.$dirty &&
                                      signupForm.userPassword.$dirty"
                                      class="error_message_text">
                            The passwords are not equal
                        </div>
                    </fieldset>
                    <!--                    Errors handling end-->

                </form>
            </div>
<!--            Sign up end-->
            <div class="col-md-1"></div>

        </div>
    </div>

    <?php
    $page->Footer();
    ?>
</div>

<?php
//Scripts==========
echo $page::jquery_script;
echo $page::bootstrap_script;

//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_signinup_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_navigation_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_navigation_factory;
echo $page::angular_signinup_factory;
echo $page::angular_user_factory;
echo $page::angular_url_factory;
?>
</body>

