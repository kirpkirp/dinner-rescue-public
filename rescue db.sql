-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2016 at 01:15 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rescue`
--

-- --------------------------------------------------------

--
-- Table structure for table `categor`
--

CREATE TABLE IF NOT EXISTS `categor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1489;

--
-- Dumping data for table `categor`
--

INSERT INTO `categor` (`id`, `name`) VALUES
(57, 'meat'),
(58, 'vegetables'),
(59, 'grocery'),
(60, 'seafood'),
(61, 'dairy'),
(62, 'cheese'),
(63, 'pasta, flour, cereals'),
(64, 'fruits, berries, nuts'),
(65, 'green'),
(66, 'spices'),
(67, 'other'),
(68, 'pulses');

-- --------------------------------------------------------

--
-- Table structure for table `cookbook`
--

CREATE TABLE IF NOT EXISTS `cookbook` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461;

--
-- Dumping data for table `cookbook`
--

INSERT INTO `cookbook` (`id`, `recipe_id`, `user_id`) VALUES
(68, 56, 3),
(69, 54, 3),
(70, 62, 3),
(77, 59, 3),
(82, 60, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cuisine`
--

CREATE TABLE IF NOT EXISTS `cuisine` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;

--
-- Dumping data for table `cuisine`
--

INSERT INTO `cuisine` (`id`, `name`) VALUES
(16, 'European'),
(17, 'Russian'),
(18, 'Ukrainian');

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE IF NOT EXISTS `dish` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=197;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`id`, `recipe_id`, `ingredient_id`) VALUES
(239, 54, 34),
(240, 54, 35),
(241, 54, 37),
(242, 54, 26),
(243, 54, 36),
(247, 55, 37),
(248, 55, 36),
(249, 55, 35),
(250, 55, 40),
(251, 55, 39),
(252, 55, 38),
(253, 55, 41),
(254, 55, 42),
(255, 55, 45),
(256, 55, 43),
(266, 56, 21),
(267, 56, 46),
(268, 56, 47),
(269, 56, 49),
(270, 56, 29),
(271, 56, 48),
(370, 62, 80),
(371, 62, 81),
(372, 62, 26),
(373, 62, 82),
(374, 62, 83),
(375, 62, 35),
(376, 62, 84),
(377, 62, 57),
(378, 62, 55),
(379, 62, 51),
(380, 61, 79),
(381, 61, 75),
(382, 61, 76),
(383, 61, 29),
(384, 61, 28),
(385, 61, 77),
(386, 61, 20),
(387, 61, 24),
(388, 61, 26),
(389, 61, 78),
(390, 59, 60),
(391, 59, 29),
(392, 59, 61),
(393, 59, 26),
(394, 59, 62),
(395, 59, 63),
(396, 59, 64),
(397, 59, 65),
(398, 59, 66),
(399, 59, 67),
(400, 58, 28),
(401, 58, 54),
(402, 58, 55),
(403, 58, 56),
(404, 58, 57),
(405, 58, 47),
(406, 58, 58),
(407, 58, 20),
(408, 58, 59),
(409, 58, 29),
(410, 57, 51),
(411, 57, 50),
(412, 57, 52),
(413, 57, 44),
(414, 57, 53),
(424, 60, 56),
(425, 60, 68),
(426, 60, 69),
(427, 60, 70),
(428, 60, 71),
(429, 60, 72),
(430, 60, 63),
(431, 60, 73),
(432, 53, 31),
(433, 53, 20),
(434, 53, 26),
(435, 53, 32),
(436, 53, 24),
(437, 53, 25),
(438, 53, 28),
(439, 53, 29),
(440, 53, 33);

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ingr_category_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=248;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`id`, `name`, `ingr_category_id`) VALUES
(20, 'plain flour', 63),
(21, 'cheakpeas', 63),
(24, 'baking powder', 59),
(25, 'baking soda', 59),
(26, 'salt', 66),
(28, 'sugar', 59),
(29, 'egg', 67),
(31, 'buttermilk', 61),
(32, 'pumpkin', 58),
(33, 'ground cinnamon', 66),
(34, 'goat cheese', 62),
(35, 'dill', 65),
(36, 'red pepper', 66),
(37, 'cucumber', 58),
(38, 'fish sauce', 67),
(39, 'canola oil', 67),
(40, 'brown sugar', 59),
(41, 'rice vinegar', 67),
(42, 'avocado', 58),
(43, 'mango', 58),
(44, 'coriander', 65),
(45, 'lime', 58),
(46, 'feta cheese', 62),
(47, 'spring onion', 65),
(48, 'courgette', 58),
(49, 'ham', 57),
(50, 'humus', 59),
(51, 'carrot', 58),
(52, 'lemon', 58),
(53, 'pita bread', 63),
(54, 'wine vinegar', 67),
(55, 'onion', 58),
(56, 'cherry tomatoes', 58),
(57, 'potato', 58),
(58, 'black pudding', 63),
(59, 'sunflower oil', 67),
(60, 'tilapia fillet', 60),
(61, 'bread crumbs', 63),
(62, 'ground pepper', 66),
(63, 'olive oil', 67),
(64, 'broccoli', 58),
(65, 'farro', 63),
(66, 'pomegranate', 64),
(67, 'salad dressing', 67),
(68, 'tomatoes', 58),
(69, 'basil', 65),
(70, 'chives', 65),
(71, 'olives', 58),
(72, 'pasta', 63),
(73, 'cheddar', 62),
(75, 'cottage cheese', 61),
(76, 'sour cream', 61),
(77, 'vanilla', 59),
(78, 'raisins', 59),
(79, 'butter', 61),
(80, 'whole fish', 60),
(81, 'peppercorns', 66),
(82, 'bay leaves', 65),
(83, 'celery', 65),
(84, 'parsley', 65);

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `cuisine_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1638;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `name`, `description`, `cuisine_id`, `created`, `updated`) VALUES
(53, 'Pumpkin-cinnamon muffins', 'Ingredients:\r\n3 1/2 cups white whole wheat flour\r\n2 1/2 teaspoons ground cinnamon\r\n2 teaspoons baking powder\r\n1 teaspoon kosher salt\r\n1/2 teaspoon baking soda\r\n1 15 ounce can pumpkin puree (2 cups)\r\n1 cup sugar\r\n3 large eggs\r\n1 1/2 cups buttermilk\r\n1/4 cup pumpkin seeds, chopped (optional)\r\n\r\ndirections\r\n1. \r\nPreheat oven to 375 degrees F. Coat twelve 2 1/2-inch muffin cups with nonstick cooking spray; set aside. In a large bowl, whisk together flour, cinnamon, baking powder, salt, and baking soda.\r\n2. \r\nIn a medium bowl, whisk together pumpkin and sugar. Whisk in eggs, one at a time, then the buttermilk, until well combined.\r\n3. \r\nPour pumpkin mixture into flour mixture. Stir briskly, with a spatula, until batter is combined. Batter will be thick. Do not overmix. Fill each muffin cup with about 1/3 cup of the batter. If desired, sprinkle each with the chopped pumpkin seeds. Bake until a toothpick inserted into the center of the muffins comes out clean, about 18 minutes. Remove muffins from tins while still warm. Repeat with remaining batter. Let cool or serve warm.\r\nStorage\r\nCooled muffins may be refrigerated, in an airtight container, up to 3 days, or frozen, in plastic freezer bags, up to 1 month.\r\n\r\nTip\r\nButtermilk Replacement:\r\nIf you don''t have buttermilk, you can make your own sour milk by using 1 1/2 tablespoons lemon juice plus enough reduced fat (2%) milk to make 1 1/2 cups. Let stand for 5 minutes before using.', 18, '2016-01-29 03:22:52', '2016-03-04 04:06:17'),
(54, 'Cucumber roll-ups', 'Ingredients \r\n2 ounces goat cheese\r\n2 teaspoons fresh dill, chopped\r\n1/8 teaspoon salt\r\n1 red bell pepper, cut into 12 1-inch-long strips\r\n1 large cucumber, cut into 12 1 1/4-inch ribbons\r\n\r\ndirections\r\n1. \r\nIn a small bowl, stir together goat cheese, dill, and salt. For each roll-up, place 1/4 teaspoon of the cheese mixture at one end of a cucumber strip, press a pepper slice on top, and roll. Secure the end with a dab of cheese.', 16, '2016-01-29 03:29:54', '2016-01-29 03:29:54'),
(55, 'Tropical cucumber salad', 'ingredients\r\n3 - 5 teaspoons fish sauce\r\n1 teaspoon freshly grated lime zest, plus more for garnish\r\n2 tablespoons lime juice\r\n1 tablespoon canola oil\r\n2 teaspoons light brown sugar\r\n1 teaspoon rice vinegar\r\n1/4 teaspoon crushed red pepper\r\n1 medium English cucumber, cut into 3/4-inch dice\r\n1 avocado, cut into 3/4-inch dice\r\n1 mango, cut into 3/4-inch dice (see Kitchen Tip)\r\n1/4 cup chopped fresh cilantro (coriander)\r\n\r\ndirections\r\nWhisk fish sauce to taste, lime zest, lime juice, oil, brown sugar, vinegar and crushed red pepper in a large bowl until combined. Add cucumber, avocado, mango and cilantro; gently toss to coat. Serve garnished with lime zest, if desired.\r\n\r\nTips:\r\n1. \r\nShopping Tip: Fish sauce is a pungent Southeast Asian condiment; find it in large supermarkets and Asian markets.\r\n2. \r\nKitchen Tip: To peel and cut a mango:\r\n1. Slice both ends off the mango, revealing the long, slender seed inside. Set the fruit upright on a work surface and remove the skin with a sharp knife.\r\n2. With the seed perpendicular to you, slice the fruit from both sides of the seed, yielding two large pieces.\r\n3. Turn the seed parallel to you and slice the two smaller pieces of fruit from each side.\r\n4. Cut the fruit into the desired shape.\r\n\r\nMAKE AHEAD TIP: Cover and refrigerate for up to 1 hour.\r\nNUTRITION INFORMATION\r\n\r\nPer Serving: cal. (kcal) 169, Fat, total (g) 11, carb. (g) 18, fiber (g) 5, pro. (g) 3, sodium (mg) 178, Percent Daily Values are based on a 2,000 calorie diet', 16, '2016-01-29 03:31:54', '2016-01-29 03:55:24'),
(56, 'Easy cheesy frittata', 'Ingredients:\r\n2 spring onions, ends trimmed off\r\n4 tablespoons of frozen peas, defrosted\r\n1 courgette, halved with the ends cut off\r\n2 slices of ham\r\n100g feta cheese\r\n4 eggs\r\nDirections:\r\n1.	Switch the oven on to 180C/ 160C fan/gas 4. Snip or cut up the spring onions and put them in a bowl. Add the peas to the bowl.\r\n2.	Grate the courgette using your rotary grater, one half at a time, then add it to the bowl.\r\n3.	Cut the ham into pieces with your scissors, if you do this over the bowl it will fall straight in.\r\n4.	Break the feta into the bowl by crumbling it with your hands.\r\n5.	Crack the eggs into a bowl and, if any bits of shell fall in, scoop them out with a spoon. Whisk the eggs until the yolks are mixed into the white.\r\n6.	Pour the eggs into the other bowl and stir. Brush a round ovenproof dish, about 16cm across, with oil. Tip everything into the dish. Put the dish in the oven for 30 minutes or until the egg is set. Serve with salad and crusty bread.', 16, '2016-02-19 22:08:55', '2016-02-19 22:14:16'),
(57, 'Carrot humus with pita dippers', 'Ingredients:\r\n1 large carrot, peeled, cut into chunks and boiled until very tender\r\n1 tub humus\r\n1 lemon, halved\r\nsmall bunch coriander (optional)\r\n2 pita breads or 4 small ones, toasted and sliced\r\nDirections:\r\n1.	Grate the cooked carrot - don''t worry if it turns to mush, this is what you want.\r\n2.	Tip the humus into a bowl, add the carrot and mix well.\r\n3.	Add a squeeze of lemon juice, but mind you don''t squirt it in your eyes.\r\n4.	Snip the coriander into little pieces using scissors and stir it into the humus. Serve with the toasted pita bread', 16, '2016-02-19 22:30:26', '2016-02-27 01:12:14'),
(58, 'Black pudding potato cakes', 'Ingredients:\r\n3 tbsp white wine vinegar\r\n1 tbsp sugar\r\n1/2 small onion, finely chopped\r\n1/2 x 250g pack cherry tomatoes, quartered\r\n400g cold mashed potato\r\n1/2 bunch spring onions, finely sliced\r\n1/2 x 230g pack black pudding or 2 large slices, diced\r\n1 tbsp plain flour\r\n2 tbsp sunflower oil\r\n2 eggs\r\nDirections:\r\n1.	Make the chutney: heat the vinegar and sugar with 1 tbsp cold water, swirling until sugar dissolves. Throw in the onion and cook for 1 min, then add the tomatoes and cook for 3 mins until starting to soften. Turn off the heat and set aside to cool.\r\n2.	Mix the mashed potato and spring onion with some seasoning, then carefully fold in the black pudding. Shape into 4 patties and dust in the flour. Heat half the oil in a large non-stick frying pan and cook the potato cakes for 3 mins each side until golden. Remove from the pan and keep warm.\r\n3.	Heat the remaining oil in the frying pan, then crack both eggs in and cook how you like them. Serve 2 black pudding cakes per person, topped with a fried egg and a dollop of tomato chutney.', 16, '2016-02-19 22:41:01', '2016-02-27 01:10:45'),
(59, 'Crispy Tilapia Cakes', 'Ingredients:\r\n1 pound tilapia fillets, cut into chunks\r\n1 egg\r\n1/2 cup bread crumbs\r\n1/4 teaspoon salt, plus more to taste\r\nFreshly ground pepper\r\n1 tablespoon olive oil\r\n4 cups broccoli florets\r\n1 1/2 cups cooked farro\r\n1/4 cup pomegranate seeds\r\n5 tablespoons weekend-prepped salad dressing\r\nDirections:\r\n1. \r\nIn a food processor, pulse the tilapia, egg, bread crumbs, salt, and pepper. Form into 8 patties. Heat the olive oil in a large, nonstick saute pan over medium heat. Cook the patties 4 minutes per side, or until opaque.\r\n2.\r\n In a steamer basket over boiling water, steam the broccoli until tender, about 5 minutes. In a large bowl, combine the broccoli, farro, pomegranate seeds, salad dressing, and salt to taste. Serve with tilapia cakes.', 17, '2016-02-19 22:44:00', '2016-02-27 01:09:42'),
(60, 'Squished tomato pasta sauce', 'Ingredients:\r\n12 cherry tomatoes\r\n2 ordinary tomatoes, halved\r\n4 basil leaves\r\nsmall bunch chives\r\n8 black or green olives, stoned (optional)\r\nolive oil, for drizzling\r\n200g pasta, cooked and kept warm\r\ncheddar or Parmesan, grated\r\nDirections:\r\n1.	Put the cherry tomatoes into a large bowl, reach down into the bowl and squeeze each one hard until it bursts (mind out, they will squirt!). Pull the tomatoes into pieces.\r\n2.	Squeeze the halved tomatoes on a lemon squeezer to make as much juice as you can. Pour this into the bowl. (Grown-ups, the squeezed halves can be used up in a stew or soup.)\r\n3.	Snip the basil and chives into the bowl using your scissors, or tear them into pieces. Then, if you are using them, snip or pull the olives in half and throw them in, too.\r\n4.	Add a dribble of olive oil, then ask your grown-up helper to put the cooked pasta into two bowls, spoon your sauce on top and sprinkle with the cheese', 16, '2016-02-19 22:46:50', '2016-02-27 01:15:10'),
(61, 'Sirniki', 'Ingredients:\r\n1) 14-15 oz cottage cheese (2-2.5 cups)\r\n2) 2 eggs\r\n3) 1/4-1/3 cup sugar\r\n4) 2 Tablespoon sour cream\r\n5) 2 teaspoons vanilla\r\n6) 1/2  cup flour, plus 1/2 cup more for dredging the Sirniki\r\n7) 1/2 teaspoon baking powder\r\n8) 1/4 teaspoon salt\r\n9) 1/3-1/2 cup raisins, optional\r\n10)	butter or oil for pan frying the Sirniki\r\nDirections:\r\n1.	Place the cottage cheese into a food processor and pulse about 10 times until it''s very fine. You can also use ricotta or cottage cheese instead of the farmer''s cheese, just make sure to drain all the excess liquid and maybe even squeeze it out with a cheesecloth.\r\n2.	Add the eggs, sugar, sour cream and vanilla. Pulse a few more times to combine.\r\n3.	In a small bowl, mix 1/2 cup flour, baking powder and salt and then add the dry ingredients to the food processor. Pulse a few more times to combine.\r\n4.	Add the raisins and mix them in with a spoon or spatula.\r\n5.	Heat 1 Tablespoon of of butter or oil in a nonstick skillet on medium low heat. Make sure that the oil is hot enough before adding the Sirniki, otherwise they will soak up the oil and be very greasy.\r\n6.	Place the remaining 1/2 cup flour into a shallow plate. I use a handy scoop to portion out this very delicate batter. It makes it MUCH easier to handle.\r\n7.	Dredge each Sirnik in flour. You want to dredge the Sirniki and place them immediately into the skillet.\r\n8.	Cook the Sirniki on medium low heat until they are golden on both sides.\r\n9.	Serve warm with sour cream, jam, fruit, condensed milk or honey.\r\nNotes\r\nYou can hand mix the Sirniki batter very easily, but you will first have to break up the cottage cheese with a fork until the texture is even and fine. Proceed by adding the rest of the ingredients. It''s much simpler to use the food processor, but the advantage of mixing the batter by hand is that it will be slightly stiffer and easier to handle. The batter made in the food processor needs to be handled quite delicately.', 17, '2016-02-19 23:08:37', '2016-02-27 01:08:03'),
(62, 'Fisherman''s Soup - Ukha', 'Ingredients:\r\n1) 1 whole fish, 1-1,5 lbs, cleaned (snapper, trout, bass, salmon, etc.)\r\n2) 8-10 cups water\r\n3) 5 peppercorns\r\n4) 3 dry bay leaves\r\n5) salt\r\n6) 3Â½ -4 cups, peeled young potatoes\r\n7) 6 young carrots, peeled\r\n8) 1 large onion, chopped\r\n9) 3-5 celery stalks, chopped\r\n10) 1 Tablespoon each, fresh parsley and dill, chopped\r\nDirections:\r\n1.	For this soup, you can use any whole fish that you like. You can use 1 whole fish, or several smaller fish and even a combination of several different types of fish.\r\n2.	Place the cleaned fish in a pot with the peppercorns, bay leaf and pour in the water. The amount of water depends on how thick or thin you like your soup. Bring the water to a boil and cook for about 20 minutes, until the fish is cooked through.\r\n3.	Strain the fish broth through a fine mesh sieve. Discard the peppercorns and bay leaves. Set the fish aside to cool. When cool enough to handle, discard the fish skin and take out all the bones. Set the fish aside.\r\n4.	Return the broth to the pot and add the potatoes and carrots. You can certainly chop the carrots and potatoes into small pieces, and that''s what I do most of the time, but I thought it would be nice to try something different. It actually a nice way to serve this soup and gives it such a rustic, peasantry appeal.\r\n5.	Add the onion and celery to the soup as well. Cook the soup for another 15 minutes or so, just until the potatoes are cooked through.\r\n6.	Add the fish to the soup and garnish with fresh herbs. Serve the soup in bowls and break apart the potatoes and carrots with your spoon as you eat it.', 17, '2016-02-19 23:31:35', '2016-02-27 01:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `recipeuser`
--

CREATE TABLE IF NOT EXISTS `recipeuser` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `cuisine_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `ingredients` varchar(400) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `pass_hash` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `pass_hash`, `email`, `active`) VALUES
(3, 'Naida', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'dng82@yandex.ru', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categor`
--
ALTER TABLE `categor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cookbook`
--
ALTER TABLE `cookbook`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_cookbook_recipe_id` (`recipe_id`), ADD KEY `FK_cookbook_user_id` (`user_id`);

--
-- Indexes for table `cuisine`
--
ALTER TABLE `cuisine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_dish_recipe_id` (`recipe_id`), ADD KEY `FK_recipe_ingredients_ingredient_id` (`ingredient_id`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_ingredient_ingredient_category_id` (`ingr_category_id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_recipe_cuisine_id` (`cuisine_id`);

--
-- Indexes for table `recipeuser`
--
ALTER TABLE `recipeuser`
  ADD PRIMARY KEY (`id`), ADD KEY `FK_recipe_user_user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categor`
--
ALTER TABLE `categor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `cookbook`
--
ALTER TABLE `cookbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `cuisine`
--
ALTER TABLE `cuisine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=441;
--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `recipeuser`
--
ALTER TABLE `recipeuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cookbook`
--
ALTER TABLE `cookbook`
ADD CONSTRAINT `FK_cookbook_recipe_id` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_cookbook_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dish`
--
ALTER TABLE `dish`
ADD CONSTRAINT `FK_dish_recipe_id` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_recipe_ingredients_ingredient_id` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ingredient`
--
ALTER TABLE `ingredient`
ADD CONSTRAINT `FK_ingredient_ingredient_category_id` FOREIGN KEY (`ingr_category_id`) REFERENCES `categor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `recipe`
--
ALTER TABLE `recipe`
ADD CONSTRAINT `FK_recipe_cuisine_id` FOREIGN KEY (`cuisine_id`) REFERENCES `cuisine` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `recipeuser`
--
ALTER TABLE `recipeuser`
ADD CONSTRAINT `FK_recipe_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
