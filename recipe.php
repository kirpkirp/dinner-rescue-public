<?php
session_start();
require_once("php/page_skeleton.php");

//if (isset($_SESSION["user"])){
//    header("Location: "."index.php", true, 303);
//    die();
//}

$page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
    //Styles==========
    echo $page::bootstrap_style;
    echo $page::modern_business_style;
    echo $page::font_awesome_style;
    echo $page::admin_main_style;

    //Scripts==========
    echo $page::for_ie_script;
    echo $page::angularjs_script;
    echo $page::angularjs_ng_messages_script;
    ?>
</head>

<body ng-app="rescue"
      ng-cloak
      ng-controller="recipe_controller"
    <?php
    if (isset($_SESSION["user"])) {
        echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
    }
    ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<!-- Search recipes area -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div><i ng-show="spinner" class="fa fa-refresh fa-spin fa-2x"></i></div>
            <hr>
            <h2 class="text-center"><strong>{{recipe.name+" ("+recipe.cuisine_name+" cuisine)"}}</strong></h2>
            <hr>

            <div class="col-md-4 text-center">
                <img ng-src="{{recipe_image_url}}" class="thumbnail center_image">
                <div ng-if="user_data.id>0">
                  <br>
                  <i ng-click='AddRemoveFromCookbook()'
                     class="fa fa-2x heart_pointer"
                     ng-class="{'fa-heart-o': recipe.in_cookbook==0, 'fa-heart': recipe.in_cookbook>0, 'in_cookbook': recipe.in_cookbook>0}"></i>
                  <br>
                </div>
            </div>

            <div class="col-md-8">
<!--                Match-->
                <div ng-if="yes_ingredients.length>0">
                  <h4><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Match: <strong>{{yes_ingredients.length}}/{{ingredients.length}}</strong></h4>
                  <br>

<!--                Ingredients you have-->
                  <h4>
                      <i class="fa fa-thumbs-up"></i>
                      <span>&nbsp;&nbsp;{{yes_ingredients.length!=ingredients.length ? "You have (" : "You have all required ingredients (" }}</span><strong>{{yes_ingredients.length}}</strong><span>):</span>
                  </h4>
                  <span class="label label-primary ingredient_font" ng-repeat="ingredient in yes_ingredients | orderBy: 'name'">
                    {{ingredient.name}}
                  </span>

                  <br><br>

<!--                Ingredients you don't have-->
                  <h4 ng-if="no_ingredients.length>0"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;You have to buy (<strong>{{no_ingredients.length}}</strong>):</h4>
                  <span class="label label-danger ingredient_font" ng-repeat="ingredient in no_ingredients | orderBy: 'name'">
                    {{ingredient.name}}
                  </span>

                </div>

                <div ng-if="yes_ingredients.length==0">
                  <h4>Ingredients (<strong>{{ingredients.length}}</strong>):</h4>
                  <span class="label label-primary ingredient_font" ng-repeat="ingredient in ingredients | orderBy: 'name'">
                    {{ingredient.name}}
                  </span>
                </div>

            </div>

            <div class="col-md-12">
                <hr>
                <h4>Description:</h4>
                <hr>
                <pre class="recipe_description">{{recipe.description}}</pre>
            </div>
        </div>
    </div>

    <?php
    $page->Footer();
    ?>
</div>


<?php
//Scripts==========
echo $page::jquery_script;
echo $page::bootstrap_script;

//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_recipe_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_navigation_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_navigation_factory;
echo $page::angular_user_factory;
echo $page::angular_url_factory;
echo $page::angular_recipes_factory;
?>

</body>