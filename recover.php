<?php
  session_start();
  require_once("php/page_skeleton.php");

  if (isset($_SESSION["user"])){
     header("Location: "."index.php", true, 303);
     die();
  }

  $page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
    //Styles==========
    echo $page::bootstrap_style;
    echo $page::modern_business_style;
    echo $page::font_awesome_style;
    echo $page::admin_main_style;

    //Scripts==========
    echo $page::for_ie_script;
    echo $page::angularjs_script;
    echo $page::angularjs_ng_messages_script;
    ?>
</head>

<body ng-app="rescue"
      ng-cloak
      ng-controller="restore_controller"
    <?php
    if (isset($_SESSION["user"])) {
        echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
    }
    ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<!-- Search recipes area -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="col-md-4"></div>

            <!--            Sign in block-->
            <div class="col-md-4">
                <br><br><br><br><br>
                <h1 class="center_text">Recover</h1>
                <br><br>

                <form name="restoreForm" novalidate ng-cloak>
                    <!--User email-->
                    <fieldset class="form-group">
                        <label class="control-label" for="userName">Email (*):</label>
                        <input
                            required
                            ng-maxlength="100"
                            ng-model="restore.email"
                            ng-class="{'error_red_border' : !restoreForm.userEmail.$valid && restoreForm.userEmail.$dirty}"
                            ng-pattern="/^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/i"
                            name="userEmail"
                            type="text"
                            class="form-control" placeholder="Your email address">
                    </fieldset>
                    <!--User email end-->

                    <fieldset class="form-group">
                        <div
                            class="btn btn-info btn-block"
                            ng-disabled="!restoreForm.$valid"
                            ng-click="RestorePassword(restoreForm)"
                            >
                            Restore&nbsp;&nbsp;<i class="fa fa-life-ring"></i>
                        </div>
                    </fieldset>

                    <!--                    Errors handling-->
                    <fieldset class="form-group">
                        <div ng-messages="restoreForm.userEmail.$error" ng-show="!restoreForm.userEmail.$valid && restoreForm.userEmail.$dirty">
                            <div ng-message="required" class="error_message_text">A user email is required field!</div>
                            <div ng-message="pattern" class="error_message_text">Not valid email address</div>
                            <div ng-message="maxlength" class="error_message_text">A user email must not exceed 100 characters</div>
                        </div>
                    </fieldset>
                    <!--                    Errors handling end-->
                </form>
            </div>
            <!--            Sign in end-->

            <div class="col-md-4"></div>

        </div>
    </div>

    <?php
    $page->Footer();
    ?>
</div>


<?php
//Scripts==========
echo $page::jquery_script;
echo $page::bootstrap_script;

//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_restore_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_navigation_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_navigation_factory;
echo $page::angular_user_factory;
echo $page::angular_url_factory;
?>

</body>