<?php
session_start();
require_once("php/page_skeleton.php");

//if (isset($_SESSION["user"])){
//    header("Location: "."index.php", true, 303);
//    die();
//}

$page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
    //Styles==========
    echo $page::bootstrap_style;
    echo $page::modern_business_style;
    echo $page::font_awesome_style;
    echo $page::admin_main_style;

    //Scripts==========
    echo $page::for_ie_script;
    echo $page::angularjs_script;
    echo $page::angularjs_ng_messages_script;
    ?>
</head>

<body ng-app="rescue"
      ng-cloak
      ng-controller="cuisines_controller"
    <?php
    if (isset($_SESSION["user"])) {
        echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
    }
    ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<!-- Search recipes area -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div ng-show="spinner">
                <br>
                <i class="fa fa-refresh fa-spin fa-2x"></i>
            </div>

            <div ng-if="recipes.length>0">
                <hr>
                <h4>{{title}} (<strong>{{recipes.length}}</strong>)
                <hr>
            </div>
            <div ng-if="recipes.length==0">
                <br>
                <div class="alert alert-warning text-center" role="alert">Didn't find any recipes</div>
            </div>
            <div ng-repeat="recipe in recipes">
                <div recipe_thumbnail_directive
                     user="user_data"
                     recipe="recipe"
                     increment-cookbook="IncrementCookbook()"
                     decrement-cookbook="DecrementCookbook()"></div>
            </div>
        </div>
    </div>

    <?php
    $page->Footer();
    ?>
</div>


<?php
//Scripts==========
echo $page::jquery_script;
echo $page::bootstrap_script;

//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_cuisines_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_navigation_directive;
echo $page::angular_recipe_thumbnail_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_navigation_factory;
echo $page::angular_user_factory;
echo $page::angular_cuisines_factory;
echo $page::angular_url_factory;
?>

</body>