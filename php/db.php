<?php
class db
{
    private static $host="localhost";
    private static $dbname="rescue";
    private static $user = "root";
    private static $pass="";

    public static function GetConnectionString()
    {
        return ("mysql:host=".self::$host.";dbname=".self::$dbname);
    }

    public static function GetUser(){
        return self::$user;
    }

    public static function GetPass(){
        return self::$pass;
    }

    public static function GetPassHash($title){
//        $salt = openssl_random_pseudo_bytes(22);
//        $salt = '$2a$%13$' . strtr(base64_encode($salt), array('_' => '.', '~' => '/'));
//        return crypt($title, $salt);
        return sha1($title);
    }

    public static function GeneratePass($length=8){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
}