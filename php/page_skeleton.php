<?php

class skeleton{
    //Scripts==========
    const for_ie_script = "
                  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                  <!--[if lt IE 9]>
                  <script src='js/for_ie/html5shiw.js'></script>
                  <script src='js/for_ie/respond.js'></script>
                  <![endif]-->
                  ";
    const jquery_script="<script src='bower_components/jquery/dist/jquery.min.js'></script>";
    const bootstrap_script="<script src='bower_components/bootstrap/dist/js/bootstrap.min.js'></script>";
    const index_slider_script="<script src='js/index_slider.js'></script>";

    //Styles==========
    const bootstrap_style = "<link href='bower_components/bootstrap/dist/css/bootstrap.min.css' rel='stylesheet'>";
    const bootstrap_social_style="<link href='bower_components/bootstrap-social/bootstrap-social.css' rel='stylesheet'>";
    const font_awesome_style="<link href='bower_components/font-awesome/css/font-awesome.min.css' rel='stylesheet'>";
    const modern_business_style="<link href='css/modern-business.css' rel='stylesheet'>";
    const slider_style="<link href='css/page_elements.css' rel='stylesheet'>";
    const admin_main_style="<link href='css/admin_main.css' rel='stylesheet'>";


    //---------- Angular + Angular messages
    const angularjs_script="<script src='bower_components/angular/angular.min.js'></script>";
    const angularjs_ng_messages_script="<script src='bower_components/angular-messages/angular-messages.min.js'></script>";

    //========== Angular app files (Dependencies)
    const angular_app="<script src='app/app.js'></script>";

    //---------- Controllers
    const angular_controllers_root="<script src='app/controllers.js'></script>";
    const angular_recipes_controller="<script src='app/controllers/recipes_controller.js'></script>";
    const angular_signinup_controller="<script src='app/controllers/signinup_controller.js'></script>";
    const angular_restore_controller="<script src='app/controllers/restore_controller.js'></script>";
    const angular_cookbook_controller="<script src='app/controllers/cookbook_controller.js'></script>";
    const angular_recipe_controller="<script src='app/controllers/recipe_controller.js'></script>";
    const angular_cuisines_controller="<script src='app/controllers/cuisines_controller.js'></script>";
    const angular_profile_controller="<script src='app/controllers/profile_controller.js'></script>";

    //---------- Directives
    const angular_directives_root="<script src='app/directives.js'></script>";
    const angular_checked_item_directive="<script src='app/directives/checked_item_directive.js'></script>";
    const angular_checked_recipe_directive="<script src='app/directives/checked_recipe_directive.js'></script>";
    const angular_navigation_directive="<script src='app/directives/navigation_directive.js'></script>";
    const angular_recipe_thumbnail_directive="<script src='app/directives/recipe_thumbnail_directive.js'></script>";

    //---------- Factories
    const angular_factories_root="<script src='app/factories.js'></script>";
    const angular_recipes_factory="<script src='app/factories/recipes_factory.js'></script>";
    const angular_navigation_factory="<script src='app/factories/navigation_factory.js'></script>";
    const angular_signinup_factory="<script src='app/factories/signinup_factory.js'></script>";
    const angular_user_factory="<script src='app/factories/user_factory.js'></script>";
    const angular_cuisines_factory="<script src='app/factories/cuisines_factory.js'></script>";
    const angular_url_factory="<script src='app/factories/url_factory.js'></script>";

    public function __construct(){}

    public function Navigation(){}

    private function SliderSocialButtons(){
        echo "
        <div class='carousel-caption'>
                    <a class='btn btn-social btn-facebook btn-xs'>
                        <span class='fa fa-facebook'></span> Sign in with Facebook
                    </a>
                    <a class='btn btn-social btn-twitter btn-xs'>
                        <span class='fa fa-twitter'></span> Sign in with Twitter
                    </a>
                    <a class='btn btn-social btn-google btn-xs'>
                        <span class='fa fa-google'></span> Sign in with Google
                    </a>
                    <p></p>
        </div>
        ";
    }

    private function JoinUsButton(){
        //$scope.joinus_button_show
        echo "<div class='carousel-caption'>";
          echo "<img src='img/website/logo-01.png' width='80' height='80'>";
          echo "<br ng-if='joinus_button_show'><br><br>";
          echo "<h2 ng-if='!joinus_button_show'>Hi <strong>{{user_data.name}}</strong></h2>";
          echo "<a ng-if='joinus_button_show' href='sign_in_up.php' id='join_us_button' class='btn btn-success btn-md btn-lg'>Join us &nbsp;<i class='fa fa-plug'></i></a>";
          echo "<br ng-if='joinus_button_show'><br><br>";
        echo "</div>";
    }

    public function Footer(){
        echo "<footer>";
        echo "<div class='row'>";
            echo "<div class='col-lg-12 text-center'>";
                echo "<p>Copyright &copy; Dinner Rescue 2016</p>";
            echo "</div>";
        echo "</div>";
        echo "</footer>";
    }

    public function Slider(){
        echo "<header id='myCarousel' class='carousel slide'>";

        echo "
        <!-- Indicators -->
        <ol class='carousel-indicators'>
          <li data-target='#myCarousel' data-slide-to='0' class='active'></li>
          <li data-target='#myCarousel' data-slide-to='1'></li>
        </ol>";

//        <li data-target='#myCarousel' data-slide-to='2'></li>

        //Wrapper for slider
        echo "<div class='carousel-inner'>";
          echo "<div class='item active'>";
          echo "<div class='fill' id='third_slide'></div>";
          $this->JoinUsButton();
        echo "</div>";

        echo "<div class='item'>";
          echo "<div class='fill' id='second_slide'></div>";
          echo "<div class='carousel-caption'>";
            echo "<div class='row'>";
              echo "<div class='visible-lg'>";
              echo "<i class='fa fa-line-chart fa-4x'></i>";
              echo "<h2><strong>Website statistic</strong></h2>";
              echo "<hr>";
              echo "</div>";
              echo "<div class='col-lg-4 text-center'>";
                echo "<span class='statistic_font'>Cuisines: <strong class='badge green_badge'>{{statistic['cuisines_amount']}}</strong></span>";
              echo "</div>";

              echo "<div class='col-lg-4 text-center'>";
                echo "<span class='statistic_font'>Ingredients: <strong class='badge green_badge'>{{statistic['ingredients_amount']}}</strong></span>";
              echo "</div>";

              echo "<div class='col-lg-4 text-center'>";
                echo "<span class='statistic_font'>Recipes: <strong class='badge green_badge'>{{statistic['recipes_amount']}}</strong></span>";
              echo "</div>";
              echo "<br><br><br>";
            echo "</div>";
        echo "</div>";

//        echo "
//            <div class='item'>
//                <div class='fill' id='third_slide'></div>
//                <div class='carousel-caption'>Your title here</div>
//            </div>";

        echo "</div>";

        echo "
        <!-- Controls -->
        <a class='left carousel-control' href='#myCarousel' data-slide='prev'>
            <span class='icon-prev'></span>
        </a>
        <a class='right carousel-control' href='#myCarousel' data-slide='next'>
            <span class='icon-next'></span>
        </a>";

        echo "</header>";
    }

    private function CreateSignInElement($social_media_name,$href){
        echo "<li>";
          echo "<a href='".$href."'>";
            echo "<div class='btn btn-social-icon btn-".$social_media_name." btn-xs'>";
              echo "<span class='fa fa-".$social_media_name."'></span>";
            echo "</div>"." ".ucfirst($social_media_name);
          echo "</a>";
        echo "</li>";
    }

    public function GenerateNavigation(){
        $user_logged=true;
        echo "<ul class='nav navbar-nav navbar-right' ng-cloak>";
        $this->GetRecipesNavElement();
        if ($user_logged){
            $this->GetCookBookNavElement();
            $this->GetGreetingNavElement();
        }
        else{
            $this->GetSignInListNavElement();
        }
        echo "</ul>";
    }

    private function GetGreetingNavElement(){
        echo "<li>";
          echo "<a class='white_link'>";
            echo "Hi Jojo &nbsp;<i class='fa fa-user'></i>";
          echo "</a>";
        echo "</li>";
    }

    private function GetCookBookNavElement(){
        echo "<li><a href='#'>My cookbook <span class='badge red_badge'>{{cookbook_amount}}</span></a></li>";
    }

    private function GetRecipesNavElement(){
        //navigation_data_list
        echo "<li class='dropdown'>";
          echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Recipes <b class='caret'></b></a>";
          echo "<ul class='dropdown-menu'>";

          //separate first drop-down item from others
          echo "<li>";
            echo "<a href='#'>{{navigation_data_list[0].name}} <span>({{navigation_data_list[0].amount}})</span></a>";
          echo "</li>";
          echo "<li role='separator' class='divider'></li>";

          //other drop-down items
          echo "<li ng-repeat='item in navigation_data_list | limitTo: navigation_data_list.length : 1'>";
            echo "<a href='#'>{{item.name}}<span>({{item.amount}})</span></a>";
          echo "</li>";

          echo "</ul>";
        echo "</li>";
    }

    private function GetSignInListNavElement(){
        $social_media=[["facebook","#"],["twitter","#"],["google","#"],];

        echo "<li class='dropdown'>";
          echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Sign In <b class='caret'></b></a>";
          echo "<ul class='dropdown-menu'>";

          foreach ($social_media as $value){
              $this::CreateSignInElement($value[0],$value[1]);
          }

          echo "</ul>";
        echo "</li>";
    }

}