<?php
require_once("php/admin_page_skeleton.php");
$page=new Admin_page_skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue (Home)</title>
    <?php
    //========== Styles
    echo $page::bootstrap_style;
    echo $page::sb_admin_style;
    echo $page::font_awesome_style;
    echo $page::admin_panel_style;

    //========== Scripts
    echo $page::for_ie_script;

    //Scripts==========
    echo $page::jquery_script;
    echo $page::bootstrap_script;
    echo $page::angularjs_script;
    echo $page::angularjs_resource_script;
    echo $page::angularjs_ng_messages_script;
    echo $page::angularjs_ng_file_upload_shim;
    echo $page::angularjs_ng_file_upload;
    ?>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <?php
       $page->Nav("Users recipes");
    ?>

    <div id="page-wrapper" ng-app="rescue">

        <div class="container-fluid" ng-controller="users_recipes_controller" ng-cloak>
            <div class="row" ng-if="user_recipe" class="text-block">
                <hr>
                <h4 class="text-center"><strong>{{user_recipe.name}} ({{user_recipe.cuisine_name}})</strong></h4>
                <hr>
                <div class="col-md-3">
                    <img ng-src="{{user_recipe.image_url}}" class="img-thumbnail">
                </div>
                <div class="col-md-15">
                    <h4>Ingredients (<strong>{{user_recipe.ingredients.length}}</strong>):</h4>
                    <span class="label label-primary ingredient_font" ng-repeat="ingredient in user_recipe.ingredients | orderBy: 'ingredient' track by $index">
                      {{ingredient}}
                    </span>
                    <br><br>
                    <pre>{{user_recipe.description}}</pre>
                    <br><br>
                </div>
                <br>
                <button type='button' class='btn btn-info btn-sm center-block' ng-click='HideActiveRecipe()'>Hide&nbsp;&nbsp;<i class="fa fa-eye-slash"></i></button>
                <hr>
            </div>
            <!-- Form block -->
            <div class="row">
                <i ng-show="recipe_spinner" class="fa fa-refresh fa-spin fa-2x"></i>
                <i ng-show="spinner" class="fa fa-refresh fa-spin fa-2x"></i>
                <table class="table table-striped table-bordered">
                    <br>
                    <thead>
                    <tr>
                        <th class="col-md-3">Recipe preview/title</th>
                        <th class="col-md-4">An author (user) data</th>
                        <th class="col-md-8">Options</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr ng-repeat="recipe in recipes">
                        <td>
                            <h5>{{recipe.name}}</h5>
                        </td>
                        <td>
                            <h5><strong>Name:</strong> {{recipe.user_name}}</h5>
                            <h5><strong>Email:</strong> {{recipe.user_email}}</h5>
                        </td>
                        <td>
                            <div user-recipe-row-directive
                                 recipe="recipe"
                                 remove-recipe="RemoveRecipe(recipe_id)"
                                 view-recipe="ViewRecipe(recipe_id)">
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#wrapper -->

    <?php
    //=====Angular app
    echo $page::angular_app;

    //    -----Controllers
    echo $page::angular_controllers_root;
    echo $page::angular_users_recipes_controller;

    //    -----Directives
    echo $page::angular_directives_root;
    echo $page::angular_user_recipe_row_directive;

    //    -----Factories
    echo $page::angular_factories_root;
    echo $page::angular_users_recipes_factory;
    ?>

</body>

</html>
