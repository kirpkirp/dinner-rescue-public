<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/ingredients - GET all ingredients
/api/ingredients/{id} - GET certain ingredient
/api/ingredients/ - POST create new ingredient
/api/ingredients/{id} -  DELETE certain ingredients
/api/ingredients/{id} - PUT update certain ingredients

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/ingredients/ - Get the list of the ingredients
$app->get('/', function (Request $request, Response $response, array $args) {
    //$ingredient = R::find("ingredient");
    $ingredients = R::getAll("SELECT id,name,ingr_category_id FROM ingredient");
    $response=$response->withHeader("Content-Type", "application/json");
    echo json_encode($ingredients);
    return $response;
});

//api/ingredients/{id} - Get certain ingredient
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne("ingredient", "id=?", array($args["id"]));
        if ($ingredient){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($ingredient->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/ingredients/ - Put method (create new ingredient)
$app->put('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //Checking ingredient name (between 3 and 30 characters) and category id
        if (!v::stringType()->length(3,30)->validate($input_data->name) ||
            !v::intVal()->validate($input_data->category_id)
        ){
            throw new Exception();
        }

        //store ingredient
        $ingredient = R::dispense("ingredient");
        $ingredient->name = (string)$input_data->name;
        $ingredient->ingr_category_id = $input_data->category_id;

       // $id = R::store($category);
        R::store($ingredient);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($ingredient->export());
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

///api/ingredients/{id} - DELETE certain ingredient
$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne('ingredient', 'id=?', array($args["id"]));

        if ($ingredient) {
            R::trash($ingredient);
            $response=$response->withStatus(204); // no content
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/ingredients/{id} - Update the ingredient
$app->put('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne("ingredient", "id=?", array($args["id"]));
        if ($ingredient){
            $body = $request->getBody();
            $input_data = json_decode($body);

            //Checking ingredient name (between 3 and 30 characters)
            if (!v::stringType()->length(3,30)->validate($input_data->name)){
                throw new Exception();
            }

            //store ingredient
            $ingredient->name = (string)$input_data->name;

            // $id = R::store($ingredient);
            R::store($ingredient);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($ingredient->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// 404 Not Found
    }
    catch (Exception $e) {
        $response=$response->withStatus(400); // The request could not be understood by the server due to malformed syntax
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();