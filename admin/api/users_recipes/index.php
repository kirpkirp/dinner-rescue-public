<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

$app->get('/', function (Request $request, Response $response, array $args) {
    $users = R::getAll("SELECT r.id,r.name,u.name AS user_name,u.email AS user_email FROM recipeuser r
                        LEFT JOIN user u ON u.id=r.user_id
                        ORDER BY r.id DESC");
    $response=$response->withHeader("Content-Type", "application/json");
    //echo json_encode(R::exportAll($cuisines));
    echo json_encode($users);
    return $response;
});

$app->get('/{id}', function (Request $request, Response $response, array $args) {
    //$recipe=R::findOne("recipeuser", "id=?", array($args["id"]));
    $user_recipe = R::getAll("SELECT id,name,description,cuisine_name,created,ingredients,image_url FROM recipeuser WHERE id=?",
                              array($args["id"]));
    $user_recipe[0]["ingredients"]=explode(',', $user_recipe[0]["ingredients"]);
    for ($i=0;$i<count($user_recipe[0]["ingredients"]);$i++){
        $user_recipe[0]["ingredients"][$i]=trim($user_recipe[0]["ingredients"][$i]);
    }
    $response=$response->withHeader("Content-Type", "application/json");
    //echo json_encode(R::exportAll($cuisines));
    echo json_encode($user_recipe[0]);
    return $response;
});

$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $user_recipe = R::findOne('recipeuser', 'id=?', array($args["id"]));
        R::trash($user_recipe);
        //echo json_encode(R::exportAll($cuisines));
        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($args["id"]);
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/block/{id}', function (Request $request, Response $response, array $args) {
    try {
        $user = R::findOne("user", "id=?", array($args["id"]));
        if ($user){

            $user->active=0;
            R::store($user);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode(array("id"=>$user->id,"active"=>$user->active));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/unblock/{id}', function (Request $request, Response $response, array $args) {
    try {
        $user = R::findOne("user", "id=?", array($args["id"]));
        if ($user){

            $user->active=1;
            R::store($user);

            $response=$response->withHeader("Content-Type", "application/json");
            //echo json_encode($user->export());
            echo json_encode(array("id"=>$user->id,"active"=>$user->active));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();