<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/cuisines - GET all cuisines
/api/cuisines/{id} - GET certain cuisine
/api/cuisines - PUT create new cuisine
/api/cuisines/{id} -  DELETE certain cuisine
/api/cuisines/{id} - PUT update certain cuisine

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/cuisines/ - Get the list of the cuisines
$app->get('/', function (Request $request, Response $response, array $args) {
  //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $cuisines = R::getAll("SELECT id,name FROM cuisine");
    $response=$response->withHeader("Content-Type", "application/json");
    //echo json_encode(R::exportAll($cuisines));
    echo json_encode($cuisines);
    return $response;
});

//api/cuisines/{id} - Get certain cuisine
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $cuisine = R::findOne("cuisine", "id=?", array($args["id"]));
        if ($cuisine){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($cuisine->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/cuisines/ - Post method (create new cuisine)
$app->put('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //Checking cuisine name (between 3 and 30 characters)
        if (!v::stringType()->length(3,30)->validate($input_data->name)){
            throw new Exception();
        }

        //store category
        $cuisine = R::dispense("cuisine");
        $cuisine->name = (string)$input_data->name;
        //$id = R::store($category);
        R::store($cuisine);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");
         echo json_encode($cuisine->export());
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

///api/cuisines/{id} - DELETE certain cuisine
$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $cuisine = R::findOne('cuisine', 'id=?', array($args["id"]));

        if ($cuisine) {
            R::trash($cuisine);
            $response=$response->withStatus(204); // no content
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/cuisines/{id} - Update the cuisine
$app->put('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $cuisine = R::findOne("cuisine", "id=?", array($args["id"]));
        if ($cuisine){
            $body = $request->getBody();
            $input_data = json_decode($body);

            //Checking cuisine name (between 3 and 30 characters)
            if (!v::stringType()->length(3,30)->validate($input_data->name)){
                throw new Exception();
            }

            //store category
            $cuisine->name = (string)$input_data->name;
            // $id = R::store($category);
            R::store($cuisine);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($cuisine->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// 404 Not Found
    }
    catch (Exception $e) {
        $response=$response->withStatus(400); // The request could not be understood by the server due to malformed syntax
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();