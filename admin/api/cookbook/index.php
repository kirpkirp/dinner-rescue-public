<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*
/api/cookbook/ - GET all recipes
/api/ingredients/{id} - GET certain ingredient
/api/ingredients/ - POST create new ingredient {"name": "...", "category_id": "..."}
/api/ingredients/{id} -  DELETE certain ingredients
/api/ingredients/{id} - PUT update certain ingredients {"name": "...", "category_id": "..."}
*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/ingredients/ - Get the list of the ingredients
$app->get('/', function (Request $request, Response $response, array $args) {
    $ingredient = R::find("ingredient");
    $response=$response->withHeader("Content-Type", "application/json");

    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($ingredient));
    return $response;
});

//api/ingredients/{id} - Get certain ingredient
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne("ingredient", "id=?", array($args["id"]));
        if ($ingredient){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode(R::exportAll($ingredient));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/ingredients/ - Post method (create new ingredient)
$app->post('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //store category
        $ingredient = R::dispense("recipe");

        $ingredient->name = (string)$input_data->name;
        $ingredient->ingr_category_id = (string)$input_data->category_id;

       // $id = R::store($category);
        R::store($ingredient);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");
         echo json_encode(R::exportAll($ingredient));
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

///api/ingredients/{id} - DELETE certain ingredient
$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne('ingredient', 'id=?', array($args["id"]));

        if ($ingredient) {
            R::trash($ingredient);
            $response=$response->withStatus(204); // no content
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/ingredients/{id} - Update the ingredient
$app->put('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $ingredient = R::findOne("recipe", "id=?", array($args["id"]));
        if ($ingredient){
            $body = $request->getBody();
            $input_data = json_decode($body);

            //store category
            $ingredient->name = (string)$input_data->name;
            $ingredient->ingr_category_id = (string)$input_data->category_id;

            // $id = R::store($category);
            R::store($ingredient);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode(R::exportAll($ingredient));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// 404 Not Found
    }
    catch (Exception $e) {
        $response=$response->withStatus(400); // The request could not be understood by the server due to malformed syntax
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();