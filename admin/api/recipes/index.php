<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');
require_once('../../php/recipe_data_checker.php'); // require all files above

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*
/api/recipes - GET all recipes
/api/recipes/{id} - GET certain recipe
/api/recipes/ - POST create new recipe {"name": "...", "description": "...", "video_url": "...", "cuisine_id": "..."}
/api/recipes/{id} -  DELETE certain recipe
/api/recipes/{id} - PUT update certain recipe {"name": "...", "description": "...", "video_url": "...", "cuisine_id": "..."}
*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

$app->get("/detailed_recipe/{id}",function(Request $request, Response $response, array $args){
    try{
        $recipe_id=$args["id"];
        $result_array=array(
           "recipe"=>array(),
           "ingredients"=> array()
        );

        if ($recipe_id>0){
            $current_recipe=R::getAll("SELECT id,name,description,cuisine_id FROM recipe WHERE id=?",array($recipe_id));

            $current_ingredients=R::getAll("SELECT i.id,i.name FROM dish as d
                                            LEFT JOIN ingredient as i ON d.ingredient_id=i.id
                                            WHERE d.recipe_id=?",array($recipe_id));

            $result_array["recipe"]=$current_recipe[0];
            $result_array["ingredients"]=$current_ingredients;
        }

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($result_array);
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
        echo $e->getMessage();
    }
    return $response;

});

//api/recipes/ - Get the list of the recipes
$app->get('/', function (Request $request, Response $response, array $args) {
    $recipes = R::getAll("SELECT id,name FROM recipe ORDER BY id DESC LIMIT 50");
    $response=$response->withHeader("Content-Type", "application/json");

    // return JSON-encoded response body with query results
    //echo json_encode(R::exportAll($recipes));
    echo json_encode($recipes);
    return $response;
});

//api/recipes/{id} - Get certain recipe
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $recipe = R::findOne("recipe", "id=?", array($args["id"]));
//        $recipe->description=htmlentities($recipe->description,ENT_QUOTES);
//        $recipe->description="sd sad asdsad";
        if ($recipe){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode(R::exportAll($recipe));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/recipes/update/ - update a recipe
$app->post('/update/', function (Request $request, Response $response, array $args){
    try {
        $recipe_data_checker=new RecipeDataChecker(
            isset($_FILES["file"]) ? $_FILES["file"] : null,
            $_POST["recipeObj"],
            $_POST["ingredientsObj"],
            $_POST["cuisineID"]);

        $recipe_id=v::intVal()->validate($_POST["recipeID"]) ? $_POST["recipeID"] : 0;

        if (
            $recipe_data_checker->CheckName() &&
            $recipe_data_checker->CheckDescription() &&
            $recipe_data_checker->CheckIngredientsArray() &&
            $recipe_data_checker->CheckCuisineID()
        ){
            //----------Store recipe data
            $recipe = R::load("recipe",$recipe_id);

            $recipe->name = $recipe_data_checker->getRecipeName();
            $recipe->description = $recipe_data_checker->getRecipeDescription();
           // $recipe->video_url= (!$recipe_data_checker->CheckURL()) ? null : $recipe_data_checker->getRecipeURL();
            $recipe->cuisine_id=$recipe_data_checker->getRecipeCuisineID();
            //$recipe->active=1; //recipe available online immediately
            $recipe->updated=(new DateTime())->format("Y-m-d H:i:s");

            $recipe_id = R::store($recipe);

            if ($recipe_data_checker->CheckImage()){
                $recipe_data_checker->SaveImage($recipe_id);
            }

            //Remove previous recipe ingredients
            //R::findOne('recipe', 'id=?', array($args["id"]));
            $previous_ingredients=R::findAll("dish", "recipe_id=?",array($recipe_id));
            R::trashAll($previous_ingredients);

            //----------Save recipe ingredients
            $recipe_ingredients=$recipe_data_checker->getRecipeIngredients();
            foreach ($recipe_ingredients as $ingr){
                $ingredient=R::dispense("dish");
                $ingredient->recipe_id=$recipe_id;
                $ingredient->ingredient_id=$ingr;
                R::store($ingredient);
            }
        }
        else{
            throw new Exception("An error occurred! Check data.");
        }
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
        echo $e->getMessage();
    }
    return $response;
});

//api/recipes/ - create new recipe
$app->post('/', function (Request $request, Response $response, array $args){
    try {
        $recipe_data_checker=new RecipeDataChecker($_FILES["file"],
                                                   $_POST["recipeObj"],
                                                   $_POST["ingredientsObj"],
                                                   $_POST["cuisineID"]);

        if (
            $recipe_data_checker->CheckImage() &&
            $recipe_data_checker->CheckName() &&
            $recipe_data_checker->CheckDescription() &&
            $recipe_data_checker->CheckIngredientsArray() &&
            $recipe_data_checker->CheckCuisineID()
        ){
            //----------Store recipe data
            $recipe = R::dispense("recipe");

            $recipe->name = $recipe_data_checker->getRecipeName();
            $recipe->description = $recipe_data_checker->getRecipeDescription();
            //$recipe->video_url= (!$recipe_data_checker->CheckURL()) ? null : $recipe_data_checker->getRecipeURL();
            $recipe->cuisine_id=$recipe_data_checker->getRecipeCuisineID();
            //$recipe->active=1; //recipe available online immediately
            $recipe->created=(new DateTime())->format("Y-m-d H:i:s");
            $recipe->updated=(new DateTime())->format("Y-m-d H:i:s");
            $recipe_id = R::store($recipe);

            $recipe_data_checker->SaveImage($recipe_id);

            //----------Save recipe ingredients
            $recipe_ingredients=$recipe_data_checker->getRecipeIngredients();
            foreach ($recipe_ingredients as $ingr){
                $ingredient=R::dispense("dish");
                $ingredient->recipe_id=$recipe_id;
                $ingredient->ingredient_id=$ingr;
                R::store($ingredient);
            }
        }
        else{
              throw new Exception("An error occurred! Check data.");
        }
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
        echo $e->getMessage();
    }
    return $response;
});

///api/recipes/{id} - DELETE certain recipe
$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $recipe = R::findOne('recipe', 'id=?', array($args["id"]));

        if ($recipe) {
            //save recipe id before it removed
            $recipe_id=$recipe["id"];

            R::trash($recipe);
            $folder_name=substr($recipe_id,-1);
            $fool_image_path=RecipeDataChecker::root_images_path.$folder_name."/".$recipe_id.".jpg";

            if (file_exists($fool_image_path)) {
                unlink($fool_image_path);
            }

            $response=$response->withStatus(204); // no content
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();