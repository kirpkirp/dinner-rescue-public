<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/categories - GET all categories
/api/categories/{id} - GET certain category
/api/categories/ - PUT create new category
/api/categories/{id} -  DELETE certain category
/api/categories/{id} - PUT update certain category

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/categories/ - Get the list of the categories
$app->get('/', function (Request $request, Response $response, array $args) {
  //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $categories = R::find("categor");

  //  $res->header('Content-Type', 'application/json');
    //$response->headers->set('Content-Type', 'application/json'); // send response header for JSON content type
    $response=$response->withHeader("Content-Type", "application/json");


    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($categories));
    return $response;

//        $category = R::dispense("categor");
//        $category->name = "Santa Claus";
//        $id = R::store($category);
//        R::close();
});

//api/categories/{id} - Get certain category
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    try {
        $category = R::findOne("categor", "id=?", array($args["id"]));
        if ($category){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($category->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/categories/ - create new category
$app->put('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //Checking category name (between 3 and 30 characters)
        if (!v::stringType()->length(3,30)->validate($input_data->name)){
            throw new Exception();
        }

        //store category
        $category = R::dispense("categor");
        $category->name = (string)$input_data->name;
       // $id = R::store($category);
        R::store($category);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");

        //echo json_encode(R::exportAll($category));
        echo json_encode($category->export());
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

///api/categories/{id} - DELETE certain category
$app->delete('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $category = R::findOne('categor', 'id=?', array($args["id"]));

        if ($category) {
            R::trash($category);
            $response=$response->withStatus(204); // no content
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $response=$response->withStatus(404);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/categories/{id} - Update the category
$app->put('/{id}', function (Request $request, Response $response, array $args) {
    try {
        $category = R::findOne("categor", "id=?", array($args["id"]));
        if ($category){
            $body = $request->getBody();
            $input_data = json_decode($body);

            //Checking category name (between 3 and 30 characters)
            if (!v::stringType()->length(3,30)->validate($input_data->name)){
                throw new Exception();
            }

            //store category
            $category->name = (string)$input_data->name;

            // $id = R::store($category);
            R::store($category);

            $response=$response->withHeader("Content-Type", "application/json");
          //  echo json_encode(R::exportAll(array($category)));
            echo json_encode($category->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// 404 Not Found
    }
    catch (Exception $e) {
        $response=$response->withStatus(400); // The request could not be understood by the server due to malformed syntax
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();