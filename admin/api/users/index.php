<?php
require_once('../../../vendor/autoload.php');
require_once('../../../php/rb.php');
require_once('../../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/cuisines - GET all cuisines
/api/cuisines/{id} - GET certain cuisine
/api/cuisines - PUT create new cuisine
/api/cuisines/{id} -  DELETE certain cuisine
/api/cuisines/{id} - PUT update certain cuisine

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/cuisines/ - Get the list of the cuisines
$app->get('/', function (Request $request, Response $response, array $args) {
  //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $users = R::getAll("SELECT id,name,email,active FROM user ORDER BY id DESC");
    $response=$response->withHeader("Content-Type", "application/json");
    //echo json_encode(R::exportAll($cuisines));
    echo json_encode($users);
    return $response;
});

//api/cuisines/{id} - Get certain cuisine
$app->post('/block/{id}', function (Request $request, Response $response, array $args) {
    try {
        $user = R::findOne("user", "id=?", array($args["id"]));
        if ($user){

            $user->active=0;
            R::store($user);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode(array("id"=>$user->id,"active"=>$user->active));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/unblock/{id}', function (Request $request, Response $response, array $args) {
    try {
        $user = R::findOne("user", "id=?", array($args["id"]));
        if ($user){

            $user->active=1;
            R::store($user);

            $response=$response->withHeader("Content-Type", "application/json");
            //echo json_encode($user->export());
            echo json_encode(array("id"=>$user->id,"active"=>$user->active));
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();