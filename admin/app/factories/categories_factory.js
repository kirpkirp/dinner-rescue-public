angular.module("rescue.factories").
    factory("categories_factory", function($resource) {
        return $resource('api/categories/:id',
            {},
            {
                get: {
                  method: "GET",
                  isArray: true
                 },
                update: {
                  method: "PUT",
                  isArray: false
                },
                create:{
                    method: "PUT",
                    isArray: false
                }
            });
    });
