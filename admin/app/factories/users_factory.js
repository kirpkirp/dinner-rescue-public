angular.module("rescue.factories").
    factory("users_factory", function($http) {
        return{
            GetUsers:
                function(){
                    return $http({method: "GET", url: "api/users/"});
                },

            BlockUser:
                function(user_id){
                    return $http({method: "POST", url: "api/users/block/"+user_id});
                },

            UnblockUser:
                function(user_id){
                    return $http({method: "POST", url: "api/users/unblock/"+user_id});
                }
        };
    });

