angular.module("rescue.factories").
    factory("ingredients_factory", function($resource) {
        return $resource('api/ingredients/:id',
            {},
            {
                get: {
                    method: "GET",
                    isArray: true
                },
                update: {
                    method: "PUT",
                    isArray: false
                },
                create:{
                    method: "PUT",
                    isArray: false
                }
            });
    });
