angular.module("rescue.factories").
    factory("recipes_factory", function(Upload,$http) {
        return{
            CreateRecipe:

                function(recipeImage, recipeObject, ingredientsObjectList,cuisineID){
                    //get ids of ingredients from ingredientsObjectList
                    //var ingredientsList=[];
                    //for (var i=0;i<ingredientsObjectList.length;i++){
                    //    ingredientsList.push(ingredientsObjectList[i].obj.id);
                    //}

                    return Upload.upload({
                           url:"api/recipes/",
                           data: {
                               file: recipeImage,
                               recipeObj: recipeObject,
                               ingredientsObj: ingredientsObjectList,
                               cuisineID: cuisineID
                           }
                    });
                },

            UpdateRecipe:
                function(recipeImage, recipeObject, ingredientsObjectList,cuisineID,recipeID){
                    //get ids of ingredients from ingredientsObjectList
                    //var ingredientsList=[];
                    //for (var i=0;i<ingredientsObjectList.length;i++){
                    //    ingredientsList.push(ingredientsObjectList[i].obj.id);
                    //}

                    return Upload.upload({
                        url:"api/recipes/update/",
                        data: {
                            file: recipeImage,
                            recipeObj: recipeObject,
                            ingredientsObj: ingredientsObjectList,
                            cuisineID: cuisineID,
                            recipeID: recipeID
                        }
                    });
                },

            GetDetailedRecipe:
                function(currentRecipeID){
                  return $http({method: "GET", url: "api/recipes/detailed_recipe/"+currentRecipeID})
                },

            GetRecipes:
                function(){
                    return $http({method: "GET",url: "api/recipes/"});
                },

            RemoveRecipe:
                function(recipeID){
                    return $http({method: "DELETE", url: "api/recipes/"+recipeID})
                }
        };
    });
