angular.module("rescue.factories").
    factory("cuisines_factory", function($resource) {
        return $resource('api/cuisines/:id',
            {},
            {
                get: {
                  method: "GET",
                  isArray: true
                 },
                update: {
                  method: "PUT",
                  isArray: false
                },
                create:{
                  method: "PUT",
                  isArray: false
                }
            });
    });
