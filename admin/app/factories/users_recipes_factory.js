angular.module("rescue.factories").
    factory("users_recipes_factory", function($http) {
        return{
            GetRecipes:
                function(){
                    return $http({method: "GET", url: "api/users_recipes/"});
                },

            GetRecipe:
                function(recipe_id){
                    return $http({method: "GET", url: "api/users_recipes/"+recipe_id});
                },

            RemoveRecipe:
                function(recipe_id){
                    return $http({method: "DELETE", url: "api/users_recipes/"+recipe_id});
                }
        };
    });


