angular.module("rescue.controllers").
    controller("users_controller", function ($scope, users_factory) {
        //Show spinner loading icon
       $scope.spinner = true;

        //Get cuisines list
        users_factory.GetUsers().then(
            //success
            function successCallback(response) {
                $scope.spinner=false;
                $scope.users=response.data;
            },
            //error
            function errorCallback(response) {
                $scope.spinner=false;
                alert("Sorry, can't retrieve users data from a data base.");
            });
    });