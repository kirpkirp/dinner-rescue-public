angular.module("rescue.controllers").
    controller("ingredients_controller", function ($scope, categories_factory, ingredients_factory,$filter) {
        //Show spinner loading icon (categories and ingredients sections)
        $scope.categories_spinner=true;
        $scope.ingredients_spinner = true;

        //Create new ingredient button state
        $scope.create_button = {disabled: false, text: "Create"};

        //current active ingredients category
        $scope.active_category_scope=null;

        //filter function
        $scope.filterIngredients = function(element) {
            return ($scope.active_category_scope==null) ? false : element.ingr_category_id==$scope.active_category_scope.obj.id;
        };

        //Get categories list
        categories_factory.query({},
            //success
            function (categories_list) {
                if (categories_list.length>0){

                    for (var i=0;i<categories_list.length;i++){
                        categories_list[i]["active"]=false;
                    }

                    $scope.categories =$filter("orderBy")(categories_list, "name");
                    $scope.categories[0]["active"]=true;
                }

              $scope.categories_spinner = false; // hide categories_spinner
              //get all available ingredients
              $scope.get_ingredients();
            }
        );

        $scope.get_ingredients=function(){
            ingredients_factory.query({},
                //success
                function (ingredients_list) {
                    $scope.ingredients = ingredients_list;
                    $scope.ingredients_spinner = false; // hide ingredients_spinner
                }
            );
        }

        $scope.clickOnCategory=function(item_scope){
            if ($scope.active_category_scope!=null){
                $scope.active_category_scope.checked=false;
            }
            $scope.active_category_scope=item_scope;
            //select current category elements as an active
            item_scope.checked=true;

            //item_scope.obj={id:"...",name:"..."}
        }

        //Create new category
        $scope.createIngredient = function (ingredientForm) {

            if ($scope.active_category_scope==null){
                alert("Chose the ingredients group!");
                return;
            }
            if (ingredientForm.$valid) {
                $scope.create_button = {disabled: true, text: "..."};
                ingredients_factory.create({}, {name: $scope.obj.ingredient, category_id: $scope.active_category_scope.obj.id},
                    function (created_ingredient_obj) {
                        $scope.ingredients.push(created_ingredient_obj); // add created ingredient to collection
                        $scope.create_button = {disabled: false, text: "Create"};
                        //reset the form
                        alert("New ingredient created");
                    },
                    //Error
                    function () {
                        $scope.create_button = {disabled: false, text: "Create"};
                        alert("Can't create new ingredient, an error has occurred");
                    }
                );
            }
        }

        $scope.removeIngredient = function (item_scope) {
            //Delete ingredient by id
            if (confirm("Do you really want to remove this item?")) {
                var item_id = item_scope.obj.id;

                ingredients_factory.delete({id: item_id}, {},
                    //Success
                    function () {
                        for (var i = 0; i < $scope.ingredients.length; i++) {
                            if ($scope.ingredients[i].id == item_id) {
                                $scope.ingredients.splice(i, 1);
                                break;
                            }
                        }
                    },
                    //Error
                    function () {
                        item_scope.buttons_state.delete={disabled:false, title:"Delete"};
                        alert("Can't remove the ingredient, an error has occurred");
                    }
                );
            }
        }
        $scope.updateIngredient = function (item_scope) {
            var item_id=item_scope.obj.id;
            var item_name=item_scope.current_name;
            ingredients_factory.update({id: item_id}, {name: item_name},
                //Success
                function () {
                    for (var i = 0; i < $scope.ingredients.length; i++) {
                        if ($scope.ingredients[i].id == item_id) {
                            $scope.ingredients[i].name=item_name;
                            alert("The ingredients name has been changed")
                            break;
                        }
                    }
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    item_scope.expand=false;
                },
                //Error
                function (){
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    alert ("Can't change the ingredients name, an error has occurred");
                }
            );
        }
    });