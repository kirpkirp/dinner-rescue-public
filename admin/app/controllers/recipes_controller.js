angular.module("rescue.controllers").
    controller("recipes_controller", function ($scope,
                                               cuisines_factory,
                                               categories_factory,
                                               ingredients_factory,
                                               recipes_factory,
                                               $filter
    ) {
        //Show spinner loading icon (categories and ingredients sections)
        $scope.spinner={categories:true,
                        ingredients:true,
                        cuisines:true,
                        recipes:true};

        //Create new ingredient button state
        //$scope.create_button = {disabled: false, text: $scope.main_button_name};

        $scope.active_cuisine_scope=null;
        $scope.active_category_scope=null;

        //active ingredients list
        $scope.ingredients_obj= {
            list:[],
            amount:{max:10,min:3}
        };


        var description_watcher=$scope.$watch("recipe_id",function(){
            if ($scope.recipe_id!=0){
                var recipe_id_number=parseInt($scope.recipe_id);
                recipes_factory.GetDetailedRecipe(recipe_id_number).then(
                    function(data, status, headers, config){
                        //success
                        var recipe=data["data"]["recipe"];
                        $scope.obj.ingredients=data["data"]["ingredients"];
                        $scope.obj.recipe_name=recipe["name"];
                        $scope.obj.recipe_description=recipe["description"];
                        $scope.obj.recipe_url=recipe["video_url"];

                        $scope.obj.active_cuisine_id=recipe["cuisine_id"];
                    },
                    function(data, status, headers, config){
                        //error
                        alert("An error has occurred!");
                    },
                    function(progr){
                        //progr.loaded, progr.total
                    }
                );
            }
            description_watcher();
        });

        //filter function
        $scope.filterIngredients = function(element) {
            return ($scope.active_category_scope==null) ? false : element.ingr_category_id==$scope.active_category_scope.obj.id;
        };

        //Get cuisines list
        $scope.GetCuisines=function(){
            cuisines_factory.query({},
                //success
                function (cuisines_list) {
                    if (cuisines_list.length>0){

                        //cuisines_list[i]["id"]==$scope.obj.active_cuisine_id;
                        $scope.cuisines =$filter("orderBy")(cuisines_list, "name");

                        //if we are updating recipe
                        if (typeof($scope.obj.active_cuisine_id) !== 'undefined'){
                            //choosing proper cuisine group
                            for (var j=0;j<cuisines_list.length;j++){
                                cuisines_list[j]["active"]=cuisines_list[j]["id"]==$scope.obj.active_cuisine_id;
                            }
                        }
                        else{
                            for (var i=0;i<cuisines_list.length;i++){
                                cuisines_list[i]["active"]=false;
                            }
                            //choosing first cuisine group
                            $scope.cuisines[0]["active"]=true;
                        }

                        //$scope.cuisines =$filter("orderBy")(cuisines_list, "name");
                        //$scope.cuisines[0]["active"]=true;
                    }

                    $scope.spinner.cuisines = false; // hide cuisines_spinner
                }
            );
        };

        $scope.GetCuisines();

        //Get categories list
        categories_factory.query({},
            //success
            function (categories_list) {
                if (categories_list.length>0){

                    for (var i=0;i<categories_list.length;i++){
                        categories_list[i]["active"]=false;
                    }

                    $scope.categories =$filter("orderBy")(categories_list, "name");
                    $scope.categories[0]["active"]=true;
                }
                $scope.spinner.categories = false; // hide categories_spinner

                //get all available ingredients
                $scope.get_ingredients();
            }
        );

        $scope.get_ingredients=function(){
            ingredients_factory.query({},
                //success
                function (ingredients_list) {
                    $scope.ingredients = ingredients_list;
                    $scope.spinner.ingredients = false; // hide ingredients_spinner

                    //all ingredients from start unselected
                    for (var z=0;z<$scope.ingredients.length;z++){
                        $scope.ingredients[z]["selected"]=false;
                    }

                    $scope.SetMainButton();

                    ////when update recipe mode is active
                    //$scope.obj.ingredients=data["data"]["ingredients"];


                    if ($scope.obj.ingredients){
                        for (var i=0;i<$scope.obj.ingredients.length;i++){
                            for (var j=0;j<$scope.ingredients.length;j++){
                                if ($scope.obj.ingredients[i]["id"]==$scope.ingredients[j]["id"]){
                                    $scope.ingredients[j]["selected"]=true;
                                    $scope.ingredients_obj.list.push($scope.ingredients[j]);
                                }
                            }
                        }
                    }
                }
            );
        };

        $scope.clickOnCuisine=function(item_scope){
            if ($scope.active_cuisine_scope!=null){
                $scope.active_cuisine_scope.checked=false;
            }
            $scope.active_cuisine_scope=item_scope;
            //select current category elements as an active
            item_scope.checked=true;
            //item_scope.obj={id:"...",name:"..."}
        }

        $scope.clickOnIngredient=function(item_obj){
            //$scope.ingredients_obj= {
            //    list:[],
            //    amount:{max:10,min:3}
            //};

            //arr.splice( index, 1 );
            var mas=$scope.ingredients_obj.list;

            if (item_obj.selected){
               mas.push(item_obj);
                //mas.push(item_scope);
            }
            else{
                for (var i=0;i<mas.length;i++){
                    if (mas[i].id==item_obj.id){
                        mas.splice(i,1);
                        break;
                    }
                }
            }
        };

        $scope.clickOnCategory=function(item_scope){
            if ($scope.active_category_scope!=null){
                $scope.active_category_scope.checked=false;
            }
            $scope.active_category_scope=item_scope;
            //select current category elements as an active
            item_scope.checked=true;
            //item_scope.obj={id:"...",name:"..."}
        };

        $scope.ResetForm=function(){
            $scope.obj.recipe_name=null;
            //$scope.obj.recipe_url=null;
            $scope.obj.recipe_description=null;
            $scope.pic_file=null;
            for (var i=0;i<$scope.ingredients.length;i++){
                $scope.ingredients[i].selected=false;
            }
            $scope.ingredients_obj.list=[];
        };

        $scope.SetMainButton=function(){
            $scope.create_button={disabled: false, text: $scope.obj.ingredients ? "Update" : "Create"};
        };

        //create recipe
        $scope.CreateRecipe=function(ingredients_id_list){
            //Create recipe
            recipes_factory.CreateRecipe(
                $scope.pic_file,
                $scope.obj,
                ingredients_id_list,
                $scope.active_cuisine_scope.obj["id"]
            ). then(function(resp){
                    //success
                    $scope.SetMainButton();
                    $scope.ResetForm();
                    $scope.GetRecipes();
                    alert("The recipe has been processed!");
                },
                function(resp){
                    //error
                    $scope.SetMainButton();
                    alert("An error has occurred!");
                },
                function(progr){
                    //progr.loaded, progr.total
                }
            );
        };

        //update recipe
        $scope.UpdateRecipe=function(ingredients_id_list){
            //Update recipe
            recipes_factory.UpdateRecipe(
                $scope.pic_file,
                $scope.obj,
                ingredients_id_list,
                $scope.active_cuisine_scope.obj["id"],
                $scope.recipe_id
            ).then(function(resp){
                    //success
                    //$scope.ResetForm();
                    //$scope.GetRecipes();
                    window.location = "recipes.php";
                    alert("The recipe has been processed!");
                },
                function(resp){
                    //error
                    $scope.SetMainButton();
                    alert("An error has occurred!");
                },
                function(progr){
                    //progr.loaded, progr.total
                }
            );
        };

        //Create new category
        $scope.createUpdateRecipe = function (recipeForm) {
           var valid_recipe_form=recipeForm.$valid &&
                                 $scope.ingredients_obj.list.length>=$scope.ingredients_obj.amount.min &&
                                 $scope.ingredients_obj.list.length<=$scope.ingredients_obj.amount.max;
           $scope.create_button={disabled: true, text: "..."};

            if (valid_recipe_form){
                var ingredients_id_list=[];
                for (var i=0;i<$scope.ingredients_obj.list.length;i++){
                    ingredients_id_list.push($scope.ingredients_obj.list[i].id);
                }

                if ($scope.obj.ingredients){
                    $scope.UpdateRecipe(ingredients_id_list);
                }
                else{
                    $scope.CreateRecipe(ingredients_id_list);
                }
            }
        };

        $scope.removeRecipe = function(recipe_id){
            //Delete ingredient by id
            if (confirm("Do you really want to remove this item?")) {
                recipes_factory.RemoveRecipe(recipe_id).
                    then(
                    //success
                    function successCallback(response) {
                        $scope.ResetForm();
                        $scope.GetRecipes();
                    },
                    //error
                    function errorCallback(response) {
                        alert("Sorry, can't retrieve recipes from data base.");
                    });
            }
        };

        //Get all recipes
        $scope.GetRecipes=function(){
            $scope.spinner.recipes=true;
            recipes_factory.GetRecipes().
                then(
                //success
                function successCallback(response) {
                    $scope.spinner.recipes=false;
                    $scope.recipes=response.data;
                },
                //error
                function errorCallback(response) {
                    $scope.spinner.recipes=false;
                    alert("Sorry, can't retrieve recipes from data base.");
                });
        };

        $scope.GetRecipes();
    });