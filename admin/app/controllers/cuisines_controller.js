angular.module("rescue.controllers").
    controller("cuisines_controller", function ($scope, cuisines_factory) {
        //Show spinner loading icon
        $scope.spinner = true;
        //Create new cuisines button state
        $scope.create_button = {disabled: false, text: "Create"};

        //Get cuisines list
        cuisines_factory.query({}, function (cuisines_list) {
            $scope.cuisines = cuisines_list;
            $scope.spinner = false; // hide spinner
        });

        //Get category by id
        //cuisines_factory.get({id:2},function(ttt){
        //    //console.log(ttt);
        //    $scope.categories=ttt;
        //});

        //Create new category
        $scope.createCuisine = function (cuisineForm) {

            if (cuisineForm.$valid) {
                $scope.create_button = {disabled: true, text: "..."};
                cuisines_factory.create({}, {name: $scope.obj.cuisine},
                    function (created_cuisine_obj) {
                        $scope.cuisines.push(created_cuisine_obj); // add elements to collection
                        $scope.create_button = {disabled: false, text: "Create"};
                        alert("New cuisine created");
                    },
                    //Error
                    function () {
                        $scope.create_button = {disabled: false, text: "Create"};
                        alert("Can't create new cuisine, an error has occurred");
                    }
                );
            }
        }

        //Remove category
        $scope.removeCuisine = function (item_scope) {
            //Delete cuisine by id
            if (confirm("Do you really want to remove this item?")) {
                var item_id = item_scope.obj.id;

                cuisines_factory.delete({id: item_id}, {},
                    //Success
                    function () {
                        for (var i = 0; i < $scope.cuisines.length; i++) {
                            if ($scope.cuisines[i].id == item_id) {
                                $scope.cuisines.splice(i, 1);
                                break;
                            }
                        }
                    },
                    //Error
                    function () {
                        item_scope.buttons_state.delete={disabled:false, title:"Delete"};
                        alert("Can't remove the cuisine, an error has occurred");
                    }
                );
            }
        }

        $scope.updateCuisine = function (item_scope) {
            var item_id=item_scope.obj.id;
            var item_name=item_scope.current_name;
            cuisines_factory.update({id: item_id}, {name: item_name},
                //Success
                function () {
                    for (var i = 0; i < $scope.cuisines.length; i++) {
                        if ($scope.cuisines[i].id == item_id) {
                            $scope.cuisines[i].name=item_name;
                            alert("The cuisine name has been changed")
                            break;
                        }
                    }
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    item_scope.expand=false;
                },
                //Error
                function (){
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    alert ("Can't change the cuisine name, an error has occurred");
                }
            );
        }

    });