angular.module("rescue.controllers").
    controller("users_recipes_controller", function ($scope, users_recipes_factory) {
        $scope.spinner = true;

        $scope.recipe_spinner=false;

        $scope.user_recipe=null;

        $scope.ViewRecipe=function(recipe_id){
           $scope.recipe_spinner=true;
           users_recipes_factory.GetRecipe(recipe_id).then(
               //success
               function successCallback(response) {
                   $scope.user_recipe=response.data;
                   $scope.recipe_spinner=false;
               },
               //error
               function errorCallback(response) {
                   $scope.recipe_spinner=false;
                   alert("Sorry, can't retrieve a current recipe from a data base.");
               });
        };

        $scope.RemoveRecipe=function(recipe_id){
            if (!confirm("Do you really want to remove this recipe?")) return;

            users_recipes_factory.RemoveRecipe(recipe_id).then(
                //success
                function successCallback(response){
                    if (!($scope.user_recipe==null)){
                        if (response.data==$scope.user_recipe.id){
                            $scope.user_recipe=null;
                        }
                    }

                    //remove user recipe from table
                    for (var i=0;i<$scope.recipes.length;i++){
                        if ($scope.recipes[i].id==response.data){
                            $scope.recipes.splice(i, 1);
                            return;
                        }
                    }
                },
                //error
                function errorCallback(response) {
                    //$scope.recipe_spinner=false;
                    alert("Sorry, can't retrieve a current recipe from a data base.");
                });
        };

        $scope.HideActiveRecipe=function(){
            $scope.user_recipe=null;
        };

        //Get cuisines list
        users_recipes_factory.GetRecipes().then(
            //success
            function successCallback(response) {
                $scope.spinner=false;
                $scope.recipes=response.data;
            },
            //error
            function errorCallback(response) {
                $scope.spinner=false;
                alert("Sorry, can't retrieve user's recipes data from a data base.");
            });
    });