angular.module("rescue.controllers").
    controller("categories_controller", function ($scope, categories_factory) {
        //Show spinner loading icon
        $scope.spinner = true;
        //Create new category button state
        $scope.create_button = {disabled: false, text: "Create"};

        //Get categories list
        categories_factory.query({}, function (categories_list) {
            $scope.categories = categories_list;
            $scope.spinner = false; // hide spinner
        });

        //Get category by id
        //categories_factory.get({id:2},function(ttt){
        //    //console.log(ttt);
        //    $scope.categories=ttt;
        //});

        //Create new category
        $scope.createCategory = function (categoryForm) {

            if (categoryForm.$valid) {
                $scope.create_button = {disabled: true, text: "..."};
                categories_factory.create({}, {name: $scope.obj.category},
                    function (created_category_obj) {
                        $scope.categories.push(created_category_obj); // add elements to collection
                        $scope.create_button = {disabled: false, text: "Create"};
                        alert("New ingredient group created");
                    },
                    //Error
                    function () {
                        $scope.create_button = {disabled: false, text: "Create"};
                        alert("Can't create new ingredient group, an error has occurred");
                    }
                );
            }
        }

        //Remove category
        $scope.removeCategory = function (item_scope) {
            //Delete category by id
            if (confirm("Do you really want to remove this item?")) {
                // $scope.removeObj({category_id: $scope.obj.id});
                var item_id = item_scope.obj.id;

                categories_factory.delete({id: item_id}, {},
                    //Success
                    function () {
                        console.log("success remove");
                        for (var i = 0; i < $scope.categories.length; i++) {
                            if ($scope.categories[i].id == item_id) {
                                $scope.categories.splice(i, 1);
                                break;
                            }
                        }
                    },
                    //Error
                    function () {
                        item_scope.buttons_state.delete={disabled:false, title:"Delete"};
                        alert("Can't remove the ingredient group, an error has occurred");
                    }
                );
            }
        }

        $scope.updateCategory = function (item_scope) {
            var item_id=item_scope.obj.id;
            var item_name=item_scope.current_name;
            categories_factory.update({id: item_id}, {name: item_name},
                //Success
                function () {
                    for (var i = 0; i < $scope.categories.length; i++) {
                        if ($scope.categories[i].id == item_id) {
                            $scope.categories[i].name=item_name;
                            alert("The ingredient group name has been changed")
                            break;
                        }
                    }
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    item_scope.expand=false;
                },
                //Error
                function (){
                    item_scope.buttons_state.update={disabled:false, title:"Update"};
                    alert ("Can't change the ingredient group name, an error has occurred");
                }
            );
        }

        ////$scope.$watch("categories", function(categories) {
        ////    console.log(categories);
        ////});
    });