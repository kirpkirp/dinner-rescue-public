angular.module("rescue.directives").
    directive("userRowDirective", function (users_factory) {
        return {
            controller: function ($scope) {
                $scope.blockUser=function(){
                    users_factory.BlockUser($scope.user.id).then(
                        //success
                        function successCallback(response) {
                            $scope.user.active=response.data["active"];
                        },
                        //error
                        function errorCallback(response) {
                            alert("Sorry, can't block the user.");
                        });
                };

                $scope.unblockUser=function(){
                    users_factory.UnblockUser($scope.user.id).then(
                        //success
                        function successCallback(response) {
                            $scope.user.active=response.data["active"];
                        },
                        //error
                        function errorCallback(response) {
                            alert("Sorry, can't unblock the user.");
                        });
                };
            },
            replace: true,
            restrict: "A",
            scope: {
                user: "="
            },
            template:
              "<div>"+
                  "<button ng-if='user.active==1' type='button' class='btn btn-danger btn-xs' ng-click='blockUser()'>Block</button>"+
                  "<button ng-if='user.active==0' type='button' class='btn btn-info btn-xs' ng-click='unblockUser()'>Unblock</button>"+
              "</div>"
        }
    });