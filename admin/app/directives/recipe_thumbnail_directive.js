angular.module("rescue.directives").
    directive("recipeThumbnailDirective", function () {
        return {
            controller: function ($scope,$window) {
                //$scope.obj.id, $scope.obj.name
                $scope.image_url="../img/app/"+$scope.obj.id[$scope.obj.id.length-1]+"/"+$scope.obj.id+".jpg?p="+Math.random();

                $scope.removeRecipeEvent=function(){
                    //removeRecipe(recipe_id)
                    $scope.removeRecipe({recipe_id: $scope.obj.id});
                };

                $scope.title=$scope.obj.name.length>=20 ?  $scope.obj.name.substring(0,20)+"..." : $scope.obj.name;
                $scope.url="../recipe.php?id="+$scope.obj.id;

                $scope.redirectToRecipeEditPage=function(){
                    $window.location.href = "recipes.php?id="+$scope.obj.id;
                };
            },
            replace: true,
            restrict: "A",
            scope: {
                obj: "=",
                removeRecipe: "&"
            },
            template:
            "<div class='col-sm-4 col-md-3'>"+
              "<div class='thumbnail'>"+
                "<a ng-href='{{url}}'><img ng-src='{{image_url}}' alt='{{obj.name}}'></a>"+
                "<div class='caption'>"+
                  "<h4>{{title}}</h4>"+
                  "<p>"+
                    "<button class='btn btn-success btn-sm' ng-click='redirectToRecipeEditPage()'>Edit &nbsp;<i class='fa fa-pencil-square-o'></i></button>"+
                    "<button class='btn btn-danger btn-sm' ng-click='removeRecipeEvent()'>Remove &nbsp;<i class='fa fa-trash'></i></button>"+
                  "</p>"+
                "</div>"+
              "</div>"+
            "</div>"
        }
    });
