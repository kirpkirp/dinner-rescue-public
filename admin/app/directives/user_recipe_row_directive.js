angular.module("rescue.directives").
    directive("userRecipeRowDirective", function (users_recipes_factory) {
        return {
            controller: function ($scope) {
                $scope.removeCurrentRecipe=function(){
                    $scope.removeRecipe({recipe_id: $scope.recipe.id});
                };

                $scope.viewCurrentRecipe=function(){
                    $scope.viewRecipe({recipe_id:$scope.recipe.id});
                };
            },
            replace: true,
            restrict: "A",
            scope: {
                recipe: "=",
                removeRecipe: "&",
                viewRecipe: "&",
                some: "&"
            },
            template:
            "<div>"+
            "<button type='button' class='btn btn-info btn-xs' ng-click='viewCurrentRecipe()'>View</button>"+
            "<button type='button' class='btn btn-danger btn-xs' ng-click='removeCurrentRecipe()'>Remove</button>"+
            "</div>"
        }
    });