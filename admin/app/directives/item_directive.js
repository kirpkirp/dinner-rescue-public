angular.module("rescue.directives").
    directive("itemDirective", function () {
        return {
//                    compile: function compile(temaplateElement, templateAttrs) {
//                        return {
//                            pre: function (scope, element, attrs) {
//                            },
//                            post: function(scope, element, attrs) {
//                                console.log(scope.obj);
//                            }
//                        }
//                    },
            //  priority: 0,
            //  terminal:false,
            //  templateUrl: 'template.html',
            //  transclude: false,
            link: function (scope, element, attrs) {
            },
            controller: function ($scope) {
                $scope.current_name = $scope.obj.name;

                $scope.expand = false;

                $scope.buttons_state = {
                    update: {disabled: false, title: "Update"},
                    delete: {disabled: false, title: "Delete"}
                }

                $scope.showHideEditPanel = function () {
                    $scope.expand = !$scope.expand;
                }
                $scope.removeObject = function () {
                    // $scope.removeObj({category_id: $scope.obj.id});
                    $scope.buttons_state.delete = {disabled: true, title: "..."};
                    $scope.removeObj({item_scope: $scope});
                }
                $scope.saveObject = function (itemForm) {
                    if (itemForm.$valid) {
                        //  $scope.saveObj({category_id: $scope.obj.id, category_name: $scope.current_name});
                        $scope.buttons_state.update = {disabled: true, title: "..."};
                        $scope.saveObj({item_scope: $scope});
                    }
                }
            },
            replace: true,
            restrict: "A",
            scope: {
                obj: "=",
                saveObj: "&",
                removeObj: "&"
            },
            template:
            "<div class='alert alert-info directive_item_style'>" +
            "<form novalidate>" +
            "<ng-form name='itemForm'>" +
            "<span ng-show='!expand'>{{obj.name}}</span>" +
            "<i ng-show='!expand' ng-click='showHideEditPanel()' class='fa fa-arrow-circle-o-down item_directive_open_editing'></i>" +
            //"<button ng-show='!expand' ng-click='showHideEditPanel()' type='button' class='btn btn-success btn-xs directive_item_margin'><i class='fa fa-pencil-square-o'></i></button>" +
            "<br ng-show='!expand'>" +
            "<div ng-show='expand'>" +
            "<input required ng-minlength='3' ng-maxlength='30' name='categoryName' type='text' ng-model='current_name' class='form-control'>" +

            "<div ng-messages='itemForm.categoryName.$error' ng-if='itemForm.categoryName.$pristine || itemForm.categoryName.$dirty'>" +
            "<div ng-message='required' class='alert alert-danger'>This field is required!</div>" +
            "<div ng-message='minlength' class='alert alert-danger'>Message must be over 2 characters</div>" +
            "<div ng-message='maxlength' class='alert alert-danger'>Message must not exceed 30 characters</div>" +
            "</div>" +

            "<hr>" +
            "<div class='btn-group'>" +
            "<button ng-disabled='buttons_state.update.disabled || !itemForm.$valid' ng-click='saveObject(itemForm)' type='button' class='btn btn-success btn-xs'>{{buttons_state.update.title}} <i class='fa fa-floppy-o'></i></button>" +
            "<button ng-disabled='buttons_state.delete.disabled' ng-click='removeObject()' type='button' class='btn btn-danger btn-xs'>{{buttons_state.delete.title}} <i class='fa fa-trash'></i></button>" +
            //"<button ng-click='showHideEditPanel()' type='button' class='btn btn-primary btn-xs'>Close <i class='fa fa-times'></i></button>" +

            "</div>" +
            "<i ng-click='showHideEditPanel()' class='fa fa-arrow-circle-o-up item_directive_open_editing'></i>" +

            "</div>" +
            "</ng-form>" +
            "</form>" +
            "</div>"
        }
    });