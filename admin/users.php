<?php
require_once("php/admin_page_skeleton.php");
$page=new Admin_page_skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue (Home)</title>
    <?php
    //========== Styles
    echo $page::bootstrap_style;
    echo $page::sb_admin_style;
    echo $page::font_awesome_style;
    echo $page::admin_panel_style;

    //========== Scripts
    echo $page::for_ie_script;

    //Scripts==========
    echo $page::jquery_script;
    echo $page::bootstrap_script;
    echo $page::angularjs_script;
    echo $page::angularjs_resource_script;
    echo $page::angularjs_ng_messages_script;
    echo $page::angularjs_ng_file_upload_shim;
    echo $page::angularjs_ng_file_upload;
    ?>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <?php
    $page->Nav("Users");
    ?>

    <div id="page-wrapper" ng-app="rescue">

        <div class="container-fluid" ng-controller="users_controller">
            <!-- Form block -->
            <div class="row">
                <i ng-show="spinner" class="fa fa-refresh fa-spin fa-2x"></i>
                <table class="table table-striped table-bordered">
                    <br>
                    <thead>
                    <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-9">Options</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr ng-repeat="user in users" ng-class="{'info':user.active==1,'danger':user.active==0}">
                        <td>{{user.id}}</td>
                        <td>{{user.name}}</td>
                        <td>{{user.email}}</td>
                        <td>
                            <div user-row-directive user="user"></div>
                        </td>
                    </tr>

                    </tbody>
                </table>


            </div>

            <!-- Items block -->
<!--            <div class="row">-->
<!--                <div class="col-lg-12">-->
<!--                    <div ng-repeat="cuisine in cuisines_filter=(cuisines | orderBy: 'name')">-->
<!--                        <div item-directive-->
<!--                             obj="cuisine"-->
<!--                             save-obj="updateCuisine(item_scope)"-->
<!--                             remove-obj="removeCuisine(item_scope)">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div>-->
<!--                        <i ng-show="spinner" class="fa fa-refresh fa-spin fa-2x"></i>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#wrapper -->

    <?php
    //=====Angular app
    echo $page::angular_app;

//    -----Controllers
    echo $page::angular_controllers_root;
    echo $page::angular_users_controller;

//    -----Directives
    echo $page::angular_directives_root;
    echo $page::angular_user_row_directive;

//    -----Factories
    echo $page::angular_factories_root;
    echo $page::angular_users_factory;
    ?>

</body>

</html>
