<?php
require_once("php/admin_page_skeleton.php");
$page=new Admin_page_skeleton();

//checking id variable, if exists in url and db row with
//current id exists then using that id otherwise (0)
$recipe_id=isset($_GET["id"]) ? $page->GetCheckRecipeID($_GET["id"]) : 0;

//get recipe data + ingredients
//'recipe' =>
//          'id' => string '24' (length=2)
//          'name' => string 'Inf 1' (length=5)
//          'description' => string 'Some descriptiuon right here 1' (length=30)
//          'video_url' => string 'https://www.youtube.com/watch?v=XDNlilnl8VY' (length=43)
//          'cuisine_id' => string '6' (length=1)
//          'active' => string '1' (length=1)
//          'created' => string '2016-01-22 11:24:46' (length=19)
//'ingredients' =>
//    array (size=5)
//      0 =>
//        array (size=2)
//          'id' => string '83' (length=2)
//          'name' => string 'Vegetable 2' (length=11)
//      1 =>
//        array (size=2)
//          'id' => string '84' (length=2)
//          'name' => string 'Vegetable 3' (length=11)
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue (Home)</title>
    <?php
    //========== Styles
    echo $page::bootstrap_style;
    echo $page::sb_admin_style;
    echo $page::font_awesome_style;
    echo $page::admin_panel_style;

    //========== Scripts
    echo $page::for_ie_script;

    //Scripts==========
    echo $page::jquery_script;
    echo $page::bootstrap_script;
    echo $page::angularjs_script;
    echo $page::angularjs_resource_script;
    echo $page::angularjs_ng_messages_script;
    //file upload scripts
    echo $page::angularjs_ng_file_upload_shim;
    echo $page::angularjs_ng_file_upload;
    ?>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <?php
      $page->Nav("Recipes");
    ?>

    <div id="page-wrapper" ng-app="rescue">
        <div class="container-fluid"
             ng-controller="recipes_controller"
             <?php
               echo "ng-init='recipe_id=".json_encode($recipe_id)."'";
            // if ($recipe_id>0){
                //echo "ng-init='recipe_ingredients_list=".json_encode($current_recipe)."'";
             //}
             ?>
             >
            <!-- Form block -->
            <form name="recipeForm" novalidate>

            <!--Recipe name-->
            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label" for="recipeName">Recipe name (*):</label>

                    <input required
                           <?php
                               //$ng_recipe_name=($recipe_id>0) ? $current_recipe["recipe"]["name"] : null;
                               //echo "ng-init=\"obj.recipe_name='".$ng_recipe_name."'\"";
                               echo "ng-init=\"obj.recipe_name=''\"";
                           ?>
                           ng-minlength="3"
                           ng-maxlength="40"
                           ng-model="obj.recipe_name"
                           ng-class="{'error_red_border' : !recipeForm.recipeName.$valid}"
                           name="recipeName" type="text" class="form-control" placeholder="New recipe name...">
                </div>

<!--                ng-if="recipeForm.recipeName.$dirty"-->
                <div class="col-lg-6" ng-messages="recipeForm.recipeName.$error">
                    <br>
                    <div ng-message="required" class="error_message_text">This field is required!</div>
                    <div ng-message="minlength" class="error_message_text">Recipe name must be over 2 characters</div>
                    <div ng-message="maxlength" class="error_message_text">Recipe name must not exceed 40 characters</div>
                </div>
            </div>
            <!--Recipe name ends-->

            <!--Description-->
            <div class="row">
                    <div class="col-lg-6">
                        <label class="control-label" for="recipeDescription">Description (*):</label>

                        <textarea
                               rows="6"
                               required
                               ng-minlength="200"
                               ng-maxlength="2000"

                            <?php
                            //  $ng_recipe_description=($recipe_id>0) ? $current_recipe["recipe"]["description"] : null;
                            //  echo "ng-init=\"obj.recipe_description='".$ng_recipe_description."'\"";
                            echo "ng-init=\"obj.recipe_description=''\"";
                            ?>
                               ng-model="obj.recipe_description"
                               ng-class="{'error_red_border' : !recipeForm.recipeDescription.$valid}"
                               name="recipeDescription" class="form-control textarea_no_resizing" placeholder="Recipe description...">
                        </textarea>
                    </div>

<!--                ng-if="recipeForm.recipeDescription.$dirty"-->
                    <div class="col-lg-6" ng-messages="recipeForm.recipeDescription.$error" >
                        <br>
                        <div ng-message="required" class="error_message_text">This field is required!</div>
                        <div ng-message="minlength" class="error_message_text">Description must be over 200 characters</div>
                        <div ng-message="maxlength" class="error_message_text">Description must not exceed 2000 characters</div>
                    </div>
            </div>
            <!--Description ends-->

            <!--Image-->
            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label" for="recipeImage">Recipe image (*):</label>
                    <br>

                    <!-- ng-file-upload source -->
                    <input type="file"
                           ng-model="pic_file"
                           name="recipeImage"
                           id="recipeImage"
                           ngf-select
                           accept="image/*"
                           ngf-max-size="100KB"
                           ngf-pattern="'.jpg'"
                           ng-class="{'error_red_border' : !recipeForm.recipeImage.$valid}"
                        <?php
                        if ($recipe_id==0){
                            echo "required";
                        }
                        ?>
                           >
<!--                    {{recipeForm.recipeImage.$error}}-->
                </div>

<!--                ng-if="recipeForm.recipeImage.$dirty"-->
<!--                <div class="col-lg-6">-->
<!--                    <br>-->
<!--                    <div  ng-show="recipeForm.recipeImage.$error.required" class="error_message_text">This field is required!</div>-->
<!--                    <div  ng-show="recipeForm.recipeImage.$error.maxSize" class="error_message_text">Max image size is 700KB</div>-->
<!--                    <div  ng-show="recipeForm.recipeImage.$error.pattern" class="error_message_text">Only .jpg image format is acceptable</div>-->
<!--                </div>-->

                    <div class="col-lg-6" ng-messages="recipeForm.recipeImage.$error">
                        <br>
                        <div ng-message="required" class="error_message_text">This field is required!</div>
                        <div ng-message="minlength" class="error_message_text">Description must be over 50 characters</div>
                        <div ng-message="maxlength" class="error_message_text">Description must not exceed 1000 characters</div>
                    </div>
            </div>
            <!--An image ends-->

            <?php
              $page->GetHTMLFormRecipePreviewImage($recipe_id);
            ?>

            <!-- Cuisine groups -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <label class="control-label">Cuisines <span class="badge">{{cuisines_filter.length}}</span></label>
<!--                    <hr>{{active_cuisine_scope.obj}}-->

                    <div ng-repeat="cuisine in cuisines_filter=(cuisines | orderBy: 'name')">
                        <div checked-item-directive obj="cuisine" item_action="clickOnCuisine(item_scope)"></div>
                    </div>

                    <div><i ng-show="cuisines_spinner" class="fa fa-refresh fa-spin fa-2x"></i></div>
                </div>
            </div>
            <!-- End cuisine groups -->

            <!-- Ingredient groups -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <label class="control-label">Groups <span class="badge">{{groups_filter.length}}</span></label>
<!--                    <hr>{{active_category_scope.obj}}-->

                    <div ng-repeat="category in groups_filter=(categories | orderBy: 'name')">
                       <div checked-item-directive obj="category" item_action="clickOnCategory(item_scope)"></div>
                    </div>

                    <div><i ng-show="categories_spinner" class="fa fa-refresh fa-spin fa-2x"></i></div>
                </div>
            </div>
            <!-- End ingredient groups -->


            <!-- Ingredients -->
            <div class="row">
              <div class="col-lg-12">
                <hr>
                <label class="control-label">
                    Ingredients <span class="badge">{{ingredients_filter.length}}</span>
                    <span class="badge" ng-class="{'red_background': ingredients_obj.list.length<ingredients_obj.amount.min,'green_background': ingredients_obj.list.length>=ingredients_obj.amount.min}">
                        <i class="fa fa-hand-o-down"></i>&nbsp;{{ingredients_obj.amount.min}}&nbsp;&nbsp;<i class="fa fa-hand-o-up"></i>&nbsp;{{ingredients_obj.amount.max}}&nbsp;&nbsp;({{ingredients_obj.list.length}})
                    </span>
                </label>
                <hr>


<!--                <span ng-repeat="ingr in ingredients_obj.list">-->
<!--                    {{ingr}}-->
<!--                </span>-->

                <div ng-repeat="ingredient in ingredients_filter=(ingredients | filter: filterIngredients |  orderBy: 'name')">
                    <div checked-recipe-directive obj="ingredient" active_ingredients_obj="ingredients_obj" item_action="clickOnIngredient(item_obj)"></div>
                </div>

                <div>
                    <i ng-show="ingredients_spinner" class="fa fa-refresh fa-spin fa-2x"></i>
                </div>
              </div>

            </div>
            <!-- End ingredients -->

                <br>
<!--                {{create_button.disabled}}-->

                <div class="row">
                    <div class="col-lg-12">
                        <input
                              ng-disabled="!recipeForm.$valid ||
                                           create_button.disabled ||
                                           ingredients_obj.list.length<ingredients_obj.amount.min ||
                                           ingredients_obj.list.length>ingredients_obj.amount.max"
                               ng-click="createUpdateRecipe(recipeForm)"
                               value="{{create_button.text}}"
                               class="btn btn-info col-lg-12">
                    </div>
                </div>

            </form>

            <hr>
            <h2>Recipes:</h2>
            <hr>

<!--            {{recipes}}-->
<!--            Recipes area-->
            <div class="row">
                <div ng-show="spinner.recipes" class="col-lg-12">
                    <i class="fa fa-refresh fa-spin fa-2x"></i>
                    <br>
                </div>

                <div ng-repeat="recipe in recipes">
                    <div recipe-thumbnail-directive obj="recipe" remove-recipe="removeRecipe(recipe_id)"></div>
                </div>
            </div>


        </div>
        <!-- .container-fluid-->

    </div>
    <!-- #page-wrapper -->

</div>
<!-- /#wrapper -->

<?php
//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_recipes_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_item_directive;
echo $page::angular_checked_item_directive;
echo $page::angular_checked_recipe_directive;
echo $page::angular_recipe_thumbnail_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_cuisines_factory;
echo $page::angular_categories_factory;
echo $page::angular_ingredients_factory;
echo $page::angular_recipes_factory;
?>

</body>

</html>