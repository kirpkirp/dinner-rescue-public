<?php
require_once('../vendor/autoload.php');
require_once('../php/rb.php');
require_once('../php/db.php');

use Respect\Validation\Validator as v;

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);


class Admin_page_skeleton{
    //========== Scripts
    const for_ie_script = "
                  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                  <!--[if lt IE 9]>
                  <script src='../js/for_ie/html5shiw.js'></script>
                  <script src='../js/for_ie/respond.js'></script>
                  <![endif]-->
                  ";
    const jquery_script="<script src='../bower_components/jquery/dist/jquery.min.js'></script>";
    const bootstrap_script="<script src='../bower_components/bootstrap/dist/js/bootstrap.min.js'></script>";
    const angularjs_script="<script src='../bower_components/angular/angular.min.js'></script>";
    const angularjs_resource_script="<script src='../bower_components/angular-resource/angular-resource.min.js'></script>";
    const angularjs_ng_messages_script="<script src='../bower_components/angular-messages/angular-messages.min.js'></script>";
    //ng-file-upload scripts
    const angularjs_ng_file_upload="<script src='../bower_components/ng-file-upload/ng-file-upload.min.js'></script>";
    //for browser that doesn't support HTML5
    const angularjs_ng_file_upload_shim="<script src='../bower_components/ng-file-upload/ng-file-upload-shim.min.js'></script>";


    //========== Angular app files
    const angular_app="<script src='app/app.js'></script>";

    //---------- Controllers
    const angular_controllers_root="<script src='app/controllers.js'></script>";
    const angular_categories_controller="<script src='app/controllers/categories_controller.js'></script>";
    const angular_cuisines_controller="<script src='app/controllers/cuisines_controller.js'></script>";
    const angular_ingredients_controller="<script src='app/controllers/ingredients_controller.js'></script>";
    const angular_recipes_controller="<script src='app/controllers/recipes_controller.js'></script>";
    const angular_users_controller="<script src='app/controllers/users_controller.js'></script>";
    const angular_users_recipes_controller="<script src='app/controllers/users_recipes_controller.js'></script>";

    //---------- Directives
    const angular_directives_root="<script src='app/directives.js'></script>";
    const angular_item_directive="<script src='app/directives/item_directive.js'></script>";
    const angular_checked_item_directive="<script src='../app/directives/checked_item_directive.js'></script>";
    const angular_checked_recipe_directive="<script src='../app/directives/checked_recipe_directive.js'></script>";
    const angular_recipe_thumbnail_directive="<script src='app/directives/recipe_thumbnail_directive.js'></script>";
    const angular_user_row_directive="<script src='app/directives/user_row_directive.js'></script>";
    const angular_user_recipe_row_directive="<script src='app/directives/user_recipe_row_directive.js'></script>";

    //---------- Factories
    const angular_factories_root="<script src='app/factories.js'></script>";
    const angular_categories_factory="<script src='app/factories/categories_factory.js'></script>";
    const angular_cuisines_factory="<script src='app/factories/cuisines_factory.js'></script>";
    const angular_ingredients_factory="<script src='app/factories/ingredients_factory.js'></script>";
    const angular_recipes_factory="<script src='app/factories/recipes_factory.js'></script>";
    const angular_users_factory="<script src='app/factories/users_factory.js'></script>";
    const angular_users_recipes_factory="<script src='app/factories/users_recipes_factory.js'></script>";

    //========== Styles
    const bootstrap_style = "<link href='../bower_components/bootstrap/dist/css/bootstrap.min.css' rel='stylesheet'>";
    const font_awesome_style="<link href='../bower_components/font-awesome/css/font-awesome.min.css' rel='stylesheet'>";
    const sb_admin_style="<link href='css/sb-admin.css' rel='stylesheet'>";
    const admin_panel_style="<link href='../css/admin_main.css' rel='stylesheet'>";

    public function __construct(){}

    public function GetCheckRecipeID($current_recipe_id){
        //$id=isset($_GET["id"]) ? $page->GetCheckRecipeID($_GET["id"]) : 0;
        if (v::intVal()->validate($current_recipe_id)){
            $recipe = R::getAll("SELECT id,name FROM recipe WHERE id=?", array($current_recipe_id));
            return (count($recipe)>0) ? $current_recipe_id : 0;
        }
        else{
            return 0;
        }
    }

    public function GetHTMLFormRecipePreviewImage($current_id){
        if ($current_id>0) {
            echo "<div class='row'>";
              echo "<div class='col-lg-3'>";
                echo "<div class='thumbnail'>";
                  echo "<img ng-src='../img/app/".substr($current_id,-1)."/".$current_id.".jpg' alt='Some text here'>";
                echo "</div>";
              echo "</div>";
            echo "</div>";
        }
    }

    public function Nav($active_tab_name){
        echo "
        <nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
            <!-- Top Menu Items -->
            <ul class='nav navbar-right top-nav'>
                <!--<li><a href='#'><i class='fa fa-fw fa-power-off'></i> Log Out</a></li>-->
            </ul>
        ";
        /*
                <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-fw fa-gear'></i> Settings <b class='caret'></b></a>
                    <ul class='dropdown-menu'>
                        <li>
                            <a href='#'><i class='fa fa-fw fa-gear'></i> Option 1</a>
                        </li>
                        <li class='divider'></li>
                        <li>
                            <a href='#'><i class='fa fa-fw fa-power-off'></i> Log Out</a>
                        </li>
                    </ul>
                </li>
         */
        $this->GenerateSideBarMenu($active_tab_name);
        echo "</nav>";
    }

    private function GenerateSideBarMenu($active_tab_name){
        $menu=[["name"=>"Ingredient groups","value"=>"Ingredient groups &nbsp;&nbsp;<i class='fa fa-bookmark'></i>","href"=>"index.php"],
               ["name"=>"Ingredients","value"=>"Ingredients &nbsp;&nbsp;<i class='fa fa-pie-chart'></i>","href"=>"ingredients.php"],
               ["name"=>"Recipes","value"=>"Recipes &nbsp;&nbsp;<i class='fa fa-cutlery'></i>","href"=>"recipes.php"],
               ["name"=>"Cuisines","value"=>"Cuisines &nbsp;&nbsp;<i class='fa fa-puzzle-piece'></i>","href"=>"cuisines.php"],
               ["name"=>"Users","value"=>"Users &nbsp;&nbsp;<i class='fa fa-users'></i>","href"=>"users.php"],
               ["name"=>"Users recipes","value"=>"User's recipes &nbsp;&nbsp;<i class='fa fa-file-text-o'></i>","href"=>"users_recipes.php"]
        ];

        echo "<div class='collapse navbar-collapse navbar-ex1-collapse'>
                <ul class='nav navbar-nav side-nav'>";

        foreach ($menu as &$val) {
            $li_element = ($val["name"] == $active_tab_name) ? "<li class='active'>" : "<li>";
            echo $li_element;
            echo "<a href='" . $val["href"] . "'>";
              echo "<i class='fa fa-fw'></i> " . $val["value"];
            echo "</a>";
            echo "</li>";
        }

          echo "</ul>";
        echo "</div>";
    }
}