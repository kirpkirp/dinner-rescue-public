<?php
use Respect\Validation\Validator as v;

class RecipeDataChecker{
    //image
    const image_width=300;
    const image_height=300;
    const image_size=100;
    const image_format="image/jpeg";

    //ingredients array
    const recipe_ingredients_max_length=10;
    const recipe_ingredients_min_length=3;

    //recipe name
    const recipe_name_min_length=3;
    const recipe_name_max_length=40;

    //recipe description
    const recipe_description_min_length=200;
    const recipe_description_max_length=2000;

    //recipe url
    const recipe_url_max_length=100;

    //root images path
    const root_images_path="../../../img/app/";

    private $recipe_image=null;
    private $recipe_ingredients=array();
    private $recipe_object=null;
    private $recipe_cuisine_id=0;

    function __construct($recipe_image,$recipe_object,$recipe_ingredients,$recipe_cuisine_id){
        $this->recipe_image=$recipe_image;
        //$this->recipe_ingredients=json_decode($recipe_ingredients);
        $this->SetRecipeIngredients($recipe_ingredients);
        $this->recipe_object=$recipe_object;
        $this->recipe_cuisine_id=v::intVal()->validate($recipe_cuisine_id) ? json_decode($recipe_cuisine_id) : 0;
    }

    public function CheckCuisineID(){
        return $this->recipe_cuisine_id>0;
    }

    function getRecipeName(){
        return $this->recipe_object["recipe_name"];
    }

    function getRecipeDescription(){
        return $this->recipe_object["recipe_description"];
    }

    function getRecipeURL(){
        return $this->recipe_object["recipe_url"];
    }

    function getRecipeCuisineID(){
        return $this->recipe_cuisine_id;
    }

    function getRecipeIngredients(){
        return $this->recipe_ingredients;
    }

    private function SetRecipeIngredients($ingredients_array){
        //convert array with (string) values to (int) values
        $this->recipe_ingredients=array_map("intval", $ingredients_array);
    }

    public function SaveImage($recipe_id){
        $temp = explode(".", $this->recipe_image["name"]);
        $destination=self::root_images_path.substr($recipe_id,-1)."/";
        $new_file_name = $recipe_id.'.'.end($temp);
        //echo "REcipe id: ".$recipe_id;
       // echo $this->recipe_image["tmp_name"].$destination.$new_file_name;
        move_uploaded_file($this->recipe_image["tmp_name"], $destination.$new_file_name);
    }

    public function Check(){
        return $this->CheckImage() &&
               $this->CheckTextData() &&
               $this->CheckIngredientsArray() &&
               $this->CheckCuisineID();
    }

    public function CheckName(){
        return v::stringType()->length(self::recipe_name_min_length,self::recipe_name_max_length)->validate($this->recipe_object["recipe_name"]);
    }

    public function CheckDescription(){
        return v::stringType()->length(self::recipe_description_min_length,self::recipe_description_max_length)->validate($this->recipe_object["recipe_description"]);
    }

    public function CheckURL(){
        return array_key_exists("recipe_url",$this->recipe_object) && isset($this->recipe_object["recipe_url"]) && is_null(json_decode($this->recipe_object["recipe_url"])) ?
            (
            (strlen($this->recipe_object["recipe_url"])>0)?
                v::stringType()->length(0,self::recipe_url_max_length)->validate($this->recipe_object["recipe_url"]) && //is it text between 0 and 100
                v::videoUrl()->validate($this->recipe_object["recipe_url"]) :                                           //is it youtube video url
                true
            ) : false;
    }

    public function CheckIngredientsArray(){
        return count($this->recipe_ingredients)>=self::recipe_ingredients_min_length &&
               count($this->recipe_ingredients)<=self::recipe_ingredients_max_length;
    }

    public function CheckImage(){
        if ($this->recipe_image==null){
            return false;
        }
        $image_data=self::GetImageData();
        return $image_data["width"]==self::image_width &&
               $image_data["height"]==self::image_height &&
               $image_data["mime"]==self::image_format &&
               $image_data["size"]<=self::image_size;
    }

    private function GetImageData(){
        $img_data=array();
        if($this->recipe_image["error"] == 0) {
            $info   = getimagesize($this->recipe_image["tmp_name"]);
            $img_data["mime"]=$info["mime"];      //image/jpeg
            $img_data["width"]=$info[0];
            $img_data["height"]=$info[1];
            $img_data["type"]=$info[2];   //2
            $img_data["size"]=filesize($this->recipe_image["tmp_name"])/1024; //file size KB
        }
        else{
            throw new Exception("Problems to load image");
        }
        return $img_data;
    }

}