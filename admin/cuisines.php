<?php
require_once("php/admin_page_skeleton.php");
$page=new Admin_page_skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue (Home)</title>
    <?php
    //========== Styles
    echo $page::bootstrap_style;
    echo $page::sb_admin_style;
    echo $page::font_awesome_style;
    echo $page::admin_panel_style;

    //========== Scripts
    echo $page::for_ie_script;

    //Scripts==========
    echo $page::jquery_script;
    echo $page::bootstrap_script;
    echo $page::angularjs_script;
    echo $page::angularjs_resource_script;
    echo $page::angularjs_ng_messages_script;
    //file upload scripts
    echo $page::angularjs_ng_file_upload_shim;
    echo $page::angularjs_ng_file_upload;
    ?>
</head>

<body>
<div id="wrapper">
    <!-- Navigation -->
    <?php
    $page->Nav("Cuisines");
    ?>

    <div id="page-wrapper" ng-app="rescue">

        <div class="container-fluid" ng-controller="cuisines_controller">
            <!-- Form block -->
            <div class="row">
                <form name="cuisineForm" novalidate class="col-lg-6">
                    <label class="control-label" for="cuisine_name">Cuisine name (*):</label>

                    <div class="input-group">
                        <input required
                               ng-minlength="3"
                               ng-maxlength="30"
                               ng-model="obj.cuisine"
                               ng-class="{'error_red_border' : !cuisineForm.cuisineName.$valid && cuisineForm.cuisineName.$dirty}"
                               name="cuisineName" type="text" class="form-control" placeholder="New cuisine name...">

                        <div class="input-group-btn">
                            <button  ng-disabled="!cuisineForm.$valid || create_button.disabled" class="btn btn-info" type="button" ng-click="createCuisine(cuisineForm)">{{create_button.text}}</button>
                        </div>
                    </div>

                </form>

                <div class="col-lg-6"
                     ng-messages="cuisineForm.cuisineName.$error"
                     ng-if="cuisineForm.cuisineName.$dirty">
                    <br>
                    <div ng-message="required" class="error_message_text">This field is required!</div>
                    <div ng-message="minlength" class="error_message_text">Cuisine name must be over 2 characters</div>
                    <div ng-message="maxlength" class="error_message_text">Cuisine name must not exceed 30 characters</div>
                </div>


            </div>

            <hr>
            <label class="control-label">Cuisines <span class="badge">{{cuisines_filter.length}}</span></label>
            <hr>

            <!-- Items block -->
            <div class="row">
                <div class="col-lg-12">
                    <div ng-repeat="cuisine in cuisines_filter=(cuisines | orderBy: 'name')">
                    <div item-directive
                         obj="cuisine"
                         save-obj="updateCuisine(item_scope)"
                         remove-obj="removeCuisine(item_scope)">
                    </div>
                </div>
                <div>
                    <i ng-show="spinner" class="fa fa-refresh fa-spin fa-2x"></i>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#wrapper -->

<?php
//=====Angular app
echo $page::angular_app;

//-----Controllers
echo $page::angular_controllers_root;
echo $page::angular_cuisines_controller;

//-----Directives
echo $page::angular_directives_root;
echo $page::angular_item_directive;

//-----Factories
echo $page::angular_factories_root;
echo $page::angular_cuisines_factory;
?>

</body>

</html>
