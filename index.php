<?php
  session_start();
  require_once("php/page_skeleton.php");
  $page=new skeleton();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dinner Rescue</title>
    <?php
      //Styles==========
      echo $page::bootstrap_style;
      echo $page::modern_business_style;
      echo $page::font_awesome_style;
      echo $page::slider_style;
      echo $page::admin_main_style;

      //Scripts==========
      echo $page::for_ie_script;
      echo $page::angularjs_script;
      echo $page::angularjs_ng_messages_script;

    ?>
</head>

<body ng-app="rescue"
      ng-controller="recipes_controller"
      ng-cloak
      <?php
        if (isset($_SESSION["user"])) {
           echo "ng-init='user_data=".json_encode($_SESSION["user"])."'";
        }
      ?>
    >

<div navigation-directive user="user_data" cookbook="cookbook_amount"></div>

<!-- Header Carousel -->
<?php
  $page->Slider();
?>
<!--============================================================================-->
<!-- Search recipes area -->
<div class="container">

    <div class="row" ng-show="spinner.constructor">
        <br>
        <i class="fa fa-refresh fa-spin fa-2x"></i>
    </div>

    <div class="row" ng-if="spinner.constructor==false">
        <div class="col-lg-12">
            <!-- Cuisine groups -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <h4>Cuisines (<strong>{{cuisines.length}}</strong>)</h4>
                    <hr>

                    <div ng-repeat="cuisine in cuisines track by $index">
                        <div
                            class="smaller_alert"
                            checked-item-directive
                            obj="cuisine"
                            item_action="clickOnCuisine(item_scope)"></div>
                    </div>
                </div>
            </div>
            <!-- End cuisine groups -->

            <!-- Ingredient groups -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <h4>Groups (<strong>{{groups_filter.length}}</strong>)</h4>
                    <hr>
                    <!--                    <hr>{{active_category_scope.obj}}-->

                    <div ng-repeat="category in groups_filter=(categories | orderBy: 'name') track by $index">
                        <div
                            class="smaller_alert"
                            checked-item-directive
                            obj="category"
                            item_action="clickOnCategory(item_scope)"></div>
                    </div>
                </div>
            </div>
            <!-- End ingredient groups -->

            <!-- Ingredients -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>

                    <h4>Ingredients (<strong>{{ingredients_filter.length}}</strong>)
                      <span class="badge" ng-class="{'red_background': ingredients_obj.list.length<ingredients_obj.amount.min || ingredients_obj.list.length>ingredients_obj.amount.max,'green_background': ingredients_obj.list.length>=ingredients_obj.amount.min}">
                        <i class="fa fa-hand-o-down"></i>&nbsp;{{ingredients_obj.amount.min}}&nbsp;&nbsp;<i class="fa fa-hand-o-up"></i>&nbsp;{{ingredients_obj.amount.max}}&nbsp;&nbsp;({{ingredients_obj.list.length}})
                    </span>
                    </h4>

                    <hr>

                    <div ng-repeat="ingredient in ingredients_filter=(ingredients | filter: filterIngredients |  orderBy: 'name') track by $index">
                        <div class="smaller_alert"
                             checked-recipe-directive
                             obj="ingredient"
                             active_ingredients_obj="ingredients_obj"
                             item_action="clickOnIngredient(item_obj)"></div>
                    </div>
                </div>
            </div>

            <hr>
            <!-- Deep search -->
            <div class="row">
                <div class="col-lg-2">
                    <h4>
                        Deep search
                        <button id="accurate_search"
                                type="button"
                                class="btn btn-sm"
                                ng-class="{'btn-danger':!accurate_search,'btn-success':accurate_search}"
                                ng-click="accurate_search_click()">{{accurate_search ? "ON" : "OFF"}}</button>
                    </h4>
                </div>
                <div class="col-lg-10">
                    <div ng-if="ingredients_obj.list.length>ingredients_obj.amount.max" class="alert alert-danger text-center alert_danger_too_much_ingredients" role="alert">
                        <i class="fa fa-flag"></i>&nbsp;&nbsp;Before continue uncheck <strong>{{ingredients_obj.list.length-ingredients_obj.amount.max}}</strong> ingredient(s) or click on a <strong>Reset button</strong>
                    </div>
                </div>

            </div>
            <hr>

        </div>
    </div>
<!--============================================================================-->

<!--    <hr ng-if="spinner.constructor==false">-->

    <div class="row" ng-if="spinner.constructor==false">
        <div class="col-lg-12 text-center">
            <div class="btn-group btn-group-lg" role="group">

                <button ng-disabled="ingredients_obj.list.length<ingredients_obj.amount.min || ingredients_obj.list.length>ingredients_obj.amount.max"
                        class="btn btn-success center-block"
                        id="search_button"
                        ng-click="GetRecipes()">
                    <i class="fa fa-search"></i>&nbsp;&nbsp;Search
                </button>
                <button ng-disabled="ingredients_obj.list.length<ingredients_obj.amount.min"
                        type="button"
                        class="btn btn-danger center-block"
                        ng-click="ResetIngredients()">
                    Reset&nbsp;&nbsp;<i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>

<!--    <hr ng-if="spinner.constructor==false">-->

    <!-- Recipes thumbnails section -->
    <div class="row">
        <div class="col-lg-12">
            <div ng-if="alert_message==0 && spinner.constructor==false">
                <hr>
                <div class="alert alert-success text-center" role="alert">
                    <i class="fa fa-lightbulb-o"></i>&nbsp;<strong>Quick tip.</strong>&nbsp;Choose up to 10/20 ingredients <i>(depends on a deep search option)</i> and click on a search button
                </div>
            </div>

            <div ng-if="alert_message>0 && recipes.length==0">
                <hr>
                <div class="alert alert-warning text-center" role="alert">Sorry, didn't find any recipes</div>
            </div>

            <div><i ng-show="spinner.recipes" class="fa fa-refresh fa-spin fa-2x"></i></div>

            <div ng-if="recipes.length>0">
              <hr>
              <h4>Recipes found (<strong>{{recipes.length}}</strong>)</h4>
              <hr>
            </div>

            <div ng-repeat="recipe in recipes">
            <div recipe_thumbnail_directive
                   search-page="true"
                   user="user_data"
                   recipe="recipe"
                   increment-cookbook="IncrementCookbook()"
                   decrement-cookbook="DecrementCookbook()"
                   confirmation-dialog="false"></div>
            </div>
        </div>
    </div>
    <!-- /.row -->

<!--    <hr ng-if="spinner.constructor==false">-->

    <?php
      $page->Footer();
    ?>
</div>
<!-- /.container -->

<?php
  //Scripts==========
  echo $page::jquery_script;
  echo $page::bootstrap_script;
  echo $page::index_slider_script;

  //=====Angular app
  echo $page::angular_app;

  //-----Controllers
  echo $page::angular_controllers_root;
  echo $page::angular_recipes_controller;

  //-----Directives
  echo $page::angular_directives_root;
  echo $page::angular_checked_item_directive;
  echo $page::angular_checked_recipe_directive;
  echo $page::angular_navigation_directive;
  echo $page::angular_recipe_thumbnail_directive;

  //-----Factories
  echo $page::angular_factories_root;
  echo $page::angular_recipes_factory;
  echo $page::angular_navigation_factory;
  echo $page::angular_user_factory;
  echo $page::angular_url_factory;
?>
</body>

</html>
