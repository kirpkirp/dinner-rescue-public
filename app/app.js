angular.module("rescue",
    [
     "rescue.controllers",
     "rescue.factories",
     "rescue.directives",
     "ngMessages"
    ]);
