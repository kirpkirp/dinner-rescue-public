angular.module("rescue.controllers").
    controller("cookbook_controller", function ($scope,
                                                user_factory
    ) {
        //$scope.user_data (id,name)

        $scope.cookbook_amount=0;

        $scope.spinner=true;

        $scope.RemoveFromCookbookList=function(recipe_id){
           for (var i=0;i<$scope.recipes.length;i++){
               if (recipe_id==$scope.recipes[i]["in_cookbook"]){
                   $scope.recipes.splice(i,1);
                   return;
               }
           }
        };

        user_factory.GetCookbookRecipes().
            then(
            //success
            function successCallback(response) {
                $scope.spinner=false;
                $scope.recipes=response.data;
            },
            //error
            function errorCallback(response) {
                //$scope.spinner.recipes=false;
                $scope.spinner=false;
                alert("Sorry, can't retrieve recipes from data base.");
            });

        $scope.IncrementCookbook=function(){
            $scope.cookbook_amount++;
        };

        $scope.DecrementCookbook=function(){
            $scope.cookbook_amount--;
        };
    });
