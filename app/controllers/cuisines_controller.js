angular.module("rescue.controllers").
    controller("cuisines_controller", function ($scope,
                                                cuisines_factory,
                                                url_factory,
                                                $window
    ) {
        //$scope.user_data (id,name)
        $scope.cookbook_amount=0;
        $scope.spinner=true;

        //$scope.RemoveFromCookbookList=function(recipe_id){
        //    for (var i=0;i<$scope.recipes.length;i++){
        //        if (recipe_id==$scope.recipes[i]["in_cookbook"]){
        //            $scope.recipes.splice(i,1);
        //            return;
        //        }
        //    }
        //};

        $scope.GetRecipes=function(){
            cuisines_factory.GetCuisineRecipes(
                url_factory.GetVar($window.location.search,"id")).
                then(
                //success
                function successCallback(response) {
                    $scope.spinner=false;
                    $scope.recipes=response.data["recipes"];
                    $scope.title=(response.data["cuisine"]) ? "Cuisine - "+response.data["cuisine"].name+" " : "";
                },
                //error
                function errorCallback(response) {
                    //$scope.spinner.recipes=false;
                    $scope.spinner=false;
                    alert("Sorry, can't retrieve recipes from data base.");
                });
        };

        $scope.GetRecipes();

        $scope.IncrementCookbook=function(){
            $scope.cookbook_amount++;
        };

        $scope.DecrementCookbook=function(){
            $scope.cookbook_amount--;
        };
    });
