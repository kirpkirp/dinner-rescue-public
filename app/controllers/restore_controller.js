angular.module("rescue.controllers").
    controller("restore_controller", function ($scope,
                                               user_factory
    ) {
        $scope.cookbook_amount=0;

        $scope.RestorePassword=function(restoreForm){
            if (!restoreForm.$valid) return;
            user_factory.RecoverPassword($scope.restore).then(
                //success
                function successCallback(response) {
                    alert("Password has been sent to your email address");
                },
                //error
                function errorCallback(response) {
                    alert("Sorry, current email address doesn't exist or server error occured.");
                }
            );
        };

    });