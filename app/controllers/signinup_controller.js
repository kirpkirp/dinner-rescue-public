angular.module("rescue.controllers").
    controller("signinup_controller", function($scope,
                                               signinup_factory,
                                               navigation_factory){
        $scope.cookbook_amount=0;

        //Get navigation cuisines data
        $scope.GetNavigationCuisinesData=function(){
            navigation_factory.GetNavigationCuisines().then(
                //Success
                function(response, status, headers, config) {
                    //{id, name, amount}
                    var navigation_data_list=response.data;
                    var recipes_amount=0;
                    for (var i=0;i<navigation_data_list.length;i++){
                        recipes_amount+=parseInt(navigation_data_list[i]["amount"]);
                    }
                    response.data.unshift({id:0,name:"All",amount:recipes_amount.toString()});
                    $scope.navigation_data_list=response.data;
                },
                function(response, status, headers, config) {
                });
        };

        $scope.GetNavigationCuisinesData();

//===============================================================
        $scope.SignInUser=function(signinForm){
          if (!signinForm.$valid){
              return;
          }
          signinup_factory.SignIn($scope.signin).then(
                //success
                function successCallback(response) {
                    window.location = "index.php";
                    //$scope.recipes=response.data;
                },
                //error
                function errorCallback(response) {
                    alert("Sorry, server error. Can't let you inside the system. Wrong login or/and password or access to the system restricted.");
                });
        };

        $scope.SignUpUser=function(signUpForm){
            var passwords_equality=$scope.signup.confirm_password!=$scope.signup.password &&
                                   signUpForm.userConfirmPassword.$dirty &&
                                   signUpForm.userPassword.$dirty;
            if (!signUpForm.$valid || passwords_equality){
                return;
            }
            signinup_factory.SignUp($scope.signup).then(
                //success
                function successCallback(response) {
                    window.location = "index.php";
                    //$scope.recipes=response.data;
                },
                //error
                function errorCallback(response) {
                    alert("Sorry, server error. Can't register you with such credentials.");
                });
        };
    });
