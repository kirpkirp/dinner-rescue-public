angular.module("rescue.controllers").
    controller("profile_controller", function($scope,
                                              user_factory){
        $scope.cookbook_amount=0;
        $scope.active_tab_index=0;

        $scope.ShowChangePass=function(){
            $scope.active_tab_index=0;
        };

        $scope.ShowOfferRecipe=function(){
            $scope.active_tab_index=1;
        };

        $scope.ppp=false;

//===============================================================
        $scope.ChangePassword=function(changePasswordForm){
            var passwords_equality=$scope.password.confirm_new_password!=$scope.password.new_password &&
                                   !changePasswordForm.confirmNewPassword.$dirty &&
                                   !changePasswordForm.newPassword.$dirty;

            //confirmNewPassword
           // $scope.ppp={equal : $scope.password.confirm_new_password==$scope.password.new_password,
           //d1: changePasswordForm.confirmNewPassword.$dirty,
           //     d2: changePasswordForm.newPassword.$dirty
           // };

            if (!changePasswordForm.$valid || passwords_equality){
                return;
            }

            user_factory.ChangePassword($scope.password).then(
                function successCallback(response){
                    alert("Your password has been changed!");

                },
                function errorCallback(respose){
                    alert("Sorry, server error. Can't change a password. Your old pass doesn't match or new password data is not valid.");
                }
            );
        };

        $scope.OfferRecipe=function(recipeForm){
            if (!recipeForm.$valid){
                return;
            }

            user_factory.SendRecipe($scope.recipe).then(
                //success
                function successCallback(response){
                    alert("Your recipe request has been sent!");
                },
                //error
                function errorCallback(response){
                    alert("Sorry, server error. Can't send your recipe data or your data invalid.");
                }
            );
            //signinup_factory.SignUp($scope.signup).then(
            //    //success
            //    function successCallback(response) {
            //        window.location = "index.php";
            //        //$scope.recipes=response.data;
            //    },
            //    //error
            //    function errorCallback(response) {
            //        alert("Sorry, server error. Can't register you with such credentials.");
            //    });
        };

    });
