angular.module("rescue.controllers").
    controller("recipes_controller", function ($scope,
                                               recipes_factory,
                                               $filter
    ) {
        //Show spinner loading icon (categories and ingredients sections)
        $scope.spinner={constructor:true,
                        recipes:false};

        //Constants for accurate_search function
        $scope.search_max={min:10,max:20};

        //Create new ingredient button state
        //$scope.create_button = {disabled: false, text: $scope.main_button_name};

        $scope.active_cuisine_scope=null;
        $scope.active_category_scope=null;

        //active ingredients list
        $scope.ingredients_obj= {
            list:[],
            amount:{min:1,max:$scope.search_max.max}
        };

        $scope.joinus_button_show=true;
        $scope.cookbook_amount=0;
        $scope.alert_message=0; //0 - quick start tip, >0 + no recipes found -> Didn't find any recipes
        $scope.accurate_search=false;

        $scope.accurate_search_click=function(){
          $scope.accurate_search=!$scope.accurate_search;
          $scope.ingredients_obj.amount.max=$scope.accurate_search ? $scope.search_max.min : $scope.search_max.max;
        };

        //watcher for user data initialization
        var user_data_watcher=$scope.$watch("user_data",function(){
            if (!($scope.user_data===undefined)){
                $scope.joinus_button_show=false;
            }
         //  console.log($scope.user_data===undefined ? "No user data" :$scope.user_data);
            user_data_watcher();
        });

        $scope.ResetIngredients=function(){
          for (var i=0;i<$scope.ingredients.length;i++){
              $scope.ingredients[i]["selected"]=false;
          }
          $scope.ingredients_obj.list=[];
        };

        //filter function
        $scope.filterIngredients = function(element) {
            return ($scope.active_category_scope==null) ? false : element.ingr_category_id==$scope.active_category_scope.obj.id;
        };

        recipes_factory.GetConstructorData().then(
            function(response, status, headers, config){
                SetCuisines(response.data["cuisines"]);
                SetIngredientGroups(response.data["ingredient_groups"]);
                SetIngredients(response.data["ingredients"]);
                SetStatistic(response.data["statistic"]);

                $scope.spinner.constructor=false;
            },
            function(response, status, headers, config){
                //alert("Sorry, can't retrieve data from the server");
                console.log("Sorry, can't retrieve data from the server.");
            }
        );

        function SetCuisines(inputData){
            var cuisines_list=inputData;

            if (cuisines_list.length > 0) {
                cuisines_list =$filter("orderBy")(cuisines_list, "name");
                cuisines_list.unshift({id:0,name:"All"});
                $scope.cuisines=cuisines_list;
                for (var i = 0; i < cuisines_list.length; i++) {
                    cuisines_list[i]["active"] = false;
                }
                $scope.cuisines = $filter("orderBy")(cuisines_list, "name");
                $scope.cuisines[0]["active"] = true;
            }
        }

        function SetIngredientGroups(inputData){
            var categories_list=inputData;
            if (categories_list.length>0) {
                for (var i = 0; i < categories_list.length; i++) {
                    categories_list[i]["active"] = false;
                }
                $scope.categories = $filter("orderBy")(categories_list, "name");
                $scope.categories[0]["active"] = true;
            }
            //get all available ingredients
            //$scope.get_ingredients();
        }

        function SetIngredients(inputData){
            $scope.ingredients = inputData;
            //$scope.spinner.ingredients = false; // hide ingredients_spinner

            //all ingredients from start unselected
            for (var z=0;z<$scope.ingredients.length;z++){
                $scope.ingredients[z]["selected"]=false;
            }

            $scope.SetMainButton();
        }

        function SetStatistic(inputData){
            $scope.statistic=inputData;
        }

        ////Get cuisines list
        //$scope.GetCuisines=function(){
        //    recipes_factory.GetCuisines().then(
        //        //Success
        //        function(response, status, headers, config){
        //            // {id,name}
        //            //Adding "ALL" option to cuisine filter
        //            var cuisines_list=response.data;
        //
        //            if (cuisines_list.length > 0) {
        //                //cuisines_list[i]["id"]==$scope.obj.active_cuisine_id;
        //                cuisines_list =$filter("orderBy")(cuisines_list, "name");
        //                cuisines_list.unshift({id:0,name:"All"});
        //
        //                //$scope.active_cuisine_scope={id:0,name:"All"};
        //
        //                $scope.cuisines=cuisines_list;
        //
        //                //TODO if filter was chose
        //               // console.log($scope.obj.active_cuisine_id);
        //                //if (typeof($scope.obj.active_cuisine_id) !== 'undefined'){
        //                //    //choosing proper cuisine group
        //                //    for (var j=0;j<cuisines_list.length;j++){
        //                //        cuisines_list[j]["active"]=cuisines_list[j]["id"]==$scope.obj.active_cuisine_id;
        //                //    }
        //                //}
        //                //else{
        //                //    for (var i=0;i<cuisines_list.length;i++){
        //                //        cuisines_list[i]["active"]=false;
        //                //    }
        //                //    //choosing first cuisine group
        //                //    $scope.cuisines[0]["active"]=true;
        //                //}
        //
        //
        //                for (var i = 0; i < cuisines_list.length; i++) {
        //                    cuisines_list[i]["active"] = false;
        //                }
        //                $scope.cuisines = $filter("orderBy")(cuisines_list, "name");
        //                $scope.cuisines[0]["active"] = true;
        //            }
        //            $scope.spinner.cuisines = false; // hide cuisines_spinner
        //        },
        //        //Error
        //        function(response, status, headers, config){
        //        });
        //};
        //
        //$scope.GetCuisines();
        //
        ////Get categories list
        //$scope.GetCategories=function(){
        //    recipes_factory.GetIngredientGroups().then(
        //        //Success
        //        function(response, status, headers, config){
        //            var categories_list=response.data;
        //            if (categories_list.length>0) {
        //                for (var i = 0; i < categories_list.length; i++) {
        //                    categories_list[i]["active"] = false;
        //                }
        //                $scope.categories = $filter("orderBy")(categories_list, "name");
        //                $scope.categories[0]["active"] = true;
        //            }
        //            $scope.spinner.categories = false; // hide categories_spinner
        //
        //            //get all available ingredients
        //            $scope.get_ingredients();
        //        },
        //        //Error
        //        function(response, status, headers, config){
        //        });
        //};
        //
        //$scope.GetCategories();
        //
        //
        //$scope.get_ingredients=function(){
        //    recipes_factory.GetIngredients().then(
        //        //Success
        //        function(response, status, headers, config){
        //            $scope.ingredients = response.data;
        //            $scope.spinner.ingredients = false; // hide ingredients_spinner
        //
        //            //all ingredients from start unselected
        //            for (var z=0;z<$scope.ingredients.length;z++){
        //                $scope.ingredients[z]["selected"]=false;
        //            }
        //
        //            $scope.SetMainButton();
        //        },
        //        //Error
        //        function(response, status, headers, config){
        //
        //        });
        //};

        $scope.clickOnCuisine=function(item_scope){
            if ($scope.active_cuisine_scope!=null){
                $scope.active_cuisine_scope.checked=false;
            }
            $scope.active_cuisine_scope=item_scope;
            //select current category elements as an active
            item_scope.checked=true;
            //item_scope.obj={id:"...",name:"..."}
        };

        $scope.clickOnIngredient=function(item_obj){
            //$scope.ingredients_obj= {
            //    list:[],
            //    amount:{max:10,min:3}
            //};

            var mas=$scope.ingredients_obj.list;

            if (item_obj.selected){
               mas.push(item_obj);
                //mas.push(item_scope);
            }
            else{
                for (var i=0;i<mas.length;i++){
                    if (mas[i].id==item_obj.id){
                        mas.splice(i,1);
                        break;
                    }
                }
            }
        };

        $scope.clickOnCategory=function(item_scope){
            if ($scope.active_category_scope!=null){
                $scope.active_category_scope.checked=false;
            }
            $scope.active_category_scope=item_scope;
            //select current category elements as an active
            item_scope.checked=true;
            //item_scope.obj={id:"...",name:"..."}
        };

        $scope.SetMainButton=function(){
          //  $scope.create_button={disabled: false, text: $scope.obj.ingredients ? "Update" : "Create"};
        };

        //Create new category
        $scope.createUpdateRecipe = function () {
           var valid_recipe_form=$scope.ingredients_obj.list.length>=$scope.ingredients_obj.amount.min &&
                                 $scope.ingredients_obj.list.length<=$scope.ingredients_obj.amount.max;
           $scope.create_button={disabled: true, text: "..."};

           if (valid_recipe_form){
               var ingredients_id_list=[];
               for (var i=0;i<$scope.ingredients_obj.list.length;i++){
                   ingredients_id_list.push($scope.ingredients_obj.list[i].id);
               }
           }
        };

        $scope.GetIngredientsIdList=function(ingredients_object){
            var result_string="";
            for (var i=0;i<ingredients_object.length;i++){
                result_string+=ingredients_object[i]["id"];
                if (i<ingredients_object.length-1){
                    result_string+=",";
                }
            }
            return result_string;
        };

        //Get all recipes
        $scope.GetRecipes=function(){
           // $scope.spinner.recipes=true;
            //var user_id=($scope.user_data===undefined) ? 0 : $scope.user_data["id"];
            if ($scope.ingredients_obj.list.length<$scope.ingredients_obj.amount.min){
                return;
            }
            $scope.spinner.recipes=true;

            recipes_factory.GetRecipes($scope.accurate_search,
                                       $scope.active_cuisine_scope.obj.id,
                                       $scope.GetIngredientsIdList($scope.ingredients_obj.list)).
                then(
                //success
                function successCallback(response) {
                    $scope.spinner.recipes=false;
                    $scope.recipes=response.data;
                    $scope.alert_message++;
                },
                //error
                function errorCallback(response) {
                    $scope.spinner.recipes=false;
                    alert("Sorry, can't retrieve recipes from data base.");
                });
        };

        $scope.IncrementCookbook=function(){
            $scope.cookbook_amount++;
        };

        $scope.DecrementCookbook=function(){
            $scope.cookbook_amount--;
        };
    });