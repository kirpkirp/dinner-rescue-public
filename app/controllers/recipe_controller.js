angular.module("rescue.controllers").
    controller("recipe_controller", function ($scope,
                                              url_factory,
                                              $window,
                                              recipes_factory,
                                              user_factory
    ) {
        //$scope.user_data (id,name)
        $scope.spinner=true;
        $scope.cookbook_amount=0;
        $scope.can_click=true;

        var user_data_watcher=$scope.$watch("user_data",function(){
            if (!($scope.user_data===undefined)){
                $scope.joinus_button_show=false;
            }
            //  console.log($scope.user_data===undefined ? "No user data" :$scope.user_data);
            user_data_watcher();
        });

        $scope.IncrementCookbook=function(){
            $scope.cookbook_amount++;
        };

        $scope.DecrementCookbook=function(){
            $scope.cookbook_amount--;
        };

        $scope.GetRecipe=function(){
            var cuisine_id=url_factory.GetVar($window.location.search,"id");
            var mode=url_factory.GetVar($window.location.search,"mode");

            if (mode===undefined){
                mode="none";
            }

            recipes_factory.GetRecipe(cuisine_id,mode).
                then(
                //success
                function successCallback(response) {
                    $scope.spinner=false;
                    $scope.recipe=response.data["recipe"];
                    $scope.ingredients=response.data["ingredients"];

                    $scope.no_ingredients=[];
                    $scope.yes_ingredients=[];

                    for(var i=0;i<$scope.ingredients.length;i++){
                        if ($scope.ingredients[i]["active"]){
                            $scope.yes_ingredients.push($scope.ingredients[i])
                        }
                        else{
                            $scope.no_ingredients.push($scope.ingredients[i]);
                        }
                    }

                    $scope.recipe_image_url="img/app/"+$scope.recipe.id[$scope.recipe.id.length-1]+"/"+$scope.recipe.id+".jpg";
                    //console.log(response.data["recipe"]);
                    //console.log(response.data["ingredients"]);
                },
                //error
                function errorCallback(response) {
                    $scope.spinner=false;
                    alert("Sorry, can't retrieve recipes from data base.");
                });
        };

        $scope.GetRecipe();

        $scope.AddRemoveFromCookbook=function(){
            if (!$scope.can_click) return;
            if ($scope.recipe.in_cookbook>0){
                $scope.can_click=false;
                $scope.RemoveRecipeFromCookbook();
            }
            else{
                $scope.can_click=false;
                $scope.AddRecipeToCookbook();
            }
        };

        $scope.RemoveRecipeFromCookbook=function(){
            user_factory.RemoveRecipeFromCookbook($scope.recipe.in_cookbook).then(
                //success
                function successCallback(response) {
                    $scope.can_click=true;
                    if ($scope.confirmationDialog){
                        if (confirm("Do you really want to remove this recipe from your cookbook?")){
                            $scope.DecrementCookbook();
                        }
                    }else{
                        $scope.DecrementCookbook();
                    }
                    $scope.recipe.in_cookbook=0;
                },
                //error
                function errorCallback(response) {
                    $scope.can_click=true;
                    alert("Can't remove recipe from a cookbook. Server error.")
                }
            );
        };

        $scope.AddRecipeToCookbook=function(){
            user_factory.AddRecipeToCookbook($scope.recipe.id).then(
                //success
                function successCallback(response) {
                    $scope.can_click=true;
                    $scope.recipe.in_cookbook=response.data["id"];
                    $scope.IncrementCookbook();
                },
                //error
                function errorCallback(response) {
                    $scope.can_click=true;
                    alert("Can't remove recipe from a cookbook. Server error.")
                }
            );
        };
    });
