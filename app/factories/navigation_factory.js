angular.module("rescue.factories").
    factory("navigation_factory", function($http) {
        return{
            GetNavigationCuisines:
                function(){
                    return $http({method: "GET", url: "api/search/navigation_cuisine/"});
                }
        };
    });

