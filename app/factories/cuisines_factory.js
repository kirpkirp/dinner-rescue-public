angular.module("rescue.factories").
    factory("cuisines_factory", function($http) {
        return{
            GetCuisineRecipes:
                function(cuisine_id){
                    if (!cuisine_id){
                        cuisine_id=0;
                    }
                    return $http({method: "GET",
                                  url: "api/cuisines/cuisine/"+cuisine_id
                    });
                }
        };
    });

// data: JSON.stringify({id: cuisine_id}),
//headers: { 'Content-Type': 'application/json;charset=utf-8' }
