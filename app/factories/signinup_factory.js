angular.module("rescue.factories").
    factory("signinup_factory", function($http) {
        return{
            SignIn:
                function(signin_data){
                    return $http({
                        method : 'POST',
                        url : 'api/sign_in_on/signin',
                        data : signin_data
                    });
                },
            SignUp:
                function(signup_data){
                    return $http({
                        method : 'POST',
                        url : 'api/sign_in_on/signup',
                        data : signup_data
                    });
                }
        };
    });
