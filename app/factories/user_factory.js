angular.module("rescue.factories").
    factory("user_factory", function($http) {
        return{
            GetCookbookRecipes:
                function(){
                    return $http({method: "GET", url: "api/user_data/cookbook_recipes/"});
                },
            //Get user information: name, amount or recipes in cookbook
            GetCookbookAmount:
                function(user_id){
                    return $http({method: "GET", url: "api/user_data/cookbook/"+user_id});
                },

            LogoutUser:
                function(){
                    return $http({method: "POST", url: "api/user_data/sign_out/"})
                },

            AddRecipeToCookbook:
                function(recipe_id){
                    return $http({method: "POST",
                                  url: "api/user_data/addcookbookrecipe/",
                                  data: "recipe_id="+recipe_id,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                                  });
                },

            RemoveRecipeFromCookbook:
                function(cookbook_id){
                    return $http({method: "POST",
                                  url: "api/user_data/removecookbookrecipe/",
                                  data: "cookbook_id="+cookbook_id,
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                    });
                },

            ChangePassword:
                function(password_object){
                    return $http({
                        method: "POST",
                        url: "api/user_data/change_password/",
                        data : password_object
                    });
                },

            RecoverPassword:
                function(password_object){
                    return $http({
                        method: "POST",
                        url: "api/user_data/recover_password/",
                        data : password_object
                    });
                },

            SendRecipe:
                function(recipe_object){
                    return $http({
                        method: "POST",
                        url: "api/user_data/send_recipe/",
                        data: recipe_object
                    });
                }
        };
    });

