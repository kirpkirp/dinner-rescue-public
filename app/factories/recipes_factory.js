angular.module("rescue.factories").
    factory("recipes_factory", function($http) {
        return{
            GetConstructorData:
                function(){
                    return $http({method: "GET", url: "api/search/constructor/"});
                },

            //GetCuisines:
            //    function(){
            //        return $http({method: "GET", url: "api/search/cuisines/"});
            //    },
            //
            //GetIngredientGroups:
            //    function(){
            //        return $http({method: "GET", url: "api/search/ingredient_groups/"});
            //    },
            //
            //GetIngredients:
            //    function(){
            //        return $http({method: "GET", url: "api/search/ingredients/"});
            //    },

            GetRecipes:
                function(accurate_search,cuisine_id,ingredients_list_string){
                    return $http({method: "POST",
                                  url: "api/search/recipes/",
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                  data: {accurate_search: accurate_search,
                                         cuisine_id : cuisine_id,
                                         ingredients: ingredients_list_string}
                                  });
                },

            GetRecipe:
                function(recipe_id_var,mode_var){
                    return $http({method: "POST",
                                  url: "api/search/recipe/",
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                  data: {recipe_id_var : recipe_id_var, mode: mode_var}
                    });
                },

            GetStatistic:
                function(){
                    return $http({method: "GET", url: "api/search/statistic/"});
                }
        };
    });
