angular.module("rescue.factories").
    factory("url_factory", function() {
        return{
            GetVar:
                function(location,url_var){
                    var pairs = location.substring(1).split("&");
                    var obj = {};
                    var pair;
                    var i;
                    for (i in pairs) {
                        if (pairs[i] === "")
                            continue;
                        pair = pairs[i].split("=");
                        obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
                    }
                    return obj[url_var];
                },
            GetURL:
                function(page_name,id,title,search_mode){

                    var main_url_part=page_name+"?id="+id+"-"+encodeURI(title);
                    if (search_mode){
                        main_url_part+="&mode=search";
                    }
                    return main_url_part;
                }
        };
    });