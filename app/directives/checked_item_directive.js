angular.module("rescue.directives").
    directive("checkedItemDirective", function () {
        return {
//                    compile: function compile(temaplateElement, templateAttrs) {
//                        return {
//                            pre: function (scope, element, attrs) {
//                            },
//                            post: function(scope, element, attrs) {
//                                console.log(scope.obj);
//                            }
//                        }
//                    },
            //  priority: 0,
            //  terminal:false,
            //  templateUrl: 'template.html',
            //  transclude: false,
            //link: function (scope, element, attrs) {
            //
            //},
            controller: function ($scope) {
                //Each directive contains access to the $scope.obj={id:"...",name:"..."}
                $scope.checked=false;
                $scope.current_name = $scope.obj.name;

                // If directive has access to the ingredients attribute
                // then this directive will implement next function
                var ing_obj=$scope.activeIngredientsObj;

                if (ing_obj){
                    for (var i=0;i<ing_obj.list.length;i++){
                        if (ing_obj.list[i]==$scope.obj.id){
                            $scope.checked=true;
                        }
                    }
                }

                //execute when user click on directive
                $scope.check_event=function(){
                    //for directive that has ingredients_obj attribute
                    if (ing_obj){
                        var ingrs_mas=ing_obj.list;
                        var max_length=ing_obj.amount.max;
                        if (!(ingrs_mas.length==max_length && !$scope.checked)){
                            //set checked if unchecked state
                            $scope.checked=!$scope.checked;
                            $scope.itemAction({item_scope: $scope});
                        }
                    }
                    //for directive that doesn't have access to ingredients_obj attribute
                    else{
                        //set checked if unchecked state
                        if (!$scope.checked){
                            $scope.checked = true;
                            $scope.itemAction({item_scope: $scope});
                        }
                    }
                };

                //fore directive that doesn't have access to ingredients_obj attribute
                if (!ing_obj){
                    //if item is active, search for ingredients for current ingredients group
                    if ($scope.obj.active){
                        $scope.itemAction({item_scope: $scope});
                    }
                    //clear unused "active" property
                    delete $scope.obj["active"];
                }
            },
            replace: true,
            restrict: "A",
            scope: {
                obj: "=",
                itemAction: "&",
                activeIngredientsObj: "="
            },
            template:
            "<div class='alert directive_item_style checked_item_directive' ng-class='{\"alert-danger\": checked,\"alert-info\": !checked, \"error_red_border\": checked}' ng-click='check_event()'>" +
              "<span>{{obj.name}}</span>" +
                "{{activeIngredientsObj.list}}" +
              //"<i ng-show='checked' ng-click='showHideEditPanel()' class='fa fa-check-circle-o checked_item_directive'></i>" +
            "</div>"
        }
    });