angular.module("rescue.directives").
    directive("checkedRecipeDirective", function () {
        return {
            controller: function ($scope) {
                // If directive has access to the ingredients attribute
                // then this directive will implement next function
                var ing_obj=$scope.activeIngredientsObj;

                if (ing_obj){
                    for (var i=0;i<ing_obj.list.length;i++){
                        if (ing_obj.list[i]==$scope.obj.id){
                            $scope.selected=true;
                        }
                    }
                }

                //execute when user click on directive
                $scope.check_event=function(){
                    //for directive that has ingredients_obj attribute
                    if (ing_obj){
                        var ingrs_mas=ing_obj.list;
                        var max_length=ing_obj.amount.max;
                        if (!((ingrs_mas.length==max_length | ingrs_mas.length>max_length) && !$scope.obj.selected)){
                            //set checked if unchecked state
                            $scope.obj.selected=!$scope.obj.selected;
                            $scope.itemAction({item_obj: $scope.obj});
                        }
                    }
                };
            },
            replace: true,
            restrict: "A",
            scope: {
                obj: "=",
                itemAction: "&",
                activeIngredientsObj: "="
            },
            template:
            "<div class='alert directive_item_style checked_item_directive' ng-class='{\"alert-danger\": obj.selected,\"alert-info\": !obj.selected, \"error_red_border\": obj.selected}' ng-click='check_event()'>" +
            "<span>{{obj.name}}</span>" +
                //"<i ng-show='checked' ng-click='showHideEditPanel()' class='fa fa-check-circle-o checked_item_directive'></i>" +
            "</div>"
        }
    });