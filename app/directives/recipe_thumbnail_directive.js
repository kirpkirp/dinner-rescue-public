angular.module("rescue.directives").
    directive("recipeThumbnailDirective", function () {
        return {
            controller: function ($scope,
                                  user_factory,
                                  url_factory
            ){
                //$scope.user -> {id, name}
                //$scope.recipe -> {id, name, in_cookbook}

                $scope.can_click=true;

                //?p="+Math.random();
                $scope.image_url="img/app/"+$scope.recipe.id[$scope.recipe.id.length-1]+"/"+$scope.recipe.id+".jpg";

                $scope.recipe_url=url_factory.GetURL(
                    "recipe.php",
                    $scope.recipe.id,
                    $scope.recipe.name,
                    $scope.searchPage
                );

                $scope.title=$scope.recipe.name.length>=15 ?  $scope.recipe.name.substring(0,15)+"..." : $scope.recipe.name;

                //$scope.redirectToRecipeEditPage=function(){
                //    $window.location.href = "recipes.php?id="+$scope.obj.id;
                //};

                $scope.AddRemoveFromCookbook=function(){
                    if (!$scope.can_click) return;
                    if ($scope.recipe.in_cookbook>0){
                        $scope.can_click=false;
                        $scope.RemoveRecipeFromCookbook();
                    }
                    else{
                        $scope.can_click=false;
                        $scope.AddRecipeToCookbook();
                    }
                };

                function RemoveRecipe(){
                    user_factory.RemoveRecipeFromCookbook($scope.recipe.in_cookbook).then(
                        //success
                        function successCallback(response) {
                            $scope.decrementCookbook();
                            $scope.removeRecipeFromList({recipe_id: $scope.recipe.in_cookbook});
                            $scope.recipe.in_cookbook = 0;
                            $scope.can_click=true;
                        },
                        //error
                        function errorCallback(response) {
                            $scope.can_click=true;
                            alert("Can't remove recipe from a cookbook. Server error.")
                        }
                    );
                }

                $scope.RemoveRecipeFromCookbook=function(){
                    if ($scope.confirmationDialog){
                        if (confirm("Do you really want to remove this recipe from your cookbook?")) {
                           RemoveRecipe();
                        }
                    }else{
                        //$scope.decrementCookbook();
                        RemoveRecipe();
                    }
                };

                $scope.AddRecipeToCookbook=function(){
                    user_factory.AddRecipeToCookbook($scope.recipe.id).then(
                        //success
                        function successCallback(response) {
                            $scope.recipe.in_cookbook=response.data["id"];
                            //$scope.cookbook++;
                            //$scope.update({current_value: $scope.cookbook});
                            //console.log($scope.cookbook);
                            $scope.incrementCookbook();
                            $scope.can_click=true;
                        },
                        //error
                        function errorCallback(response) {
                            $scope.can_click=true;
                            alert("Can't remove recipe from a cookbook. Server error.")
                        }
                    );
                };

            },
            replace: true,
            restrict: "A",
            scope: {
                searchPage: "=",
                user: "=",
                recipe: "=",
                incrementCookbook: "&",
                decrementCookbook: "&",
                removeRecipeFromList: "&",
                confirmationDialog: "="
            },
            template:
            "<div class='col-md-3'>"+
              "<div class='thumbnail' ng-class='{\"one_hundred_match\": recipe.amount==recipe.general_amount && recipe.general_amount>0}'>"+
                "<a ng-href='{{recipe_url}}' target='_blank'>"+
                  "<img ng-src='{{image_url}}' alt='{{recipe.name}}' class='image_url_active'>"+
                "</a>"+
                "<div class='caption'>"+
                  "<h4><strong>{{title}}</strong></h4>"+

                    "<i ng-if='user.id>0' ng-click='AddRemoveFromCookbook()' class=\"fa fa-2x heart_pointer_margin\" ng-class='{\"fa-heart-o\": recipe.in_cookbook==0, \"fa-heart\": recipe.in_cookbook>0, \"in_cookbook\": recipe.in_cookbook>0}'></i>"+
                    "<span class='match_percent' ng-if='recipe.general_amount>0'>{{recipe.amount}}/{{recipe.general_amount}}</span>"+

                  "<br ng-if='user.id>0 || recipe.general_amount>0'>"+

                "</div>"+
              "</div>"+
            "</div>"
        }
    });
