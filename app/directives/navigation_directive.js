angular.module("rescue.directives").
    directive("navigationDirective", function () {
        return {
            controller: function ($scope,
                                  navigation_factory,
                                  user_factory,
                                  url_factory
            ){
                //user is active
                $scope.active_user=!($scope.user===undefined);

                //Get navigation cuisines data
                $scope.GetNavigationCuisinesData=function(){
                    navigation_factory.GetNavigationCuisines().then(
                        //Success
                        function(response, status, headers, config) {
                            //{id, name, amount}
                            var navigation_data_list=response.data;
                            var recipes_amount=0;
                            for (var i=0;i<navigation_data_list.length;i++){
                                recipes_amount+=parseInt(navigation_data_list[i]["amount"]);
                                navigation_data_list[i]["url"]=url_factory.GetURL(
                                    "cuisine.php",
                                    navigation_data_list[i]["id"],
                                    navigation_data_list[i]["name"]
                                );
                            }
                            navigation_data_list.unshift({id:0,
                                                                 name:"All",
                                                                 amount:recipes_amount.toString(),
                                                                 url: "cuisine.php?id=0-All"
                            });
                            $scope.navigation_data_list=navigation_data_list;
                        },
                        //Error
                        function(response, status, headers, config) {
                            //alert("Sorry. Can't get data from server");
                            console.log("Sorry, can't get naviagtion bar data from the server");
                        }
                    );
                };

                $scope.GetNavigationCuisinesData();

                $scope.signOutUser=function(){
                    user_factory.LogoutUser().then(
                        //Success
                        function(response, status, headers, config){
                            window.location="index.php";

                        },
                        //Error
                        function(response, status, headers, config) {
                            alert("Sorry. Can't signout user, problems with server.");
                        }
                    );
                };

                $scope.GetCookbookAmount=function(){
                    if (!($scope.user===undefined)){
                        user_factory.GetCookbookAmount($scope.user["id"]).then(
                            //Success
                            function(response, status, headers, config){
                                $scope.cookbook=response.data["amount"];
                            },
                            //Error
                            function(response, status, headers, config) {
                                alert("Sorry. Can't signout user, problems with server.");
                            }
                        );
                    }
                };

                $scope.GetCookbookAmount();
            },
            replace: true,
            restrict: "A",
            scope: {
                user: "=",
                cookbook: "="
            },
            template:
               "<nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>"+
               "<div class='container'>"+
                "<div class='navbar-header'>"+
                  "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>"+
                    "<span class='sr-only'>Toggle navigation</span>"+
                    "<span class='icon-bar'></span>"+
                    "<span class='icon-bar'></span>"+
                    "<span class='icon-bar'></span>"+
                  "</button>"+
                  "<a class='navbar-brand' href='index.php'>Dinner Rescue</a>"+
                "</div>"+
                "<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>"+
                  "<ul class='nav navbar-nav navbar-right' ng-cloak>"+
                    "<li class='dropdown'>"+
                      "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Recipes <b class='caret'></b></a>"+
                      "<ul class='dropdown-menu'>"+
                         //separate first drop-down item from others
                        "<li>"+
                          "<a ng-href='{{navigation_data_list[0].url}}'>{{navigation_data_list[0].name}}&nbsp;<span>({{navigation_data_list[0].amount}})</span></a>"+
                        "</li>"+
                        "<li role='separator' class='divider'></li>"+
                         //other drop-down items
                        "<li ng-repeat='item in navigation_data_list | limitTo: navigation_data_list.length : 1 track by $index'>"+
                          "<a ng-href='{{item.url}}'>{{item.name}}&nbsp;<span>({{item.amount}})</span></a>"+
                        "</li>"+
                      "</ul>"+
                    "</li>"+

                    "<li ng-if='active_user'><a href='cookbook.php'>My cookbook&nbsp;&nbsp;<span class='badge red_badge'>{{cookbook}}</span></a></li>"+

                    //"<li ng-if='active_user'><a class='white_link'>Hi, {{user.name}}&nbsp;&nbsp;<i class='fa fa-user'></i></a></li>"+

                    "<li ng-if='active_user' class='dropdown'>"+
                      "<a href='#' class='dropdown-toggle' data-toggle='dropdown' class='white_link'><i class='fa fa-user'></i>&nbsp;&nbsp;{{user.name}} <b class='caret'></b></a>"+
                      "<ul class='dropdown-menu'>"+
                        "<li><a href='profile.php'>Profile</a></li>"+
                        "<li role='separator' class='divider'></li>"+
                        "<li><a href='#' ng-click='signOutUser()'>Sign Out&nbsp;&nbsp;<i class='fa fa-power-off'></i></a></li>"+
                      "</ul>"+
                    "</li>"+

                    "<li ng-if='!active_user'><a href='sign_in_up.php'>Sign Up / In&nbsp;&nbsp;<i class='fa fa-star'></i></a></li>"+
                  "</ul>"+
              "</div>"+
              "</nav>"
        }
    });
