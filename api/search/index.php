<?php
session_start();
require_once('../../vendor/autoload.php');
require_once('../../php/rb.php');
require_once('../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/search/cuisines/ - GET all cuisines
/api/search/ingredient_groups/ - GET all ingredient groups
/api/search/ingredients/ - GET all ingredients
/api/search/ - GET all recipes that match the input ingredients list

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

$app->get('/navigation_cuisine/', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $cuisines=R::getAll("SELECT c.id,c.name, SUM(r.id IS NOT NULL) as amount FROM cuisine c
                         LEFT JOIN recipe r ON c.id=r.cuisine_id
                         GROUP BY c.id;");

    $response=$response->withHeader("Content-Type", "application/json");
    // return JSON-encoded response body with query results
    echo json_encode($cuisines);
    return $response;
});

$app->get('/constructor/', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $cuisines=R::getAll("SELECT c.id,c.name, SUM(r.id IS NOT NULL) as amount FROM cuisine c
                         LEFT JOIN recipe r ON c.id=r.cuisine_id
                         GROUP BY c.id;");

    $response=$response->withHeader("Content-Type", "application/json");
    // return JSON-encoded response body with query results
    echo json_encode(array(
            "cuisines"=>R::getAll("SELECT id,name FROM cuisine"),
            "ingredient_groups"=>R::exportAll(R::find("categor")),
            "ingredients"=>R::getAll("SELECT id,name,ingr_category_id FROM ingredient"),
            "statistic"=>R::getAll("SELECT
                                   (SELECT COUNT(*) FROM rescue.cuisine c) AS cuisines_amount,
                                   (SELECT COUNT(*) FROM rescue.ingredient r) AS ingredients_amount,
                                   (SELECT COUNT(*) FROM rescue.recipe re) AS recipes_amount")[0]
        )
    );
    return $response;
});

$app->get('/ingredient_groups/', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $categories = R::find("categor");
    $response=$response->withHeader("Content-Type", "application/json");
    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($categories));
    return $response;
});

$app->get('/cuisines/', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $cuisines = R::getAll("SELECT id,name FROM cuisine");
    $response=$response->withHeader("Content-Type", "application/json");
    //echo json_encode(R::exportAll($cuisines));
    echo json_encode($cuisines);
    return $response;
});

$app->get('/ingredients/', function (Request $request, Response $response, array $args) {
    //$ingredient = R::find("ingredient");
    $ingredients = R::getAll("SELECT id,name,ingr_category_id FROM ingredient");
    $response=$response->withHeader("Content-Type", "application/json");
    echo json_encode($ingredients);
    return $response;
});

$app->get('/statistic/', function (Request $request, Response $response, array $args) {
    $statistic = R::getAll("SELECT
       (SELECT COUNT(*) FROM rescue.cuisine c) AS cuisines_amount,
       (SELECT COUNT(*) FROM rescue.ingredient r) AS ingredients_amount,
       (SELECT COUNT(*) FROM rescue.recipe re) AS recipes_amount");
    $response=$response->withHeader("Content-Type", "application/json");
    echo json_encode($statistic);
    return $response;
});

$app->post('/recipes/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $search_data = json_decode($body);

        //set session variable for ingredients
        $_SESSION["ingredients"]=explode(',', $search_data->ingredients);;

        $cuisine_id=$search_data->cuisine_id;
        $ingredients=explode(',', $search_data->ingredients);

        if (count($ingredients)==0){
            array_push($ingredients,0);
        }

        $string_for_query="";
        if (count($ingredients)>0){
            for ($i=0;$i<count($ingredients);$i++){
                $ingredients[$i]=intval($ingredients[$i]);
                if ($i==count($ingredients)-1){
                    $string_for_query.="?";
                }
                else{
                    $string_for_query.="?,";
                }
            }
        }
        else{
            $string_for_query="?";
        }


        if (isset($_SESSION["user"])){
            if (!v::intVal()->validate($_SESSION["user"]["id"])
            ){
                throw new Exception();
            }
        }

        if (!v::intVal()->validate($cuisine_id) ||
            count($ingredients)<1
        ){
            throw new Exception();
        }

        $user_id=!isset($_SESSION["user"]) ? 0 : intval($_SESSION["user"]["id"]);

//        $recipes = R::getAll("SELECT DISTINCT r.id,r.name, IFNULL (c.id,0) AS in_cookbook
//                              FROM recipe r
//                              LEFT JOIN cookbook c ON c.recipe_id=r.id AND c.user_id=?
//                              ORDER BY r.id DESC",
//                              array($user_id));
        $cuisine_query="";
        if ($cuisine_id>0){
            array_push($ingredients,$cuisine_id);
            $cuisine_query="AND r.cuisine_id=? ";
        }

        $accurate_search_query=$search_data->accurate_search ? " HAVING COUNT(*)=".count($ingredients)." " : "";

        array_unshift($ingredients,$user_id);

        $current_query_string=
            "SELECT d.recipe_id as id,
                    r.name,
                    COUNT(*) as amount,
                    s.general as general_amount,
                    ROUND(100*COUNT(*)/s.general,1) as recipe_match,
                    IFNULL (c.id,0) AS in_cookbook
             FROM dish d
             LEFT JOIN recipe r ON d.recipe_id=r.id
             LEFT JOIN (SELECT COUNT(*) AS general, d.recipe_id FROM dish d GROUP BY d.recipe_id) s ON s.recipe_id=d.recipe_id
             LEFT JOIN cookbook c ON c.recipe_id=d.recipe_id AND c.user_id=?
             WHERE d.ingredient_id IN (".$string_for_query.") ".$cuisine_query."
             GROUP BY d.recipe_id".$accurate_search_query."
             ORDER BY recipe_match DESC";

        $recipes=R::getAll($current_query_string, $ingredients);

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($recipes);
        return $response;
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/recipe/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $recipe_data = json_decode($body);

        $recipe_id_var=$recipe_data->recipe_id_var;
        $recipe_mode=$recipe_data->mode;

        $recipe_id=substr($recipe_id_var, 0, strspn($recipe_id_var, "0123456789"));

        if (isset($_SESSION["user"])){
            if (!v::intVal()->validate($_SESSION["user"]["id"]) ||
                !v::intVal()->validate($recipe_id)
            ){
                throw new Exception();
            }
        }

        $user_id=!isset($_SESSION["user"]) ? 0 : intval($_SESSION["user"]["id"]);

        $recipe = R::getAll("SELECT DISTINCT cc.name AS cuisine_name,r.id,r.name, IFNULL (c.id,0) AS in_cookbook, r.description
                              FROM recipe r
                              LEFT JOIN cookbook c ON c.recipe_id=r.id AND c.user_id=?
                              LEFT JOIN cuisine cc ON cc.id=r.cuisine_id
                              WHERE r.id=?
                              ORDER BY r.id DESC",
                              array($user_id,$recipe_id));

        $ingredients=R::getAll("SELECT i.id,i.name
                                FROM dish d
                                LEFT JOIN ingredient i ON d.ingredient_id=i.id
                                WHERE d.recipe_id=?",
                                array($recipe_id));

        //set all ingredients passive mode
        for ($i=0;$i<count($ingredients);$i++){
            $ingredients[$i]["active"]=false;
            //here compare with session ingredients variable
        }

        //set active ingredients
        if (isset($_SESSION["ingredients"]) && $recipe_mode=="search"){
            for ($i=0;$i<count($ingredients);$i++){
                for ($j=0;$j<count($_SESSION["ingredients"]);$j++){
                    if ($_SESSION["ingredients"][$j]==$ingredients[$i]["id"]){
                        $ingredients[$i]["active"]=true;
                    }
                }
            }
        }

        $response=$response->withHeader("Content-Type", "application/json");

        $result_obj=array(
            "recipe"=>count($recipe)>0 ? $recipe[0] : $recipe,
            "ingredients"=>$ingredients
        );

        echo json_encode($result_obj);
        return $response;
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/categories/ - Get the list of the categories
$app->get('/', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    $categories = R::find("categor");

    $response=$response->withHeader("Content-Type", "application/json");

    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($categories));
    return $response;
});

//api/categories/{id} - Get certain category
$app->get('/{id}', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
    try {
        $category = R::findOne("categor", "id=?", array($args["id"]));
        if ($category){
            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($category->export());
        }
        else{
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/categories/ - create new category
$app->put('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //Checking category name (between 3 and 30 characters)
        if (!v::stringType()->length(3,30)->validate($input_data->name)){
            throw new Exception();
        }

        //store category
        $category = R::dispense("categor");
        $category->name = (string)$input_data->name;
       // $id = R::store($category);
        R::store($category);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");

        //echo json_encode(R::exportAll($category));
        echo json_encode($category->export());
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();