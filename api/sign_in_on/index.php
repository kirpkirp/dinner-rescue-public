<?php
session_start();
require_once('../../vendor/autoload.php');
require_once('../../php/rb.php');
require_once('../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

//api/sign_in_on/ - Create new user profile
$app->post('/signup', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    //name, email, password
    $user_data = json_decode($body);

    $check_user_data=v::stringType()->length(4,30)->validate($user_data->name) &&
                     v::stringType()->email()->max(100)->validate($user_data->email) &&
                     v::stringType()->length(4,20)->validate($user_data->password) &&
                     v::stringType()->length(4,20)->validate($user_data->confirm_password) &&
                     $user_data->password===$user_data->confirm_password;

    try {
        if (!$check_user_data){
            throw new Exception();
        }
        //Checking if a user with same name or email already exists
        $same_users=R::getAll("SELECT COUNT(*) as amount FROM user WHERE name=? OR email=?",array($user_data->name,$user_data->email));

        if ($same_users[0]["amount"]>0){
            throw new Exception();
        }

//        //crypt password hashing
//        $salt = openssl_random_pseudo_bytes(22);
//        $salt = '$2a$%13$' . strtr(base64_encode($salt), array('_' => '.', '~' => '/'));
//        $pass_hash = crypt($user_data->password, $salt);

        $user = R::dispense("user");
        $user->name = $user_data->name;
        $user->email=$user_data->email;
        $user->pass_hash=db::GetPassHash($user_data->password);
        R::store($user);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");
        $_SESSION["user"]=array("id"=>$user->id,"name"=>$user->name);
        echo json_encode($_SESSION["user"]);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/signin', function (Request $request, Response $response, array $args) {
    $body = $request->getBody();
    //email, password
    $user_data = json_decode($body);

    $check_user_data=v::stringType()->email()->max(100)->validate($user_data->email) &&
                     v::stringType()->length(4,20)->validate($user_data->password);

    try {
        if (!$check_user_data){
            throw new Exception();
        }
        //Checking if a user with same name or email already exists
        $same_users=R::getAll("SELECT * FROM user WHERE email=?",array($user_data->email));
        $user_status=$same_users[0]["active"];

        //$user_valid=$same_users[0]["pass_hash"]===crypt($user_data->password, $same_users[0]["pass_hash"]);
        $user_valid=$same_users[0]["pass_hash"]===db::GetPassHash($user_data->password);
        if (!$user_valid || $user_status==0){
            throw new Exception();
        }
        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");
        $_SESSION["user"]=array("id"=>$same_users[0]["id"],"name"=>$same_users[0]["name"]);
        echo json_encode($_SESSION["user"]);
        //echo json_encode($same_users[0]);
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
        echo $e->getMessage();
    }
    return $response;
});

//api/categories/ - create new category
$app->put('/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $input_data = json_decode($body);

        //Checking category name (between 3 and 30 characters)
        if (!v::stringType()->length(3,30)->validate($input_data->name)){
            throw new Exception();
        }

        //store category
        $category = R::dispense("categor");
        $category->name = (string)$input_data->name;
       // $id = R::store($category);
        R::store($category);

        // return JSON-encoded response body
        $response=$response->withHeader("Content-Type", "application/json");

        //echo json_encode(R::exportAll($category));
        echo json_encode($category->export());
    } catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();