<?php
session_start();
require_once('../../vendor/autoload.php');
require_once('../../php/rb.php');
require_once('../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

/*

/api/search/cuisines/ - GET all cuisines
/api/search/ingredient_groups/ - GET all ingredient groups
/api/search/ingredients/ - GET all ingredients
/api/search/ - GET all recipes that match the input ingredients list

*/

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

$app->post('/recover_password/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $email_data = json_decode($body);

        $check_email=v::email()->validate($email_data->email);

        if (!$check_email){
            throw new Exception();
        }

        $first_user = R::getAll("SELECT * FROM user WHERE email=?",array($email_data->email));

        if ($first_user){
            $current_user=$first_user[0];
            $user = R::load("user",$current_user["id"]);

            $new_pass=db::GeneratePass();
            $user->pass_hash=db::GetPassHash($new_pass);
            R::store($user);

            mail($user->email,
                 "Password recover",
                 "Your new password on dinner rescue is: ".$new_pass);

            $response=$response->withHeader("Content-Type", "application/json");
//            echo json_encode($new_pass);
            echo json_encode($user->id);
        }
        else{
            throw new Exception();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/change_password/', function (Request $request, Response $response, array $args) {
    try {
        $body = $request->getBody();
        $user_data = json_decode($body);

        $check_user_data=
            v::stringType()->length(4,20)->validate($user_data->old_password) &&
            v::stringType()->length(4,20)->validate($user_data->new_password) &&
            v::stringType()->length(4,20)->validate($user_data->confirm_new_password) &&
            $user_data->new_password===$user_data->confirm_new_password;

        if (!isset($_SESSION["user"]["id"]) ||
            !v::intVal()->validate($_SESSION["user"]["id"]) ||
            !$check_user_data
        ){
            throw new Exception();
        }

        $user_id=intval($_SESSION["user"]["id"]);
        $user = R::load("user",$user_id);

        $user_pass_old_hash=db::GetPassHash($user_data->old_password);
        if ($user->pass_hash==$user_pass_old_hash){
            $user->pass_hash = db::GetPassHash($user_data->new_password);
        }
        else{
            throw new Exception();
        }

        R::store($user);

        $response=$response->withHeader("Content-Type", "application/json");

        echo json_encode($user->export());
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/addcookbookrecipe/', function (Request $request, Response $response, array $args) {
    try {
        if (!v::intVal()->validate($_POST["recipe_id"]) ||
            !isset($_SESSION["user"]["id"]) ||
            !v::intVal()->validate($_SESSION["user"]["id"])
        ){
            throw new Exception();
        }

        $user_id=intval($_SESSION["user"]["id"]);
        $recipe_id=intval($_POST["recipe_id"]);
//
//        $cookbook_amount = R::getAll("SELECT COUNT(*) as amount FROM cookbook
//                                      WHERE user_id=?",array($user_id));

        $cookbook=R::dispense("cookbook");
        //getting user_id data form the session variable ($_SESSION["user"]) (id,name)
        $cookbook->user_id=$user_id;
        $cookbook->recipe_id=$recipe_id;
        R::store($cookbook);

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($cookbook->export());
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/removecookbookrecipe/', function (Request $request, Response $response, array $args) {
    try {
        if (!v::intVal()->validate($_POST["cookbook_id"]) ||
            !isset($_SESSION["user"]["id"]) ||
            !v::intVal()->validate($_SESSION["user"]["id"])
        ) {
            throw new Exception();
        }

        $user_id = intval($_SESSION["user"]["id"]);
        $cookbook_id = intval($_POST["cookbook_id"]);

        $cookbook_row = R::findOne('cookbook', 'id=?', array($cookbook_id));

        if ($cookbook_row) {
            if ($cookbook_row->user_id==$user_id){
                R::trash($cookbook_row);
                $response = $response->withStatus(204); // no content
            }
            else{
                throw new Exception();
            }

        } else {
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->post('/send_recipe/', function (Request $request, Response $response, array $args) {
    try {
        if (!isset($_SESSION["user"]["id"]) ||
            !v::intVal()->validate($_SESSION["user"]["id"])
        ) {
            throw new Exception();
        }

        $user_id = intval($_SESSION["user"]["id"]);

        $body = $request->getBody();
        $recipe_data = json_decode($body);

        $check_recipe_data=
            v::stringType()->length(3,50)->validate($recipe_data->cuisine_name) &&
            v::stringType()->length(3,40)->validate($recipe_data->name) &&
            v::stringType()->length(10,400)->validate($recipe_data->ingredients) &&
            v::stringType()->length(200,2000)->validate($recipe_data->description);// &&
            v::stringType()->length(null,300)->url()->validate($recipe_data->image_url);

        if ($check_recipe_data) {
            $recipe_user= R::dispense("recipeuser");
            $recipe_user->name=$recipe_data->name;
            $recipe_user->description=$recipe_data->description;
            $recipe_user->cuisine_name=$recipe_data->cuisine_name;
            $recipe_user->created=(new DateTime())->format("Y-m-d H:i:s");
            $recipe_user->ingredients=$recipe_data->ingredients;
            $recipe_user->user_id=$user_id;
            $recipe_user->image_url=$recipe_data->image_url;

            $id=R::store($recipe_user);

            $response=$response->withHeader("Content-Type", "application/json");
            echo json_encode($id);
        } else {
            throw new ResourceNotFoundException();
        }
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

//api/categories/ - Get the list of the categories
$app->post('/sign_out/', function (Request $request, Response $response, array $args) {
    $response=$response->withHeader("Content-Type", "application/json");
    session_destroy();
    echo json_encode(true);
    return $response;
});

$app->get('/cookbook/{id}', function (Request $request, Response $response, array $args) {
    //echo v::intVal()->validate($args["name"]) ? 1 : 0;
//    $cuisines=R::getAll("SELECT c.id,c.name, SUM(r.id IS NOT NULL) as amount FROM cuisine c
//                         LEFT JOIN recipe r ON c.id=r.cuisine_id
//                         GROUP BY c.id;");
//
//    $response=$response->withHeader("Content-Type", "application/json");
//    // return JSON-encoded response body with query results
//    echo json_encode($cuisines);
//    return $response;

    try {
        $user_id=$args["id"];
        if (!v::intVal()->validate($user_id)){
            throw new Exception();
        }

        $cookbook_amount = R::getAll("SELECT COUNT(*) as amount FROM cookbook
                                      WHERE user_id=?",array($user_id));

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($cookbook_amount[0]);
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->get('/cookbook_recipes/', function (Request $request, Response $response, array $args) {
    try {
        if (!isset($_SESSION["user"]["id"]) ||
            !v::intVal()->validate($_SESSION["user"]["id"])
        ){
            throw new Exception();
        }
        $user_id=$_SESSION["user"]["id"];

        $cookbook_recipes = R::getAll("SELECT DISTINCT r.id,r.name,c.id AS in_cookbook
                                       FROM recipe r
                                       LEFT JOIN cookbook c
                                       ON c.recipe_id=r.id
                                       WHERE c.user_id=? AND c.id IS NOT NULL
                                       ORDER BY c.id DESC",
                                       array($user_id));

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode($cookbook_recipes);
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();