<?php
session_start();
require_once('../../vendor/autoload.php');
require_once('../../php/rb.php');
require_once('../../php/db.php');

use Respect\Validation\Validator as v;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ResourceNotFoundException extends Exception {}

$app = new \Slim\App;

//Setup redbean connection
R::setup(db::GetConnectionString(),db::GetUser(),db::GetPass()); //mysql
//Prevent database changing
R::freeze(true);

$app->get('/cuisine/{id}', function (Request $request, Response $response, array $args) {
    try {
//        $body = $request->getBody();
//        //email, password
//        $user_data = json_decode($body);
        $user_id=0;
        if (isset($_SESSION["user"])){
            if (!v::intVal()->validate($_SESSION["user"]["id"])){
                throw new Exception();
            }
            else{
                $user_id=$_SESSION["user"]["id"];
            }
        }

        $current_id=substr($args["id"], 0, strspn($args["id"], "0123456789"));

        //$category_id=v::intVal()->validate($current_id) ? $current_id : 0;

        $recipes=array();
        $cuisine=array();

        if (v::intVal()->validate($current_id)){
            if ($current_id==0){
                $recipes=R::getAll("SELECT DISTINCT r.id,r.name, IFNULL (c.id,0) AS in_cookbook
                                    FROM recipe r
                                    LEFT JOIN cookbook c ON c.recipe_id=r.id AND c.user_id=?
                                    ORDER BY r.id DESC",array($user_id));
                $cuisine=array(array("name"=>"All"));
            }
            else{
                $recipes=R::getAll("SELECT DISTINCT r.id,r.name, IFNULL (c.id,0) AS in_cookbook
                                    FROM recipe r
                                    LEFT JOIN cookbook c ON c.recipe_id=r.id AND c.user_id=?
                                    WHERE r.cuisine_id=?
                                    ORDER BY r.id DESC",
                                    array($user_id,$current_id));
                $cuisine= R::getAll("SELECT name FROM cuisine WHERE id=?", array($current_id));
            }
        }

        $cuisine_object= count($cuisine)>0 ? $cuisine[0] : array("name"=>"none");

        $response=$response->withHeader("Content-Type", "application/json");
        echo json_encode(array("recipes"=>$recipes,"cuisine"=> $cuisine_object));
    }
    catch (ResourceNotFoundException $e){
        $response=$response->withStatus(404);// return 404 server error
    }
    catch (Exception $e) {
        $response=$response->withStatus(400);
        $response=$response->withHeader('X-Status-Reason', $e->getMessage());
    }
    return $response;
});

$app->run();